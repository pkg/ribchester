/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "error.h"

#include <gio/gio.h>

#define RC_ERROR_PREFIX "org.apertis.Ribchester.Error."

static const GDBusErrorEntry error_entries[] =
{
    { RC_ERROR_FAILED, RC_ERROR_PREFIX "Failed" },
    { RC_ERROR_INVALID_ARGUMENT, RC_ERROR_PREFIX "InvalidArgument" },
    { RC_ERROR_NOT_FOUND, RC_ERROR_PREFIX "NotFound" },
    { RC_ERROR_EXISTS, RC_ERROR_PREFIX "Exists" }
};

G_STATIC_ASSERT (G_N_ELEMENTS (error_entries) == RC_N_ERRORS);

GQuark
rc_error_quark (void)
{
  static volatile gsize id = 0;

  g_dbus_error_register_error_domain ("rc-error-quark", &id,
      error_entries, G_N_ELEMENTS (error_entries));

  return (GQuark) id;
}
