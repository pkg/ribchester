/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __RIBCHESTER_H__
#define __RIBCHESTER_H__
#define __RIBCHESTER_IN_RIBCHESTER_H__

#include <ribchester/enumtypes.h>
#include <ribchester/error.h>

#include <ribchester/org.apertis.Ribchester.AppStore.h>
#include <ribchester/org.apertis.Ribchester.AppStore.Transaction.h>
#include <ribchester/org.apertis.Ribchester.BundleManager1.h>

typedef enum
{
	RIBCHESTER_BOOT_TYPE_NORMAL,
	RIBCHESTER_BOOT_TYPE_SUCCESSFUL_UPDATE,
	RIBCHESTER_BOOT_TYPE_FAILED_UPDATE,
	RIBCHESTER_BOOT_TYPE_ROLLBACK,
	RIBCHESTER_BOOT_TYPE_UNKNOWN
} RibchesterBootType;

#define RC_BUS_NAME "org.apertis.Ribchester"
#define RC_OBJECT_PATH_BUNDLE_MANAGER1 "/org/apertis/Ribchester/BundleManager1"

typedef enum
{
	RIBCHESTER_OK,           		// Info message indicating that there is no error
	RIBCHESTER_INSTALLER_TEMP_EXISTS,  		// Error Info message indicating that an installer temp subvolume is present
	RIBCHESTER_APP_SUBVOL_EXISTS,     		// Err info message indicating that an app subvolume already exists.
	RIBCHESTER_APP_SUBVOL_UNAVAILABLE,     	// Err info message indicating that an app subvolume is not available
	RIBCHESTER_APP_SUBVOL_MOUNT_EXISTS,   	// Error Info message indicating that a mount point already exists for the app subvol
	RIBCHESTER_APP_SUBVOL_BUSY,   	// Error message indicating that the subvolume is not mounted
	RIBCHESTER_APP_SUBVOL_NOT_MOUNTED,   	// Error message indicating that the subvolume is not mounted
	RIBCHESTER_APP_SNAPSHOT_EXISTS,   		// Error message indicating that the snapshot for subvolume already exists
	RIBCHESTER_APP_SNAPSHOT_UNAVAILABLE,   	// Error message indicating that the snapshot for subvolume is not available
	RIBCHESTER_APP_ALREADY_MOUNTED,   	// App subvolume is alreaduy mounted in the given path
	RIBCHESTER_APP_DATA_SYNC_FAILED,                     // Error message indicating that the data sync to requested subvol failed
        RIBCHESTER_APP_DATA_NO_SOURCE_DATA,          // Error message indicating that the source data not found for sync
	RIBCHESTER_UNKNOWN_ERROR		// Unknown error in mount manager
} RibchesterErrInfo;

typedef enum
{
	RIBCHESTER_APP_STORE_TRANSACTION_STAGE_CANCELLED = -20,
	RIBCHESTER_APP_STORE_TRANSACTION_STAGE_CANCELLING = -15,
	RIBCHESTER_APP_STORE_TRANSACTION_STAGE_FAILED = -10,
	RIBCHESTER_APP_STORE_TRANSACTION_STAGE_FAILING = -5,
	RIBCHESTER_APP_STORE_TRANSACTION_STAGE_STARTING = 0,
	RIBCHESTER_APP_STORE_TRANSACTION_STAGE_VERIFYING = 10,
	RIBCHESTER_APP_STORE_TRANSACTION_STAGE_PREPARING = 20,
	RIBCHESTER_APP_STORE_TRANSACTION_STAGE_UNPACKING = 30,
	RIBCHESTER_APP_STORE_TRANSACTION_STAGE_FINISHING = 40,
	RIBCHESTER_APP_STORE_TRANSACTION_STAGE_SUCCEEDED = 50,
} RibchesterAppStoreTransactionStage;

#undef __RIBCHESTER_IN_RIBCHESTER_H__
#endif
