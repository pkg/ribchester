/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef RIBCHESTER_ERROR_H
#define RIBCHESTER_ERROR_H

#include <glib.h>

G_BEGIN_DECLS

/**
 * RC_ERROR:
 *
 * Error domain for errors encountered by the Ribchester volume-mounting
 * service. Error codes in this domain are members of the #RcError
 * enumeration.
 */
#define RC_ERROR (rc_error_quark ())

/**
 * RcError:
 * @RC_ERROR_FAILED: A generic error: "something went wrong".
 * @RC_ERROR_INVALID_ARGUMENT: An argument was invalid.
 * @RC_ERROR_NOT_FOUND: The specified file, app-bundle, etc. does not exist.
 * @RC_ERROR_EXISTS: An attempt to create something failed because it already
 *  exists.
 *
 * Error enumeration for errors encountered by the Ribchester
 * volume-mounting service.
 */
typedef enum
{
  RC_ERROR_FAILED,
  RC_ERROR_INVALID_ARGUMENT,
  RC_ERROR_NOT_FOUND,
  RC_ERROR_EXISTS,
  /*< private >*/
  RC_N_ERRORS /*< skip >*/
} RcError;

GQuark rc_error_quark (void);

G_END_DECLS

#endif
