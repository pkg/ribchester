/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <gio/gunixfdlist.h>
#include <glib.h>
#include <glib/gstdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "tests/common.h"

typedef struct
{
  RibchesterAppStore *app_store_proxy;
  RibchesterBundleManager1 *bundle_manager1_proxy;
  TestsAppBundle bundle;
  int bundle_fd;
} Fixture;

/* None of these tests need more than one version at the moment */
static const gchar * const version_numbers[] = { "0" };

/*
 * Common setup for all tests.
 */
static void
setup (Fixture *f,
       gconstpointer user_data)
{
  g_autofree gchar *ribchester_full = NULL;
  g_autoptr (GError) error = NULL;
  TestsInstallApi api = GPOINTER_TO_INT (user_data);

  ribchester_full = g_find_program_in_path ("ribchester");

  if (api != TESTS_INSTALL_API_BUNDLE_MANAGER1 &&
      ribchester_full == NULL)
    {
      g_test_skip ("This test is only for the full version of ribchester");
      return;
    }

  if (!tests_require_ribchester (NULL, &f->app_store_proxy,
                                 &f->bundle_manager1_proxy))
    return;

  if (!tests_app_bundle_set_up (&f->bundle, f->app_store_proxy,
                                f->bundle_manager1_proxy,
                                CBY_PROCESS_INFO_NO_USER_ID,
                                version_numbers,
                                G_N_ELEMENTS (version_numbers)))
    return;
}

static void
teardown (Fixture *f,
          gconstpointer unused G_GNUC_UNUSED)
{
  g_autoptr (GError) error = NULL;

  tests_app_bundle_tear_down (&f->bundle);
  g_clear_object (&f->app_store_proxy);
  g_clear_object (&f->bundle_manager1_proxy);
}

static void
test_bundle_install (Fixture *f,
                     gconstpointer user_data)
{
  gboolean succeeded;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *objpath = NULL;
  g_autoptr (RibchesterAppStoreTransaction) transaction = NULL;
  gint stage;
  g_autofree gchar *bundle_id = NULL;
  g_autofree gchar *bundle_version = NULL;
  gint attempts_remaining;
  g_autofree gchar *desktoppath = NULL;
  g_autoptr (GFile) desktop_file = NULL;
  TestsInstallApi api = GPOINTER_TO_INT (user_data);

  /* early return if failed or skipped */
  if (g_test_failed ())
    return;

  objpath = tests_app_bundle_begin_install_bundle (&f->bundle, api, NULL,
                                                   &f->bundle.versions[0]);

  transaction = ribchester_app_store_transaction_proxy_new_sync (g_dbus_proxy_get_connection (G_DBUS_PROXY (f->app_store_proxy)),
                                                                 G_DBUS_PROXY_FLAGS_GET_INVALIDATED_PROPERTIES,
                                                                 g_dbus_proxy_get_name (G_DBUS_PROXY (f->app_store_proxy)),
                                                                 objpath,
                                                                 NULL, &error);

  g_assert_nonnull (transaction);
  g_assert_no_error (error);

  g_object_get (transaction, "stage", &stage, NULL);
  g_assert_cmpint (stage, >=, 0);

  succeeded = ribchester_app_store_transaction_call_release_sync (transaction,
                                                                  NULL,
                                                                  &error);
  g_assert_no_error (error);
  g_assert_cmpint (succeeded, ==, TRUE);

  /* Wait until the transaction goes into a succeeded state */
  g_object_get (transaction, "stage", &stage, NULL);
  while (stage >= 0 &&
         stage != RIBCHESTER_APP_STORE_TRANSACTION_STAGE_SUCCEEDED)
    {
      g_main_context_iteration (NULL, TRUE);
      g_object_get (transaction, "stage", &stage, NULL);
    }

  g_assert_cmpint (stage, ==,
                   RIBCHESTER_APP_STORE_TRANSACTION_STAGE_SUCCEEDED);

  g_object_get (transaction, "bundle-id", &bundle_id, NULL);
  g_assert_cmpstr (bundle_id, ==, TEST_BUNDLE_ID);

  g_object_get (transaction, "bundle-version", &bundle_version, NULL);
  g_assert_cmpstr (bundle_version, ==, "0");

  /* Wait for a while to let the transaction disappear */
  attempts_remaining = 100;
  while (--attempts_remaining)
    {
      g_autoptr (GVariant) ret;

      ret = g_dbus_connection_call_sync (g_dbus_proxy_get_connection (G_DBUS_PROXY (transaction)),
                                         g_dbus_proxy_get_name (G_DBUS_PROXY (transaction)),
                                         g_dbus_proxy_get_object_path (G_DBUS_PROXY (transaction)),
                                         "org.freedesktop.DBus.Properties",
                                         "GetAll",
                                         g_variant_new ("(s)",
                                                        g_dbus_proxy_get_interface_name (G_DBUS_PROXY (transaction))),
                                         G_VARIANT_TYPE ("(a{sv})"),
                                         G_DBUS_CALL_FLAGS_NONE,
                                         -1, NULL, &error);

      g_usleep (G_USEC_PER_SEC);

      if (ret == NULL)
        {
          /* gdbus responds with unknown method if the object on which the
           * properties interface is used isn't registered */
          g_assert_error (error, G_DBUS_ERROR, G_DBUS_ERROR_UNKNOWN_METHOD);
          break;
        }
    }

  g_assert_cmpint (attempts_remaining, >, 0);

  /* Assert the application bundle was indeed installed where expected */
  desktoppath = g_build_filename (G_DIR_SEPARATOR_S,
                                  "Applications",
                                  TEST_BUNDLE_ID,
                                  "share",
                                  "applications",
                                  TEST_BUNDLE_ID".desktop",
                                  NULL);

  desktop_file = g_file_new_for_path (desktoppath);
  g_assert (g_file_query_exists (desktop_file, NULL));
}

static void
test_bundle_cancel (Fixture *f,
                    gconstpointer user_data)
{
  gboolean succeeded;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *objpath = NULL;
  g_autoptr (RibchesterAppStoreTransaction) transaction = NULL;
  gint stage;
  gint attempts_remaining;
  TestsInstallApi api = GPOINTER_TO_INT (user_data);

  /* early return if failed or skipped */
  if (g_test_failed ())
    return;

  objpath = tests_app_bundle_begin_install_bundle (&f->bundle, api, NULL,
                                                   &f->bundle.versions[0]);

  transaction = ribchester_app_store_transaction_proxy_new_sync (g_dbus_proxy_get_connection (G_DBUS_PROXY (f->app_store_proxy)),
                                                                 G_DBUS_PROXY_FLAGS_GET_INVALIDATED_PROPERTIES,
                                                                 g_dbus_proxy_get_name (G_DBUS_PROXY (f->app_store_proxy)),
                                                                 objpath,
                                                                 NULL, &error);

  g_assert_nonnull (transaction);
  g_assert_no_error (error);

  g_object_get (transaction, "stage", &stage, NULL);
  g_assert_cmpint (stage, >=, 0);

  succeeded = ribchester_app_store_transaction_call_cancel_sync (transaction,
                                                                 NULL,
                                                                 &error);
  g_assert_no_error (error);
  g_assert_cmpint (succeeded, ==, TRUE);

  succeeded = ribchester_app_store_transaction_call_release_sync (transaction,
                                                                  NULL,
                                                                  &error);
  g_assert_no_error (error);
  g_assert_cmpint (succeeded, ==, TRUE);

  /* Wait until the transaction goes into a cancelled state */
  g_object_get (transaction, "stage", &stage, NULL);
  while (stage >= 0 ||
         stage == RIBCHESTER_APP_STORE_TRANSACTION_STAGE_CANCELLING)
    {
      g_main_context_iteration (NULL, TRUE);
      g_object_get (transaction, "stage", &stage, NULL);
    }

  g_assert_cmpint (stage, ==,
                   RIBCHESTER_APP_STORE_TRANSACTION_STAGE_CANCELLED);

  /* Wait for a while to let the transaction disappear */
  attempts_remaining = 100;
  while (--attempts_remaining)
    {
      g_autoptr (GVariant) ret;

      ret = g_dbus_connection_call_sync (g_dbus_proxy_get_connection (G_DBUS_PROXY (transaction)),
                                         g_dbus_proxy_get_name (G_DBUS_PROXY (transaction)),
                                         g_dbus_proxy_get_object_path (G_DBUS_PROXY (transaction)),
                                         "org.freedesktop.DBus.Properties",
                                         "GetAll",
                                         g_variant_new ("(s)",
                                                        g_dbus_proxy_get_interface_name (G_DBUS_PROXY (transaction))),
                                         G_VARIANT_TYPE ("(a{sv})"),
                                         G_DBUS_CALL_FLAGS_NONE,
                                         -1, NULL, &error);

      if (ret == NULL)
        {
          /* gdbus responds with unknown method if the object on which the
           * properties interface is used isn't registered */
          g_assert_error (error, G_DBUS_ERROR, G_DBUS_ERROR_UNKNOWN_METHOD);
          break;
        }

      g_usleep (G_USEC_PER_SEC/10);

    }

  g_assert_cmpint (attempts_remaining, >, 0);
}

int
main (int argc,
      char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/bundle/install/bundle-manager1", Fixture,
              GINT_TO_POINTER (TESTS_INSTALL_API_BUNDLE_MANAGER1),
              setup, test_bundle_install, teardown);
  g_test_add ("/bundle/cancel/bundle-manager1", Fixture,
              GINT_TO_POINTER (TESTS_INSTALL_API_BUNDLE_MANAGER1),
              setup, test_bundle_cancel, teardown);

  g_test_add ("/bundle/install/app-store", Fixture,
              GINT_TO_POINTER (TESTS_INSTALL_API_APP_STORE),
              setup, test_bundle_install, teardown);
  g_test_add ("/bundle/cancel/app-store", Fixture,
              GINT_TO_POINTER (TESTS_INSTALL_API_APP_STORE),
              setup, test_bundle_cancel, teardown);

  return g_test_run ();
}
