/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <ribchester/ribchester.h>

/* Test that every error is registered with the GDBus error infrastructure. */
static void
test_rc_error (void)
{
  GEnumClass *cls = g_type_class_ref (RC_TYPE_ERROR);
  guint i;

  for (i = 0; i < cls->n_values; i++)
    {
      const GEnumValue *v = &cls->values[i];
      g_autoptr (GError) error = g_error_new (RC_ERROR, v->value, "?!");
      g_autofree gchar *name = g_dbus_error_encode_gerror (error);

      g_assert_true (g_str_has_prefix (name, "org.apertis.Ribchester."));

      /* We want Ribchester.Error.NOT_FOUND, not
       * Ribchester.Error.ERROR_NOT_FOUND. */
      g_assert_false (g_str_has_prefix (v->value_name, "error"));
      g_assert_false (g_str_has_prefix (v->value_nick, "error"));
    }

  g_type_class_unref (cls);
}

int
main (int argc,
    char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/rc-error", test_rc_error);

  return g_test_run ();
}
