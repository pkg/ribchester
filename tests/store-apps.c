/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2016-2017 Collabora Ltd.
 * Copyright © 2017 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <gio/gio.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <glib-object.h>

#include <canterbury/canterbury-platform.h>
#include <ribchester/ribchester.h>

#include "tests/common.h"

#define TESTS_TYPE_MOCK_USER_MANAGER (tests_mock_user_manager_get_type ())

G_DECLARE_FINAL_TYPE (TestsMockUserManager, tests_mock_user_manager, TESTS,
                      MOCK_USER_MANAGER, CbyPerUserAppManager1Skeleton)

struct _TestsMockUserManager
{
  CbyPerUserAppManager1Skeleton parent;

  GDBusConnection *system_bus;
  guint times_asked_to_terminate;
  guint own_name_id;
  gboolean name_owned;
  gboolean will_fail;
};

G_DEFINE_TYPE (TestsMockUserManager, tests_mock_user_manager,
               CBY_TYPE_PER_USER_APP_MANAGER1_SKELETON)

static gboolean
terminate_bundle_cb (TestsMockUserManager *self,
                     GDBusMethodInvocation *invocation,
                     const gchar *bundle_id,
                     gpointer nil G_GNUC_UNUSED)
{
  CbyPerUserAppManager1 *iface = CBY_PER_USER_APP_MANAGER1 (self);

  self->times_asked_to_terminate++;

  if (self->will_fail)
    g_dbus_method_invocation_return_dbus_error (invocation,
                                                "com.example.Failed",
                                                "I can't do that");
  else
    cby_per_user_app_manager1_complete_terminate_bundle (iface, invocation);

  return TRUE; /* handled */
}

static void
tests_mock_user_manager_dispose (GObject *object)
{
  TestsMockUserManager *self = TESTS_MOCK_USER_MANAGER (object);

  if (self->own_name_id)
    {
      g_bus_unown_name (self->own_name_id);
      self->own_name_id = 0;
    }

  if (self->system_bus != NULL)
    {
      g_autoptr (GError) error = NULL;

      g_dbus_connection_close_sync (self->system_bus, NULL, &error);

      if (!g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CLOSED))
        g_assert_no_error (error);

      g_clear_object (&self->system_bus);
    }

  G_OBJECT_CLASS (tests_mock_user_manager_parent_class)->dispose (object);
}

static void
tests_mock_user_manager_tear_down (TestsMockUserManager *self)
{
  g_autoptr (GError) error = NULL;

  g_dbus_connection_close_sync (self->system_bus, NULL, &error);
  g_assert_no_error (error);
}

static void
mock_user_manager_name_acquired_cb (GDBusConnection *connection,
                                    const gchar *name,
                                    gpointer user_data)
{
  TestsMockUserManager *self = TESTS_MOCK_USER_MANAGER (user_data);

  g_test_message ("TestsMockUserManager %p: Name %s acquired", self, name);

  self->name_owned = TRUE;
}

static void
mock_user_manager_name_lost_cb (GDBusConnection *connection,
                                const gchar *name,
                                gpointer user_data)
{
  TestsMockUserManager *self = TESTS_MOCK_USER_MANAGER (user_data);

  g_test_message ("TestsMockUserManager %p: Name %s lost", self, name);

  /* It's OK to be replaced by another user manager, but it isn't OK if we
   * could not get the name at all */
  if (!self->name_owned)
    g_error ("Unable to acquire bus name %s", name);
}

static void
tests_mock_user_manager_constructed (GObject *object)
{
  TestsMockUserManager *self = TESTS_MOCK_USER_MANAGER (object);
  g_autoptr (GAsyncResult) result = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) tuple = NULL;
  g_autofree gchar *system_bus_address = NULL;

  G_OBJECT_CLASS (tests_mock_user_manager_parent_class)->constructed (object);

  g_signal_connect (self, "handle-terminate-bundle",
                    G_CALLBACK (terminate_bundle_cb), NULL);

  /* Connect to system bus so we can register to receive TerminateBundle().
   * Connect with a private connection so that Ribchester thinks we are
   * multiple instances of Canterbury. */
  system_bus_address =
      g_dbus_address_get_for_bus_sync (G_BUS_TYPE_SYSTEM, NULL, &error);
  g_assert_no_error (error);
  self->system_bus =
      g_dbus_connection_new_for_address_sync (system_bus_address,
                                              (G_DBUS_CONNECTION_FLAGS_MESSAGE_BUS_CONNECTION |
                                               G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT),
                                              NULL, NULL, &error);
  g_assert_no_error (error);

  g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (self),
                                    self->system_bus,
                                    CBY_OBJECT_PATH_PER_USER_APP_MANAGER1,
                                    &error);
  g_assert_no_error (error);

  self->own_name_id =
      g_bus_own_name_on_connection (self->system_bus,
                                    CBY_SYSTEM_BUS_NAME_PER_USER_APP_MANAGER1,
                                    (G_BUS_NAME_OWNER_FLAGS_ALLOW_REPLACEMENT |
                                     G_BUS_NAME_OWNER_FLAGS_REPLACE),
                                    mock_user_manager_name_acquired_cb,
                                    mock_user_manager_name_lost_cb,
                                    self, NULL);

  while (!self->name_owned)
    g_main_context_iteration (NULL, TRUE);

  g_dbus_connection_call (self->system_bus, RC_BUS_NAME,
                          CBY_OBJECT_PATH_PRIVILEGED_APP_HELPER1,
                          CBY_INTERFACE_PRIVILEGED_APP_HELPER1,
                          "RegisterUserManager",
                          NULL, G_VARIANT_TYPE_UNIT,
                          G_DBUS_CALL_FLAGS_NONE, -1, NULL,
                          tests_store_result_cb, &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  tuple = g_dbus_connection_call_finish (self->system_bus, result, &error);
  g_assert_no_error (error);
}

static void
tests_mock_user_manager_class_init (TestsMockUserManagerClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  object_class->constructed = tests_mock_user_manager_constructed;
  object_class->dispose = tests_mock_user_manager_dispose;
}

static void
tests_mock_user_manager_init (TestsMockUserManager *self)
{
}

typedef struct
{
  TestsAppBundle bundle;
  RibchesterAppStore *app_store_proxy;
  RibchesterBundleManager1 *bundle_manager1_proxy;
  guint ordinary_uid;
  guint ordinary_gid;
  gchar *uid_string;
  TestsMockUserManager *user_managers[2];
} Fixture;

typedef enum
{
  STOP_AFTER_BEGIN,
  STOP_AFTER_UNPACK,
  /* don't stop: in other words, commit the transaction */
  STOP_NEVER
} StopAfter;

typedef struct
{
  TestsInstallApi install_api;
  StopAfter stop_after;
  gboolean reinstall;
} Config;

static const gchar * const version_numbers[] = { "0", "1" };

/*
 * Common setup for all tests.
 */
static void
setup (Fixture *f,
       gconstpointer user_data)
{
  g_autofree gchar *ribchester_full = NULL;
  const Config *config = user_data;
  g_autoptr (GError) error = NULL;
  gsize i;

  ribchester_full = g_find_program_in_path ("ribchester");

  if (config->install_api != TESTS_INSTALL_API_BUNDLE_MANAGER1 &&
      ribchester_full == NULL)
    {
      g_test_skip ("This test is only for the full version of ribchester");
      return;
    }

  if (!tests_require_ordinary_uid (&f->ordinary_uid, &f->ordinary_gid,
                                   NULL, NULL))
    return;

  if (!tests_require_ribchester (NULL, &f->app_store_proxy,
                                 &f->bundle_manager1_proxy))
    return;

  if (!tests_app_bundle_set_up (&f->bundle, f->app_store_proxy,
                                f->bundle_manager1_proxy, f->ordinary_uid,
                                version_numbers,
                                G_N_ELEMENTS (version_numbers)))
    return;

  f->uid_string = g_strdup_printf ("%u", f->ordinary_uid);

  /* We do this after tests_app_bundle_set_up() so that if there is a
   * leftover app-bundle present, we won't count the TerminateBundle()
   * call that happened while we removed it */
  for (i = 0; i < G_N_ELEMENTS (f->user_managers); i++)
    f->user_managers[i] = g_object_new (TESTS_TYPE_MOCK_USER_MANAGER,
                                        NULL);
}

/*
 * Helper: do some or all of the process of installing an app-store app.
 *
 * @from_version: the version already installed
 * @to_version: the version to be installed
 * @stop_after: stop after doing this much of the process, or STOP_NEVER
 * @already_unpacked: if non-%NULL, assume we already called BeginInstall()
 */
static void
fixture_install_app (Fixture *f,
                     const TestsVersion *from_version,
                     const TestsVersion *to_version,
                     TestsInstallApi install_api,
                     StopAfter stop_after,
                     const gchar *already_unpacked)
{
  GStatBuf stat_buf;
  gchar *executable = NULL;
  gboolean ok;
  GError *error = NULL;
  guint times_asked_to_terminate;

  times_asked_to_terminate = f->user_managers[0]->times_asked_to_terminate;
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==,
                    times_asked_to_terminate);

  if (install_api == TESTS_INSTALL_API_LOW_LEVEL)
    {
      g_autofree gchar *unpack_to = NULL;
      g_autofree gchar *bindir = NULL;

      if (already_unpacked != NULL)
        {
          unpack_to = g_strdup (already_unpacked);
        }
      else
        {
          unpack_to = tests_app_bundle_begin_install (&f->bundle,
                                                      from_version, to_version);

          g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==,
                            times_asked_to_terminate + 1);
          g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==,
                            times_asked_to_terminate + 1);
        }

      /* The temporary subvolume is owned by root. */
      tests_assert_syscall_0 (g_stat (unpack_to, &stat_buf));
      g_assert_cmpuint (stat_buf.st_uid, ==, 0);
      g_assert_cmpuint (stat_buf.st_gid, ==, 0);
      g_assert_cmpuint (stat_buf.st_mode & S_IFMT, ==, S_IFDIR);
      g_assert_cmpuint (stat_buf.st_mode & ~S_IFMT, ==, 0755);

      if (stop_after <= STOP_AFTER_BEGIN)
        return;

      /* The real store-app installer would unpack a tarball or other package
       * file; do a simple version, creating a script ${prefix}/bin/hello-0. */
      bindir = g_build_filename (unpack_to, "bin", NULL);
      executable = g_build_filename (bindir, to_version->flag_file, NULL);
      tests_assert_syscall_0 (g_mkdir_with_parents (bindir, 0755));
      ok = g_file_set_contents (executable, "#!/bin/sh\necho \"hello, world\"\n",
                                -1, &error);
      g_assert_no_error (error);
      g_assert_true (ok);
      tests_assert_syscall_0 (chmod (executable, 0755));
      g_free (executable);

      if (stop_after <= STOP_AFTER_UNPACK)
        return;

      /* Commit the transaction. */
      tests_app_bundle_commit_install (&f->bundle, from_version,
                                       to_version);
    }
  else
    {
      g_assert (install_api == TESTS_INSTALL_API_APP_STORE ||
                install_api == TESTS_INSTALL_API_BUNDLE_MANAGER1);
      g_assert_cmpint (stop_after, ==, STOP_NEVER);
      g_assert_cmpstr (already_unpacked, ==, NULL);

      tests_app_bundle_install_simple (&f->bundle, install_api,
                                       from_version, to_version);

      g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==,
                        times_asked_to_terminate + 1);
      g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==,
                        times_asked_to_terminate + 1);
    }

  /* The executable is in the new location */
  executable = g_build_filename (f->bundle.static_subvolume, "bin",
                                 to_version->flag_file, NULL);
  g_assert_true (g_file_test (executable, G_FILE_TEST_EXISTS));
  g_free (executable);

  /* The app is usable */
  executable = g_build_filename (f->bundle.static_path, "bin",
                                 to_version->flag_file, NULL);
  g_assert_true (g_file_test (executable, G_FILE_TEST_EXISTS));
  g_free (executable);
}

/*
 * Helper: upgrade from a version to another. This is a test in its
 * own right (test_upgrade()) and is also a prerequisite for the rollback test.
 */
static void
fixture_upgrade (Fixture *f,
                 TestsInstallApi install_api,
                 StopAfter stop_after,
                 const TestsVersion *from_version,
                 const TestsVersion *to_version)
{
  gchar *executable = NULL;
  g_autofree gchar *data_file_via_mount = NULL;
  g_autofree gchar *data_file_internal = NULL;
  gchar *bindir = NULL;
  gboolean ok;
  GError *error = NULL;
  g_autofree gchar *unpack_to = NULL;
  guint times_asked_to_terminate;

  times_asked_to_terminate = f->user_managers[0]->times_asked_to_terminate;
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==,
                    times_asked_to_terminate);

  if (getuid () == 0)
    {
      tests_assert_syscall_0 (g_mkdir_with_parents (f->bundle.users_path,
                                                    0755));
      tests_assert_syscall_0 (g_mkdir_with_parents (f->bundle.persistence_path,
                                                    0700));
      tests_assert_syscall_0 (chown (f->bundle.persistence_path,
                                     f->ordinary_uid, f->ordinary_gid));

      /* Pretend we've used the app, by creating some data. */
      data_file_internal = g_build_filename (f->bundle.variable_subvolume,
                                             "users", f->uid_string, "hello.db",
                                             NULL);
      data_file_via_mount = g_build_filename (f->bundle.persistence_path,
                                              "hello.db", NULL);

      ok = g_file_set_contents (data_file_internal, "here is my valuable data",
                                -1, &error);
      g_assert_no_error (error);
      g_assert_true (ok);
      tests_assert_syscall_0 (chown (data_file_internal, f->ordinary_uid,
                                     f->ordinary_gid));
    }

  /* hello-0 is still the version that's mounted at this point */
  executable = g_build_filename (f->bundle.static_path, "bin",
                                 from_version->flag_file, NULL);
  g_assert_true (g_file_test (executable, G_FILE_TEST_EXISTS));
  g_free (executable);

  if (getuid () == 0)
    g_assert_true (g_file_test (data_file_via_mount, G_FILE_TEST_EXISTS));

  /* Start upgrading to version 1 */
  if (install_api == TESTS_INSTALL_API_LOW_LEVEL)
    {
      unpack_to = tests_app_bundle_begin_install (&f->bundle,
                                                  from_version, to_version);

      g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==,
                        times_asked_to_terminate + 1);
      g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==,
                        times_asked_to_terminate + 1);

      /* The current subvolume has not been deleted yet, and is still
       * at the old version */
      g_assert_true (g_file_test (f->bundle.static_subvolume,
                                  G_FILE_TEST_IS_DIR));
      g_assert_true (g_file_test (f->bundle.variable_subvolume,
                                  G_FILE_TEST_IS_DIR));
      executable = g_build_filename (f->bundle.static_subvolume, "bin",
                                     from_version->flag_file, NULL);
      g_assert_true (g_file_test (executable, G_FILE_TEST_EXISTS));
      g_free (executable);
      /* The current subvolume also still has variable data */
      g_assert_true (g_file_test (data_file_internal, G_FILE_TEST_EXISTS));

      /* The rollback subvolume hasn't been updated yet: if it exists, it's
       * from a version older than the old version */
      executable = g_build_filename (f->bundle.rollback_static_subvolume, "bin",
                                     from_version->flag_file, NULL);
      g_assert_false (g_file_test (executable, G_FILE_TEST_EXISTS));
      g_free (executable);

      /* Nothing is mounted any more */
      bindir = g_build_filename (f->bundle.static_path, "bin", NULL);
      g_assert_false (g_file_test (bindir, G_FILE_TEST_EXISTS));
      g_free (bindir);
      g_assert_false (g_file_test (data_file_via_mount, G_FILE_TEST_EXISTS));

      /* In particular, hello-1 isn't in the mounted path */
      executable = g_build_filename (f->bundle.static_path, "bin",
                                     to_version->flag_file, NULL);
      g_assert_false (g_file_test (executable, G_FILE_TEST_EXISTS));
      g_free (executable);

      /* Install version 1 */
      fixture_install_app (f, from_version, to_version, install_api, stop_after,
                           unpack_to);

      /* We weren't asked to terminate it again */
      g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==,
                        times_asked_to_terminate + 1);
      g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==,
                        times_asked_to_terminate + 1);

      if (stop_after <= STOP_AFTER_UNPACK)
        {
          /* Version 0 is still in the active subvolume */
          executable = g_build_filename (f->bundle.static_subvolume, "bin",
                                         from_version->flag_file, NULL);
          g_assert_true (g_file_test (executable, G_FILE_TEST_EXISTS));
          g_free (executable);

          g_assert_true (g_file_test (data_file_internal, G_FILE_TEST_EXISTS));
          return;
        }
    }
  else
    {
      g_assert (install_api == TESTS_INSTALL_API_APP_STORE ||
                install_api == TESTS_INSTALL_API_BUNDLE_MANAGER1);
      g_assert_cmpint (stop_after, ==, STOP_NEVER);

      tests_app_bundle_install_simple (&f->bundle, install_api,
                                       from_version, to_version);

      g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==,
                        times_asked_to_terminate + 1);
      g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==,
                        times_asked_to_terminate + 1);
    }

  /* If we committed the transaction, version 1 is now active */
  executable = g_build_filename (f->bundle.static_subvolume, "bin",
                                 to_version->flag_file, NULL);
  g_assert_true (g_file_test (executable, G_FILE_TEST_EXISTS));
  g_free (executable);

  /* The old version is available through the rollback subvolume instead */
  executable = g_build_filename (f->bundle.rollback_static_subvolume, "bin",
                                 from_version->flag_file, NULL);
  g_assert_true (g_file_test (executable, G_FILE_TEST_EXISTS));
  g_free (executable);

  if (getuid () == 0)
    g_assert_true (g_file_test (data_file_internal, G_FILE_TEST_EXISTS));

  /* The old version isn't active any more, and we didn't copy its static
   * files into the new version */
  executable = g_build_filename (f->bundle.static_subvolume, "bin",
                                 from_version->flag_file, NULL);
  g_assert_false (g_file_test (executable, G_FILE_TEST_EXISTS));
  g_free (executable);

  /* The data is available in the current version */
  if (getuid () == 0)
    g_assert_true (g_file_test (data_file_internal, G_FILE_TEST_EXISTS));

  /* Also, it's mounted */
  executable = g_build_filename (f->bundle.static_path, "bin",
                                 to_version->flag_file, NULL);
  g_assert_true (g_file_test (executable, G_FILE_TEST_EXISTS));
  g_free (executable);
  executable = g_build_filename (f->bundle.static_path, "bin",
                                 from_version->flag_file, NULL);
  g_assert_false (g_file_test (executable, G_FILE_TEST_EXISTS));
  g_free (executable);

  if (getuid () == 0)
    g_assert_true (g_file_test (data_file_via_mount, G_FILE_TEST_EXISTS));

  /* Through the whole process, we were asked to terminate it exactly once */
  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==,
                    times_asked_to_terminate + 1);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==,
                    times_asked_to_terminate + 1);
}

static void
test_install (Fixture *f,
              gconstpointer user_data)
{
  const Config *config = user_data;
  g_autofree gchar *hello = NULL;
  const TestsVersion *version = &f->bundle.versions[0];

  /* early return if failed or skipped */
  if (g_test_failed ())
    return;

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 0);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 0);

  /* To install a non-empty app-bundle with the low-level interface we have
   * to be root */
  if (config->install_api == TESTS_INSTALL_API_LOW_LEVEL &&
      !tests_require_running_as_root ())
    return;

  fixture_install_app (f, NULL, version, config->install_api,
                       config->stop_after, NULL);
  hello = g_build_filename (f->bundle.static_path, "bin", version->flag_file,
                            NULL);

  if (config->stop_after < STOP_NEVER)
    {
      /* It isn't mounted yet */
      g_assert_false (g_file_test (hello, G_FILE_TEST_EXISTS));
    }
  else
    {
      /* The file is present if we have committed the transaction */
      g_assert_true (g_file_test (hello, G_FILE_TEST_EXISTS));
    }

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 1);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 1);

  /* Clean up: if we aborted early, this implicitly cancels the transaction */
  tests_app_bundle_remove (&f->bundle, config->install_api);

  if (config->stop_after < STOP_NEVER)
    {
      /* At the moment removing a bundle asks for termination again, even if
       * we never finished installing it, so these will be == 2; but allow
       * either way */
      g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, >=, 1);
      g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, <=, 2);
      g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, >=, 1);
      g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, <=, 2);
    }
  else
    {
      g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 2);
      g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 2);
    }
}

static void
test_cancel_install (Fixture *f,
                     gconstpointer user_data)
{
  const Config *config = user_data;
  g_autofree gchar *hello = NULL;
  const TestsVersion *version = &f->bundle.versions[0];

  /* if stop_after was STOP_NEVER, this would be a duplicate of
   * test_install() */
  g_assert (config->stop_after < STOP_NEVER);
  /* The high-level API can't be cancelled by RollBack() */
  g_assert (config->install_api == TESTS_INSTALL_API_LOW_LEVEL);

  /* early return if failed or skipped */
  if (g_test_failed ())
    return;

  /* Rolling back an upgrade currently requires administrator privileges */
  if (!tests_require_running_as_root ())
    return;

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 0);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 0);

  fixture_install_app (f, NULL, version, config->install_api,
                       config->stop_after, NULL);

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 1);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 1);

  /* It isn't mounted yet */
  hello = g_build_filename (f->bundle.static_path, "bin", version->flag_file,
                            NULL);
  g_assert_false (g_file_test (hello, G_FILE_TEST_EXISTS));

  /* Explicitly cancel the installation */
  tests_app_bundle_roll_back (&f->bundle, TESTS_ROLLBACK_MODE_CANCEL_INSTALL,
                              version, NULL);

  /* Calling TerminateBundle() when we roll back while the transaction is
   * open isn't strictly necessary. At the moment we don't, so this will
   * still be 1. */
  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, >=, 1);
  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, <=, 2);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, >=, 1);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, <=, 2);
}

static void
test_upgrade (Fixture *f,
              gconstpointer user_data)
{
  const Config *config = user_data;
  const TestsVersion *old_version = &f->bundle.versions[0];
  const TestsVersion *new_version = &f->bundle.versions[1];

  /* early return if failed or skipped */
  if (g_test_failed ())
    return;

  /* To install an app-bundle with the low-level interface we have
   * to be root */
  if (config->install_api == TESTS_INSTALL_API_LOW_LEVEL &&
      !tests_require_running_as_root ())
    return;

  fixture_install_app (f, NULL, old_version, TESTS_INSTALL_API_ANY, STOP_NEVER,
                       NULL);

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 1);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 1);

  fixture_upgrade (f, config->install_api, config->stop_after, old_version,
                   new_version);

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 2);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 2);

  /* Whatever point we stopped at, we should be able to remove the app
   * without rolling back, and it should get rid of all versions
   * (active, rollback, temporary). */
  tests_app_bundle_remove (&f->bundle, config->install_api);

  if (config->stop_after < STOP_NEVER)
    {
      /* Calling TerminateBundle() when we remove the bundle while the
       * upgrade transaction is open isn't strictly necessary, but at the
       * moment we do it anyway, so this will be 3 */
      g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, >=, 2);
      g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, <=, 3);
      g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, >=, 2);
      g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, <=, 3);
    }
  else
    {
      g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 3);
      g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 3);
    }

  g_assert_false (g_file_test (f->bundle.static_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (f->bundle.variable_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (f->bundle.rollback_static_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (f->bundle.rollback_variable_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (f->bundle.uninstalled_static_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (f->bundle.uninstalled_variable_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (f->bundle.temporary_subvolume,
                               G_FILE_TEST_EXISTS));
}

static void
test_rollback (Fixture *f,
               gconstpointer user_data)
{
  const Config *config = user_data;
  g_autofree gchar *data_file_via_mount = NULL;
  g_autofree gchar *contents = NULL;
  g_autofree gchar *executable = NULL;
  gboolean ok;
  GError *error = NULL;
  /* The old version we install first, and then roll back to */
  const TestsVersion *stable_version = &f->bundle.versions[0];
  /* The new version we upgrade to, and then roll back from */
  const TestsVersion *unstable_version = &f->bundle.versions[1];

  /* early return if failed or skipped */
  if (g_test_failed ())
    return;

  /* Rolling back currently requires administrative privileges */
  if (!tests_require_running_as_root ())
    return;

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 0);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 0);

  /* Before we can roll back, we need to upgrade. */
  fixture_install_app (f, NULL, stable_version, TESTS_INSTALL_API_ANY,
                       STOP_NEVER, NULL);

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 1);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 1);

  fixture_upgrade (f, config->install_api, config->stop_after,
                   stable_version, unstable_version);

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 2);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 2);

  if (g_test_failed ())
    return;

  data_file_via_mount = g_build_filename (f->bundle.variable_path, "users",
                                          f->uid_string, "hello.db", NULL);

  if (config->stop_after == STOP_NEVER)
    {
      /* The installation finished, and we use the app for a while.
       * Change the data (as though we had used the app) so that it will be
       * obvious that we've rolled back. */
      ok = g_file_set_contents (data_file_via_mount,
                                "th1s new ver5i0n c0rrupts my d4ta",
                                -1, &error);
      g_assert_no_error (error);
      g_assert_true (ok);
      /* We are not happy with version 1 and want to roll back to version 0 */
      tests_app_bundle_roll_back (&f->bundle, TESTS_ROLLBACK_MODE_ROLL_BACK,
                                  unstable_version, stable_version);

      g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 3);
      g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 3);
    }
  else
    {
      /* We're rolling back at some random point during installation:
       * whatever point that is, it should work. */
      tests_app_bundle_roll_back (&f->bundle,
                                  TESTS_ROLLBACK_MODE_CANCEL_INSTALL,
                                  unstable_version, stable_version);

      /* Calling TerminateBundle() on rollback while the upgrade transaction
       * is open would be OK; at the moment we don't do it, so this will
       * be 2 */
      g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, >=, 2);
      g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, <=, 3);
      g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, >=, 2);
      g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, <=, 3);
    }

  /* The old executable is back */
  g_free (executable);
  executable = g_build_filename (f->bundle.static_subvolume, "bin",
                                 stable_version->flag_file, NULL);
  g_assert_true (g_file_test (executable, G_FILE_TEST_EXISTS));

  /* The new executable isn't present */
  g_free (executable);
  executable = g_build_filename (f->bundle.static_subvolume, "bin",
                                 unstable_version->flag_file, NULL);
  g_assert_false (g_file_test (executable, G_FILE_TEST_EXISTS));

  /* The data file is back to its old (version 0) contents */
  g_file_get_contents (data_file_via_mount, &contents, NULL, &error);
  g_assert_no_error (error);
  g_assert_cmpstr (contents, ==, "here is my valuable data");

  /* Also, it's mounted */
  g_free (executable);
  executable = g_build_filename (f->bundle.static_path, "bin",
                                 stable_version->flag_file, NULL);
  g_assert_true (g_file_test (executable, G_FILE_TEST_EXISTS));

  g_free (executable);
  executable = g_build_filename (f->bundle.static_path, "bin",
                                 unstable_version->flag_file, NULL);
  g_assert_false (g_file_test (executable, G_FILE_TEST_EXISTS));

  g_assert_true (g_file_test (data_file_via_mount, G_FILE_TEST_EXISTS));

  /* Clean up */
  tests_app_bundle_remove (&f->bundle, config->install_api);

  if (config->stop_after == STOP_NEVER)
    {
      g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 4);
      g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 4);
    }
  else
    {
      /* Accept either 3 or 4 for the same reason we accepted either 2 or 3
       * above */
      g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, >=, 3);
      g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, <=, 4);
      g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, >=, 3);
      g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, <=, 4);
    }
}

static void
test_delete_rollback (Fixture *f,
                      gconstpointer user_data G_GNUC_UNUSED)
{
  /* The old version we install first, and then roll back to */
  const TestsVersion *old_version = &f->bundle.versions[0];
  /* The new version we upgrade to, and then roll back from */
  const TestsVersion *new_version = &f->bundle.versions[1];

  /* early return if failed or skipped */
  if (g_test_failed ())
    return;

  /* DeleteRollbackSnapshot() needs administrative privileges */
  if (!tests_require_running_as_root ())
    return;

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 0);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 0);

  fixture_install_app (f, NULL, old_version, TESTS_INSTALL_API_ANY,
                       STOP_NEVER, NULL);

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 1);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 1);

  fixture_upgrade (f, TESTS_INSTALL_API_ANY, STOP_NEVER, old_version,
                   new_version);

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 2);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 2);

  if (g_test_failed ())
    return;

  g_assert_true (g_file_test (f->bundle.rollback_static_subvolume,
                              G_FILE_TEST_IS_DIR));
  g_assert_true (g_file_test (f->bundle.rollback_variable_subvolume,
                              G_FILE_TEST_IS_DIR));

  tests_app_bundle_delete_rollback_snapshot (&f->bundle, old_version);

  g_assert_false (g_file_test (f->bundle.rollback_static_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (f->bundle.rollback_variable_subvolume,
                               G_FILE_TEST_EXISTS));

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 2);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 2);

  tests_app_bundle_remove (&f->bundle, TESTS_INSTALL_API_ANY);

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 3);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 3);
}

static void
test_uninstall (Fixture *f,
                gconstpointer user_data)
{
  const Config *config = user_data;
  g_autofree gchar *hello = NULL;
  g_autofree gchar *data_file = NULL;
  gboolean ok;
  GError *error = NULL;
  const TestsVersion *version = &f->bundle.versions[0];

  /* early return if failed or skipped */
  if (g_test_failed ())
    return;

  /* To install a non-empty app-bundle with the low-level interface we have
   * to be root */
  if (config->install_api == TESTS_INSTALL_API_LOW_LEVEL &&
      !tests_require_running_as_root ())
    return;

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 0);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 0);

  fixture_install_app (f, NULL, version, config->install_api, STOP_NEVER, NULL);

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 1);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 1);

  hello = g_build_filename (f->bundle.static_path, "bin",
                            version->flag_file, NULL);
  g_assert_true (g_file_test (hello, G_FILE_TEST_EXISTS));

  if (getuid () == 0)
    {
      /* Pretend we've used the app, by creating some data */
      tests_assert_syscall_0 (g_mkdir_with_parents (f->bundle.users_path,
                                                    0755));
      tests_assert_syscall_0 (g_mkdir_with_parents (f->bundle.persistence_path,
                                                    0700));
      tests_assert_syscall_0 (chown (f->bundle.persistence_path,
                                     f->ordinary_uid, f->ordinary_gid));
      data_file = g_build_filename (f->bundle.persistence_path, "hello.db",
                                    NULL);
      ok = g_file_set_contents (data_file, "here is my valuable data", -1,
                                &error);
      g_assert_no_error (error);
      g_assert_true (ok);
    }

  g_assert_true (tests_is_mount_point (f->bundle.static_path));
  g_assert_true (tests_is_mount_point (f->bundle.variable_path));
  g_assert_true (g_file_test (f->bundle.static_subvolume, G_FILE_TEST_IS_DIR));
  g_assert_true (g_file_test (f->bundle.variable_subvolume,
                              G_FILE_TEST_IS_DIR));
  g_assert_false (g_file_test (f->bundle.uninstalled_static_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (f->bundle.uninstalled_variable_subvolume,
                               G_FILE_TEST_EXISTS));

  /* Temporarily uninstall the app */
  tests_app_bundle_uninstall (&f->bundle, version);

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 2);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 2);

  g_assert_false (tests_is_mount_point (f->bundle.static_path));
  g_assert_false (tests_is_mount_point (f->bundle.variable_path));
  g_assert_false (g_file_test (hello, G_FILE_TEST_EXISTS));

  if (getuid () == 0)
    g_assert_false (g_file_test (data_file, G_FILE_TEST_EXISTS));

  g_assert_true (g_file_test (f->bundle.uninstalled_static_subvolume,
                              G_FILE_TEST_IS_DIR));
  g_assert_true (g_file_test (f->bundle.uninstalled_variable_subvolume,
                              G_FILE_TEST_IS_DIR));

  if (config->reinstall)
    {
      /* Bring it back from the dead */
      tests_app_bundle_reinstall (&f->bundle, version);

      g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 2);
      g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 2);

      g_assert_true (g_file_test (hello, G_FILE_TEST_EXISTS));

      if (getuid () == 0)
        g_assert_true (g_file_test (data_file, G_FILE_TEST_EXISTS));

      g_assert_true (tests_is_mount_point (f->bundle.static_path));
      g_assert_true (tests_is_mount_point (f->bundle.variable_path));
      g_assert_true (g_file_test (f->bundle.static_subvolume,
                                  G_FILE_TEST_IS_DIR));
      g_assert_true (g_file_test (f->bundle.variable_subvolume,
                                  G_FILE_TEST_IS_DIR));

      g_assert_false (g_file_test (f->bundle.uninstalled_static_subvolume,
                                   G_FILE_TEST_EXISTS));
      g_assert_false (g_file_test (f->bundle.uninstalled_variable_subvolume,
                                   G_FILE_TEST_EXISTS));
    }

  /* Remove it completely */
  tests_app_bundle_remove (&f->bundle, config->install_api);

  /* Calling TerminateBundle() during remove after uninstall isn't strictly
   * necessary, but at the moment we do it anyway, so this will be 3 */
  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, >=, 2);
  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, <=, 3);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, >=, 2);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, <=, 3);

  g_assert_false (g_file_test (f->bundle.uninstalled_static_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (f->bundle.uninstalled_variable_subvolume,
                               G_FILE_TEST_EXISTS));
}

static void
test_terminate_fails (Fixture *f,
                      gconstpointer user_data)
{
  const TestsVersion *version = &f->bundle.versions[0];
  g_autoptr (GAsyncResult) result = NULL;
  g_autoptr (GError) error = NULL;
  gboolean ok;

  /* early return if failed or skipped */
  if (g_test_failed ())
    return;

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 0);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 0);

  fixture_install_app (f, NULL, version, TESTS_INSTALL_API_ANY, STOP_NEVER,
                       NULL);

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 1);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 1);

  f->user_managers[0]->will_fail = FALSE;
  f->user_managers[1]->will_fail = TRUE;

  ribchester_bundle_manager1_call_remove_bundle (f->bundle_manager1_proxy,
                                                 f->bundle.id, NULL,
                                                 tests_store_result_cb,
                                                 &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 2);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 2);

  ok = ribchester_bundle_manager1_call_remove_bundle_finish (f->bundle_manager1_proxy,
                                                             result,
                                                             &error);
  g_assert_error (error, RC_ERROR, RC_ERROR_FAILED);
  g_assert_cmpint (ok, ==, FALSE);
  g_clear_object (&result);
  g_clear_error (&error);

  f->user_managers[0]->will_fail = TRUE;
  f->user_managers[1]->will_fail = FALSE;

  ribchester_bundle_manager1_call_remove_bundle (f->bundle_manager1_proxy,
                                                 f->bundle.id, NULL,
                                                 tests_store_result_cb,
                                                 &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 3);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 3);

  ok = ribchester_bundle_manager1_call_remove_bundle_finish (f->bundle_manager1_proxy,
                                                             result,
                                                             &error);
  g_assert_error (error, RC_ERROR, RC_ERROR_FAILED);
  g_assert_cmpint (ok, ==, FALSE);
  g_clear_object (&result);
  g_clear_error (&error);

  f->user_managers[0]->will_fail = FALSE;
  f->user_managers[1]->will_fail = FALSE;

  ribchester_bundle_manager1_call_remove_bundle (f->bundle_manager1_proxy,
                                                 f->bundle.id, NULL,
                                                 tests_store_result_cb,
                                                 &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  g_assert_cmpuint (f->user_managers[0]->times_asked_to_terminate, ==, 4);
  g_assert_cmpuint (f->user_managers[1]->times_asked_to_terminate, ==, 4);

  ok = ribchester_bundle_manager1_call_remove_bundle_finish (f->bundle_manager1_proxy,
                                                             result,
                                                             &error);
  g_assert_no_error (error);
  g_assert_cmpint (ok, ==, TRUE);
  g_clear_object (&result);
}

static void
teardown (Fixture *f,
          gconstpointer unused G_GNUC_UNUSED)
{
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (f->user_managers); i++)
    {
      if (f->user_managers[i] != NULL)
        tests_mock_user_manager_tear_down (f->user_managers[i]);

      g_clear_object (&f->user_managers[i]);
    }

  tests_app_bundle_tear_down (&f->bundle);
  g_clear_object (&f->app_store_proxy);
  g_clear_object (&f->bundle_manager1_proxy);
}

static const Config stop_after_begin =
{
  TESTS_INSTALL_API_LOW_LEVEL,
  STOP_AFTER_BEGIN,
};

static const Config stop_after_unpack =
{
  TESTS_INSTALL_API_LOW_LEVEL,
  STOP_AFTER_UNPACK,
};

static Config low_level =
{
  TESTS_INSTALL_API_LOW_LEVEL,
  STOP_NEVER,
};

static Config bundle_manager1 =
{
  TESTS_INSTALL_API_BUNDLE_MANAGER1,
  STOP_NEVER,
};

static Config app_store =
{
  TESTS_INSTALL_API_APP_STORE,
  STOP_NEVER,
};

static Config low_level_reinstall =
{
  TESTS_INSTALL_API_LOW_LEVEL,
  STOP_NEVER,
  .reinstall = TRUE,
};

static Config app_store_reinstall =
{
  TESTS_INSTALL_API_APP_STORE,
  STOP_NEVER,
  .reinstall = TRUE,
};

int
main (int argc,
    char **argv)
{
  /* Make sure the GDBus error enum is registered */
  (void) rc_error_quark ();

  g_test_init (&argc, &argv, NULL);

  g_test_add ("/install/low-level", Fixture, &low_level,
              setup, test_install, teardown);
  g_test_add ("/install/bundle-manager1", Fixture, &bundle_manager1,
              setup, test_install, teardown);
  g_test_add ("/install/app-store", Fixture, &app_store,
              setup, test_install, teardown);

  g_test_add ("/install/remove-after-begin", Fixture, &stop_after_begin,
              setup, test_install, teardown);
  g_test_add ("/install/remove-after-unpack", Fixture, &stop_after_unpack,
              setup, test_install, teardown);

  g_test_add ("/install/cancel-after-begin", Fixture, &stop_after_begin,
              setup, test_cancel_install, teardown);
  g_test_add ("/install/cancel-after-unpack", Fixture, &stop_after_unpack,
              setup, test_cancel_install, teardown);

  g_test_add ("/upgrade/low-level", Fixture, &low_level,
              setup, test_upgrade, teardown);
  g_test_add ("/upgrade/bundle-manager1", Fixture, &bundle_manager1,
              setup, test_upgrade, teardown);
  g_test_add ("/upgrade/app-store", Fixture, &app_store,
              setup, test_upgrade, teardown);
  g_test_add ("/upgrade/remove-after-begin", Fixture, &stop_after_begin,
              setup, test_upgrade, teardown);
  g_test_add ("/upgrade/remove-after-unpack", Fixture, &stop_after_unpack,
              setup, test_upgrade, teardown);

  g_test_add ("/upgrade-rollback/low-level", Fixture, &low_level,
              setup, test_rollback, teardown);
  g_test_add ("/upgrade-rollback/app-store", Fixture, &app_store,
              setup, test_rollback, teardown);
  g_test_add ("/upgrade-rollback/cancel-after-begin", Fixture,
              &stop_after_begin, setup, test_rollback, teardown);
  g_test_add ("/upgrade-rollback/cancel-after-unpack", Fixture,
              &stop_after_unpack, setup, test_rollback, teardown);

  g_test_add ("/uninstall/low-level", Fixture, &low_level,
              setup, test_uninstall, teardown);
  g_test_add ("/uninstall/app-store", Fixture, &app_store,
              setup, test_uninstall, teardown);
  g_test_add ("/reinstall/low-level", Fixture, &low_level_reinstall,
              setup, test_uninstall, teardown);
  g_test_add ("/reinstall/app-store", Fixture, &app_store_reinstall,
              setup, test_uninstall, teardown);

  /* Deleting the rollback snapshot requires use of the low-level API anyway
   * so there's no point in distinguishing here */
  g_test_add ("/delete-rollback", Fixture, &low_level,
              setup, test_delete_rollback, teardown);

  g_test_add ("/terminate-fails", Fixture, &bundle_manager1,
              setup, test_terminate_fails, teardown);

  return g_test_run ();
}
