/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef TESTS_COMMON_H
#define TESTS_COMMON_H

#include <errno.h>

#include <glib.h>
#include <gio/gio.h>

#include <canterbury/canterbury.h>

#include <ribchester/ribchester.h>

G_BEGIN_DECLS

#define TEST_BUNDLE_ID "org.apertis.Ribchester.TestAppBundle"

gboolean tests_is_mount_point (const gchar *mount_point);

const gchar *tests_printf (const gchar *format, ...) G_GNUC_PRINTF (1, 2);

void tests_store_result_cb (GObject *source_object,
                            GAsyncResult *result,
                            gpointer user_data);

/* Using string comparison so we get a better assertion message */
#define tests_assert_syscall_0(syscall) \
  g_assert_cmpstr ((syscall) != 0 ? g_strerror (errno) : "", ==, "")

typedef enum
{
  TESTS_ROLLBACK_MODE_CANCEL_INSTALL,
  TESTS_ROLLBACK_MODE_ROLL_BACK
} TestsRollbackMode;

typedef enum
{
  TESTS_INSTALL_API_LOW_LEVEL,
  TESTS_INSTALL_API_BUNDLE_MANAGER1,
  TESTS_INSTALL_API_APP_STORE,
  TESTS_INSTALL_API_ANY = TESTS_INSTALL_API_BUNDLE_MANAGER1,
} TestsInstallApi;

typedef struct
{
  /* Version number, e.g. 1.2.3 */
  gchar *number;
  /* Directory where this version is stored */
  gchar *directory;
  /* Name of a fake static file, e.g. hello-1.2.3 */
  gchar *flag_file;
  /* Root of where this bundle is stored on disk */
  gchar *storage_path;
  /* Subvolume in @storage_path for static files, such as @flag_file */
  gchar *static_subvolume;
  /* Subvolume in @storage_path for variable files */
  gchar *variable_subvolume;
} TestsVersion;

typedef struct
{
  CbyProcessInfo *process_info;
  gchar *id;

  gchar *storage_path;
  gchar *current_symlink;
  gchar *static_subvolume;
  gchar *static_path;
  gchar *old_current_symlink;

  gchar *variable_subvolume;
  gchar *variable_path;
  gchar *users_path;
  gchar *persistence_path;

  gchar *rollback_symlink;
  gchar *rollback_static_subvolume;
  gchar *rollback_variable_subvolume;

  gchar *old_rollback_symlink;
  gchar *old_rollback_static_subvolume;
  gchar *old_rollback_variable_subvolume;

  gchar *uninstalled_symlink;
  gchar *uninstalled_static_subvolume;
  gchar *uninstalled_variable_subvolume;

  gchar *installing_symlink;
  gchar *temporary_subvolume;

  TestsVersion *versions;
  gsize n_versions;

  RibchesterAppStore *app_store_proxy;
  RibchesterBundleManager1 *bundle_manager1_proxy;
  GDBusConnection *system_bus;
  const gchar *ribchester_bus_name;
} TestsAppBundle;

gchar *tests_app_bundle_begin_install_bundle (TestsAppBundle *self,
                                              TestsInstallApi install_api,
                                              const TestsVersion *from_version,
                                              const TestsVersion *to_version);
void tests_app_bundle_install_bundle (TestsAppBundle *self,
                                      TestsInstallApi install_api,
                                      const TestsVersion *from_version,
                                      const TestsVersion *to_version);
void tests_app_bundle_install_simple (TestsAppBundle *self,
                                      TestsInstallApi install_api,
                                      const TestsVersion *from_version,
                                      const TestsVersion *to_version);
gchar *tests_app_bundle_begin_install (TestsAppBundle *self,
                                       const TestsVersion *from_version,
                                       const TestsVersion *to_version);
void tests_app_bundle_commit_install (TestsAppBundle *self,
                                      const TestsVersion *from_version,
                                      const TestsVersion *to_version);
void tests_app_bundle_roll_back (TestsAppBundle *self,
                                 TestsRollbackMode mode,
                                 const TestsVersion *from_version,
                                 const TestsVersion *to_version);
void tests_app_bundle_uninstall (TestsAppBundle *self,
                                 const TestsVersion *version);
void tests_app_bundle_reinstall (TestsAppBundle *self,
                                 const TestsVersion *version);
void tests_app_bundle_remove (TestsAppBundle *self,
                              TestsInstallApi install_api);
gboolean tests_app_bundle_try_remove (TestsAppBundle *self,
                                      TestsInstallApi install_api,
                                      GError **error);
void tests_app_bundle_delete_rollback_snapshot (TestsAppBundle *self,
                                                const TestsVersion *version);

gboolean tests_app_bundle_set_up (TestsAppBundle *bundle,
                                  RibchesterAppStore *app_store_proxy,
                                  RibchesterBundleManager1 *bundle_manager1_proxy,
                                  guint ordinary_uid,
                                  const gchar * const *version_numbers,
                                  gsize n_versions);
void tests_app_bundle_tear_down (TestsAppBundle *bundle);

gboolean tests_require_ribchester (GDBusConnection **bus_out,
                                   RibchesterAppStore **app_store_proxy_out,
                                   RibchesterBundleManager1 **bundle_manager1_proxy_out);

gboolean tests_require_running_as_root (void);

gboolean tests_require_ordinary_uid (guint *ordinary_uid,
                                     guint *ordinary_gid,
                                     guint *unrelated_uid,
                                     guint *unrelated_gid);

G_END_DECLS

#endif
