/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * Call PrepareAppBundle as a subprocess. We do this as a subprocess,
 * because some of the main test's setup needs to be done as root.
 */

#include <stdlib.h>

#include <glib.h>
#include <glib/gstdio.h>
#include <gio/gio.h>

#include <canterbury/canterbury-platform.h>
#include <ribchester/ribchester.h>

int
main (int argc,
      char **argv)
{
  GError *error = NULL;
  GDBusConnection *bus = NULL;
  CbyPrivilegedAppHelper1 *proxy = NULL;
  gchar *returned_path = NULL;
  const gchar *bundle_id;
  guint flags;
  int ret = 1;

  if (argc != 3)
    {
      g_set_error (&error, G_OPTION_ERROR, G_OPTION_ERROR_FAILED,
                   "usage: prepare-app-bundle-helper com.example.Bundle 0");
      goto finally;
    }

  bus = g_bus_get_sync (G_BUS_TYPE_SYSTEM, NULL, &error);

  if (bus == NULL)
    goto finally;

  proxy = cby_privileged_app_helper1_proxy_new_sync (bus,
      G_DBUS_PROXY_FLAGS_NONE,
      "org.apertis.Ribchester",
      "/org/apertis/Canterbury/PrivilegedAppHelper1",
      NULL, &error);

  if (proxy == NULL)
    goto finally;

  /* We do not validate these, because the parent process is a test that
   * needs to exercise invalid as well as valid input to the D-Bus method. */
  bundle_id = argv[1];
  flags = atoi (argv[2]);

  /* Make sure RcError gets mapped */
  (void) RC_ERROR;

  if (!cby_privileged_app_helper1_call_prepare_app_bundle_sync (proxy,
      bundle_id, flags, &returned_path, NULL, &error))
    goto finally;

  g_print ("%s\n", returned_path);
  ret = 0;

finally:
  g_clear_object (&proxy);
  g_clear_object (&bus);
  g_free (returned_path);

  if (error != NULL)
    {
      gchar *error_name;

      /* We re-use D-Bus error names as a convenient way to transfer
       * machine-readable errors to our caller. */
      error_name = g_dbus_error_encode_gerror (error);
      g_print ("%s:%s\n", error_name, error->message);
      g_error_free (error);
      g_free (error_name);
    }

  return ret;
}
