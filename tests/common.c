/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "tests/common.h"

#include <fcntl.h>
#include <pwd.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <canterbury/canterbury.h>
#include <canterbury/canterbury-platform.h>
#include <gio/gio.h>
#include <gio/gunixfdlist.h>
#include <glib/gstdio.h>
#include <libmount.h>

#include <ribchester/ribchester.h>
#include "fdio.h"

/* These are hard-coded here rather than being imported from the main
 * Ribchester code, to make incompatible changes more visible */
#define GENERAL "/run/ribchester/general"

static const gchar *
get_app_bundles_path (void)
{
  static const gchar *s_app_bundles_path = NULL;

  if (s_app_bundles_path == NULL)
    {
      if (g_file_test ("/var/lib/ribchester", G_FILE_TEST_IS_DIR))
        s_app_bundles_path = "/var/lib/ribchester/app-bundles";
    }

  if (s_app_bundles_path == NULL)
    s_app_bundles_path = GENERAL "/app-bundles";

  return s_app_bundles_path;
}

static void
tests_assert_symlink_internal (const gchar *file,
                               gint line,
                               const gchar *symlink_description,
                               const gchar *symlink,
                               const gchar *points_to_description,
                               const gchar *points_to)
{
  g_autoptr (GError) error = NULL;
  g_autofree gchar *target = g_file_read_link (symlink, &error);

  if (error != NULL)
    {
      g_critical ("%s:%d: g_file_read_link (%s): %s", file, line,
                  symlink_description, error->message);
      g_test_fail ();
      return;
    }

  if (g_strcmp0 (target, points_to) != 0)
    {
      g_critical ("%s:%d: g_file_read_link (%s): was \"%s\", should be %s "
                  "\"%s\"",
                  file, line, symlink_description, symlink,
                  points_to_description, points_to);
      g_test_fail ();
      return;
    }
}

#define tests_assert_symlink(symlink, points_to) \
  tests_assert_symlink_internal (__FILE__, __LINE__, #symlink, symlink, \
                                 #points_to, points_to)

/**
 * Returns: %TRUE if something is mounted on @mount_point
 */
gboolean
tests_is_mount_point (const gchar *mount_point)
{
  gboolean ret;
  int err;
  struct libmnt_table *table;

  table = mnt_new_table ();

  if (table == NULL)
    g_error ("out of memory");

  err = mnt_table_parse_mtab (table, NULL);

  if (err < 0)
    g_error ("mnt_table_parse_mtab: %s", g_strerror (-err));

  ret = (mnt_table_find_target (table, mount_point, MNT_ITER_FORWARD) != NULL);

  mnt_unref_table (table);
  return ret;
}

/**
 * Format a string as if via g_strdup_printf() and mark it to be freed at the
 * end of the test.
 */
const gchar *
tests_printf (const gchar *format,
              ...)
{
  va_list ap;
  gchar *string;

  va_start (ap, format);
  string = g_strdup_vprintf (format, ap);
  va_end (ap);

  g_test_queue_free (string);
  return string;
}

/**
 * @source_object: unused
 * @result: an async result
 * @user_data: a `GAsyncResult **`
 *
 * A GAsyncReadyCallback that writes the @result through the
 * GAsyncResult ** @user_data.
 */
void
tests_store_result_cb (GObject *source_object G_GNUC_UNUSED,
    GAsyncResult *result,
    gpointer user_data)
{
  GAsyncResult **result_p = user_data;

  g_assert (*result_p == NULL);
  *result_p = g_object_ref (result);
}

/**
 * @ordinary_uid: the uid to use in bundle->persistence_path,
 *  or #CBY_PROCESS_INFO_NO_USER_ID to skip
 */
gboolean
tests_app_bundle_set_up (TestsAppBundle *bundle,
                         RibchesterAppStore *app_store_proxy,
                         RibchesterBundleManager1 *bundle_manager1_proxy,
                         guint ordinary_uid,
                         const gchar * const *version_numbers,
                         gsize n_versions)
{
  g_autoptr (GError) error = NULL;
  GDBusProxy *proxy;
  gsize i;

  /* root is not a suitable uid */
  g_assert (ordinary_uid > 0);

  bundle->id = g_strdup (TEST_BUNDLE_ID);
  bundle->app_store_proxy = g_object_ref (app_store_proxy);
  bundle->bundle_manager1_proxy = g_object_ref (bundle_manager1_proxy);
  proxy = G_DBUS_PROXY (app_store_proxy);
  bundle->system_bus = g_object_ref (g_dbus_proxy_get_connection (proxy));
  bundle->ribchester_bus_name = g_dbus_proxy_get_name (proxy);

  bundle->storage_path = g_build_filename (get_app_bundles_path (), bundle->id, NULL);
  bundle->current_symlink = g_build_filename (bundle->storage_path, "current",
                                              NULL);
  bundle->old_current_symlink = g_build_filename (bundle->storage_path,
                                                  "old-current", NULL);
  bundle->rollback_symlink = g_build_filename (bundle->storage_path, "rollback",
                                               NULL);
  bundle->old_rollback_symlink = g_build_filename (bundle->storage_path,
                                                   "old-rollback", NULL);
  bundle->installing_symlink = g_build_filename (bundle->storage_path,
                                                 "installing", NULL);
  bundle->uninstalled_symlink = g_build_filename (bundle->storage_path,
                                                  "uninstalled", NULL);

  bundle->static_subvolume =
    g_build_filename (get_app_bundles_path (), bundle->id, "current", "static", NULL);
  g_test_message ("Static subvolume: \"%s\"", bundle->static_subvolume);
  bundle->static_path = g_build_filename ("/Applications", bundle->id, NULL);
  g_test_message ("Static mount point: \"%s\"", bundle->static_path);

  bundle->variable_subvolume =
    g_build_filename (get_app_bundles_path (), bundle->id, "current", "variable", NULL);
  g_test_message ("Variable subvolume: \"%s\"", bundle->variable_subvolume);

  bundle->variable_path = g_build_filename ("/var/Applications", bundle->id,
                                            NULL);
  g_test_message ("Variable mount point: \"%s\"", bundle->variable_path);
  bundle->users_path = g_build_filename (bundle->variable_path, "users", NULL);
  g_test_message ("Users directory: \"%s\"", bundle->users_path);

  bundle->versions = g_new0 (TestsVersion, n_versions);
  bundle->n_versions = n_versions;

  for (i = 0; i < n_versions; i++)
    {
      const gchar *number = version_numbers[i];
      TestsVersion *version = &bundle->versions[i];

      version->number = g_strdup (number);
      version->directory = g_strdup_printf ("version-%s", number);
      version->storage_path = g_build_filename (bundle->storage_path,
                                                version->directory, NULL);
      version->flag_file = g_strdup_printf ("hello-%s", number);
      version->static_subvolume = g_build_filename (version->storage_path,
                                                    "static", NULL);
      version->variable_subvolume = g_build_filename (version->storage_path,
                                                      "variable", NULL);
    }

  /* For application bundle rollback */
  bundle->rollback_static_subvolume =
    g_build_filename (get_app_bundles_path (), bundle->id, "rollback", "static", NULL);
  bundle->rollback_variable_subvolume =
    g_build_filename (get_app_bundles_path (), bundle->id, "rollback", "variable", NULL);

  /* Temporarily exists while committing */
  bundle->old_rollback_static_subvolume =
    g_build_filename (get_app_bundles_path (), bundle->id, "old-rollback", "static", NULL);
  bundle->old_rollback_variable_subvolume =
    g_build_filename (get_app_bundles_path (), bundle->id, "old-rollback", "variable",
                      NULL);

  /* For temporary uninstallation */
  bundle->uninstalled_static_subvolume =
    g_build_filename (get_app_bundles_path (), bundle->id, "uninstalled", "static", NULL);
  bundle->uninstalled_variable_subvolume =
    g_build_filename (get_app_bundles_path (), bundle->id, "uninstalled", "variable", NULL);

  /* Used during installation. There is no "variable" equivalent because
   * installation touches exactly the static part. */
  bundle->temporary_subvolume =
    g_build_filename (get_app_bundles_path (), bundle->id, "installing", "static", NULL);

  /* We can only have a persistence path if we have a useful uid. */
  if (ordinary_uid == CBY_PROCESS_INFO_NO_USER_ID)
    {
      bundle->persistence_path = NULL;
      g_test_message ("No uid supplied, so not creating a persistence path");
    }
  else
    {
      bundle->persistence_path = g_strdup_printf ("%s/%u", bundle->users_path,
                                                  ordinary_uid);
      g_test_message ("Persistence path for uid %u: \"%s\"", ordinary_uid,
                      bundle->persistence_path);
    }

  bundle->process_info =
    cby_process_info_new_for_path_and_user (bundle->static_path, ordinary_uid);
  g_assert_cmpuint (cby_process_info_get_user_id (bundle->process_info), ==,
                    ordinary_uid);
  g_assert_cmpstr (cby_process_info_get_bundle_id (bundle->process_info), ==,
                   bundle->id);

  /* If the uid is not known, this is asserting NULL == NULL - still
   * true. */
  g_assert_cmpstr (
      cby_process_info_get_persistence_path (bundle->process_info), ==,
      bundle->persistence_path);

  if (!g_file_test ("/Applications", G_FILE_TEST_IS_DIR))
    {
      g_test_message ("/Applications is not a directory");
      g_test_fail ();
      return FALSE;
    }

  if (!g_file_test ("/var/Applications", G_FILE_TEST_IS_DIR))
    {
      g_test_message ("/var/Applications is not a directory");
      g_test_fail ();
      return FALSE;
    }

  if (!tests_app_bundle_try_remove (bundle, TESTS_INSTALL_API_ANY, &error))
    {
      if (g_error_matches (error, G_DBUS_ERROR, G_DBUS_ERROR_ACCESS_DENIED))
        {
          /* We can't do app-bundle-based tests as this user in this state
           * (most likely they are not considered to be actively logged-in
           * because we are using su to switch from root to an ordinary
           * user) */
          g_test_skip ("AccessDenied error while trying to remove app-bundle");
          return FALSE;
        }
      else
        {
          /* Other errors are fatal */
          g_test_message ("Unable to remove app-bundle: %s:%d: %s",
                          g_quark_to_string (error->domain), error->code,
                          error->message);
          g_test_fail ();
          return FALSE;
        }
    }

  return TRUE;
}

void
tests_app_bundle_tear_down (TestsAppBundle *self)
{
  gsize i;

  if (self->app_store_proxy != NULL)
    {
      g_autoptr (GError) error = NULL;

      /* Just ignore AccessDenied here: if we weren't allowed to remove it
       * then we wouldn't have been allowed to install it either */
      if (!tests_app_bundle_try_remove (self, TESTS_INSTALL_API_ANY, &error) &&
          !g_error_matches (error, G_DBUS_ERROR, G_DBUS_ERROR_ACCESS_DENIED))
        {
          g_test_message ("Unable to remove app-bundle: %s:%d: %s",
                          g_quark_to_string (error->domain), error->code,
                          error->message);
          g_test_fail ();
        }
    }

  /* Check that we haven't leaked any mounts */
  g_assert_false (tests_is_mount_point (self->static_path));
  g_assert_false (tests_is_mount_point (self->variable_path));

  /* Check that we haven't leaked any subvolumes or directories */
  g_assert_false (g_file_test (self->current_symlink,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->old_current_symlink,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->static_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->variable_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->rollback_symlink,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->rollback_static_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->rollback_variable_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->old_rollback_symlink,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->old_rollback_static_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->old_rollback_variable_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->uninstalled_symlink,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->uninstalled_static_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->uninstalled_variable_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->installing_symlink,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->temporary_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->storage_path,
                               G_FILE_TEST_EXISTS));

  for (i = 0; i < self->n_versions; i++)
    {
      TestsVersion *version = &self->versions[i];

      g_assert_false (g_file_test (version->variable_subvolume,
                                   G_FILE_TEST_EXISTS));
      g_assert_false (g_file_test (version->static_subvolume,
                                   G_FILE_TEST_EXISTS));
      g_assert_false (g_file_test (version->storage_path, G_FILE_TEST_EXISTS));

      g_clear_pointer (&version->number, g_free);
      g_clear_pointer (&version->directory, g_free);
      g_clear_pointer (&version->storage_path, g_free);
      g_clear_pointer (&version->static_subvolume, g_free);
      g_clear_pointer (&version->variable_subvolume, g_free);
    }

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->storage_path, g_free);
  g_clear_pointer (&self->current_symlink, g_free);
  g_clear_pointer (&self->old_current_symlink, g_free);
  g_clear_pointer (&self->static_subvolume, g_free);
  g_clear_pointer (&self->static_path, g_free);
  g_clear_pointer (&self->variable_subvolume, g_free);
  g_clear_pointer (&self->variable_path, g_free);
  g_clear_pointer (&self->users_path, g_free);
  g_clear_pointer (&self->persistence_path, g_free);
  g_clear_pointer (&self->rollback_symlink, g_free);
  g_clear_pointer (&self->rollback_static_subvolume, g_free);
  g_clear_pointer (&self->rollback_variable_subvolume, g_free);
  g_clear_pointer (&self->old_rollback_symlink, g_free);
  g_clear_pointer (&self->old_rollback_static_subvolume, g_free);
  g_clear_pointer (&self->old_rollback_variable_subvolume, g_free);
  g_clear_pointer (&self->uninstalled_symlink, g_free);
  g_clear_pointer (&self->uninstalled_static_subvolume, g_free);
  g_clear_pointer (&self->uninstalled_variable_subvolume, g_free);
  g_clear_pointer (&self->installing_symlink, g_free);
  g_clear_pointer (&self->temporary_subvolume, g_free);
  g_clear_object (&self->process_info);
  g_clear_object (&self->app_store_proxy);
}

static void
ribchester_appeared_cb (GDBusConnection *connection,
                        const gchar *name,
                        const gchar *owner,
                        gpointer user_data)
{
  gboolean *done = user_data;

  g_test_message ("Found Ribchester at \"%s\"", owner);
  *done = TRUE;
}

static void
ribchester_vanished_cb (GDBusConnection *connection,
                        const gchar *name,
                        gpointer user_data)
{
  gboolean *done = user_data;

  g_test_message ("Ribchester is not running, try: sudo systemctl start "
                  "ribchester.service");
  g_test_fail ();
  *done = TRUE;
}

/**
 * @bus_out: (out) (transfer none) (optional): the connection to the system bus
 * @app_store_proxy_out: (out) (transfer none) (optional): the AppStore
 *  interface proxy
 * @bundle_manager_proxy_out: (out) (transfer none) (optional): the
 *  BundleManager1 interface proxy
 */
gboolean
tests_require_ribchester (GDBusConnection **bus_out,
                          RibchesterAppStore **app_store_proxy_out,
                          RibchesterBundleManager1 **bundle_manager1_proxy_out)
{
  GError *error = NULL;
  g_autoptr (GAsyncResult) result = NULL;
  g_autoptr (GDBusConnection) bus = NULL;
  g_autoptr (RibchesterAppStore) app_store_proxy = NULL;
  g_autoptr (RibchesterBundleManager1) bundle_manager1_proxy = NULL;
  guint watch_id;
  gboolean done = FALSE;

  bus = g_bus_get_sync (G_BUS_TYPE_SYSTEM, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (G_IS_DBUS_CONNECTION (bus));

  watch_id = g_bus_watch_name (G_BUS_TYPE_SYSTEM, "org.apertis.Ribchester",
                               G_BUS_NAME_WATCHER_FLAGS_NONE,
                               ribchester_appeared_cb, ribchester_vanished_cb,
                               &done, NULL);

  while (!done)
    g_main_context_iteration (NULL, TRUE);

  g_bus_unwatch_name (watch_id);

  /* if we couldn't get the name, give up */
  if (g_test_failed ())
    return FALSE;

  ribchester_app_store_proxy_new (bus, G_DBUS_PROXY_FLAGS_NONE,
                                  RC_BUS_NAME,
                                  "/org/apertis/Ribchester/AppStore",
                                  NULL, tests_store_result_cb, &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  app_store_proxy = ribchester_app_store_proxy_new_finish (result, &error);
  g_assert_no_error (error);
  g_assert_true (RIBCHESTER_IS_APP_STORE_PROXY (app_store_proxy));
  g_clear_object (&result);

  ribchester_bundle_manager1_proxy_new (bus, G_DBUS_PROXY_FLAGS_NONE,
                                        RC_BUS_NAME,
                                        RC_OBJECT_PATH_BUNDLE_MANAGER1,
                                        NULL, tests_store_result_cb, &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  bundle_manager1_proxy = ribchester_bundle_manager1_proxy_new_finish (result,
                                                                       &error);
  g_assert_no_error (error);
  g_assert_true (RIBCHESTER_IS_BUNDLE_MANAGER1_PROXY (bundle_manager1_proxy));
  g_clear_object (&result);

  if (bus_out != NULL)
    *bus_out = g_steal_pointer (&bus);

  if (app_store_proxy_out != NULL)
    *app_store_proxy_out = g_steal_pointer (&app_store_proxy);

  if (bundle_manager1_proxy_out != NULL)
    *bundle_manager1_proxy_out = g_steal_pointer (&bundle_manager1_proxy);

  return TRUE;
}

gboolean
tests_require_running_as_root (void)
{
  if (getuid () != 0)
   {
     g_test_skip ("This test requires root privileges");
     return FALSE;
   }

  return TRUE;
}

/**
 * If we are running as an ordinary user, put our own uid and gid
 * in @ordinary_uid and @ordinary_gid. If we are running as root (uid 0),
 * put the uid and gid of `user` there instead.
 *
 * Either way, put the uid and gid of an unused non-root user (`bin`)
 * in @unrelated_uid and @unrelated_gid.
 *
 * If we do not seem to be running on Apertis, mark the test skipped
 * and return %FALSE.
 */
gboolean
tests_require_ordinary_uid (guint *ordinary_uid,
                            guint *ordinary_gid,
                            guint *unrelated_uid,
                            guint *unrelated_gid)
{
  uid_t uid = getuid ();
  struct passwd *pwd;

  /* We make this assumption here, as does Canterbury */
  G_STATIC_ASSERT (sizeof (uid_t) <= sizeof (guint));

  if (unrelated_uid != NULL || unrelated_gid != NULL)
    {
      pwd = getpwnam ("bin");

      /* Apertis is a Debian derivative, and user 'bin' has uid 2
       * (this is centrally allocated in base-passwd). 'bin' is chosen to be
       * a harmless user that cannot actually log in. */
      if (pwd == NULL || pwd->pw_uid != 2)
        {
          g_test_skip ("This does not appear to be an Apertis environment");
          return FALSE;
        }

      g_assert_cmpint (pwd->pw_uid, >, 0);
      g_assert_cmpint (pwd->pw_uid, !=, CBY_PROCESS_INFO_NO_USER_ID);
      g_assert_cmpint (pwd->pw_gid, >, 0);

      if (unrelated_uid != NULL)
        *unrelated_uid = pwd->pw_uid;

      if (unrelated_gid != NULL)
        *unrelated_gid = pwd->pw_gid;
    }

  if (uid == 0)
    {
      pwd = getpwnam ("user");

      /* This is hard-coded in Apertis image creation. */
      if (pwd == NULL || pwd->pw_uid != 1000)
        {
          g_test_skip ("This does not appear to be an Apertis environment");
          return FALSE;
        }

      g_assert_cmpint (pwd->pw_uid, >, 0);
      g_assert_cmpint (pwd->pw_uid, !=, CBY_PROCESS_INFO_NO_USER_ID);
      g_assert_cmpint (pwd->pw_gid, >, 0);

      if (ordinary_uid != NULL)
        *ordinary_uid = (guint) pwd->pw_uid;

      if (ordinary_gid != NULL)
        *ordinary_gid = (guint) pwd->pw_gid;
    }
  else
    {
      gid_t gid = getgid ();

      g_assert_cmpint (uid, >, 0);
      g_assert_cmpint (uid, !=, CBY_PROCESS_INFO_NO_USER_ID);
      g_assert_cmpint (gid, >, 0);

      if (ordinary_uid != NULL)
        *ordinary_uid = (guint) uid;

      if (ordinary_gid != NULL)
        *ordinary_gid = (guint) gid;
    }

  return TRUE;
}

/*
 * tests_app_bundle_begin_install_bundle:
 * @self: The app-bundle
 * @install_api: How to install the bundle
 * @from_version: (nullable): the version to upgrade from, or %NULL for a
 *  new installation
 * @to_version: the version to install or upgrade to
 *
 * Install the app-bundle from a .bundle file. The caller is expected to wait
 * for the resulting transaction to finish.
 *
 * Returns: the object path of the transaction
 */
gchar *
tests_app_bundle_begin_install_bundle (TestsAppBundle *self,
                                       TestsInstallApi install_api,
                                       const TestsVersion *from_version,
                                       const TestsVersion *to_version)
{
  g_autoptr (GAsyncResult) result = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GUnixFDList) fd_list = NULL;
  g_autofree gchar *basename =
    g_strconcat (self->id, "-", to_version->number, ".bundle", NULL);
  g_autofree gchar *bundle =
    g_test_build_filename (G_TEST_BUILT, "tests", basename, NULL);
  g_autofree gchar *path = NULL;
  g_auto (RcFileDescriptor) fd = -1;
  gint handle;
  gboolean ok;

  fd = open (bundle, O_RDONLY | O_CLOEXEC);
  /* Using string comparison here so we get a better assertion message */
  g_assert_cmpstr (fd >= 0 ? NULL : g_strerror (errno), ==, NULL);

  fd_list = g_unix_fd_list_new ();
  handle = g_unix_fd_list_append (fd_list, fd, NULL);
  g_assert_cmpint (handle, ==, 0);

  if (install_api == TESTS_INSTALL_API_BUNDLE_MANAGER1)
    {
      ribchester_bundle_manager1_call_install_bundle (self->bundle_manager1_proxy,
                                                      g_variant_new_handle (handle),
                                                      fd_list,
                                                      NULL,
                                                      tests_store_result_cb,
                                                      &result);

      while (result == NULL)
        g_main_context_iteration (NULL, TRUE);

      ok = ribchester_bundle_manager1_call_install_bundle_finish (self->bundle_manager1_proxy,
                                                                  &path,
                                                                  NULL,
                                                                  result,
                                                                  &error);
    }
  else
    {
      g_assert (install_api == TESTS_INSTALL_API_APP_STORE);

      ribchester_app_store_call_install_app_bundle (self->app_store_proxy,
                                                    g_variant_new_handle (handle),
                                                    fd_list,
                                                    NULL,
                                                    tests_store_result_cb,
                                                    &result);

      while (result == NULL)
        g_main_context_iteration (NULL, TRUE);

      ok = ribchester_app_store_call_install_app_bundle_finish (self->app_store_proxy,
                                                                &path,
                                                                NULL,
                                                                result,
                                                                &error);
    }

  g_assert_no_error (error);
  g_assert_cmpint (ok, ==, TRUE);
  g_assert_nonnull (path);

  return g_steal_pointer (&path);
}

/*
 * tests_app_bundle_install_bundle:
 * @self: The app-bundle
 * @install_api: How to install the app-bundle
 * @from_version: (nullable): the version to upgrade from, or %NULL for a
 *  new installation
 * @to_version: the version to install or upgrade to
 *
 * Install the app-bundle from a .bundle file and wait for the resulting
 * transaction to succeed.
 */
void
tests_app_bundle_install_bundle (TestsAppBundle *self,
                                 TestsInstallApi install_api,
                                 const TestsVersion *from_version,
                                 const TestsVersion *to_version)
{
  g_autoptr (GError) error = NULL;
  g_autofree gchar *path = NULL;
  g_autofree gchar *bundle_id = NULL;
  g_autofree gchar *got_version = NULL;
  g_autoptr (RibchesterAppStoreTransaction) transaction = NULL;
  RibchesterAppStoreTransactionStage stage;

  path = tests_app_bundle_begin_install_bundle (self, install_api,
                                                from_version, to_version);
  g_assert (path != NULL);

  transaction =
    ribchester_app_store_transaction_proxy_new_sync (self->system_bus,
                                                     G_DBUS_PROXY_FLAGS_GET_INVALIDATED_PROPERTIES,
                                                     self->ribchester_bus_name,
                                                     path, NULL, &error);
  g_assert_nonnull (transaction);
  g_assert_no_error (error);

  g_object_get (transaction,
                "stage", &stage,
                NULL);
  g_assert_cmpint (stage, >=, 0);

  while (stage >= 0 &&
         stage != RIBCHESTER_APP_STORE_TRANSACTION_STAGE_SUCCEEDED)
    {
      g_main_context_iteration (NULL, TRUE);
      g_object_get (transaction,
                    "stage", &stage,
                    NULL);
    }

  g_assert_cmpint (stage, ==,
                   RIBCHESTER_APP_STORE_TRANSACTION_STAGE_SUCCEEDED);

  g_object_get (transaction,
                "bundle-id", &bundle_id,
                "bundle-version", &got_version,
                NULL);
  g_assert_cmpstr (bundle_id, ==, self->id);
  g_assert_cmpstr (got_version, ==, to_version->number);

  /* For simplicity, this version of app installation doesn't wait
   * for the object to disappear from the bus. tests/install-app-bundle.c
   * exercises this in more detail.
   *
   * This version also deliberately omits the call to Release(), so that
   * we can check that the code path that waits for the unique name to
   * disappear works correctly. */
}

void
tests_app_bundle_install_simple (TestsAppBundle *self,
                                 TestsInstallApi install_api,
                                 const TestsVersion *from_version,
                                 const TestsVersion *to_version)
{
  if (install_api == TESTS_INSTALL_API_LOW_LEVEL)
    {
      g_autofree gchar *unpack_to = NULL;

      unpack_to = tests_app_bundle_begin_install (self, from_version,
                                                  to_version);
      tests_app_bundle_commit_install (self, from_version, to_version);
    }
  else
    {
      tests_app_bundle_install_bundle (self, install_api, from_version,
                                       to_version);
    }
}

/*
 * @from_version: (nullable): the version we are upgrading from, or %NULL
 *  if this is an initial installation
 *
 * Returns: (transfer full):
 */
gchar *
tests_app_bundle_begin_install (TestsAppBundle *self,
                                const TestsVersion *from_version,
                                const TestsVersion *to_version)
{
  g_autoptr (GAsyncResult) result = NULL;
  g_autofree gchar *unpack_to = NULL;
  GError *error = NULL;
  gboolean succeeded;

  g_return_val_if_fail (to_version != NULL, NULL);

  if (from_version != NULL)
    {
      g_test_message ("Beginning upgrade of \"%s\" from version \"%s\" to "
                      "\"%s\"...",
                      self->id, from_version->number, to_version->number);

      tests_assert_symlink (self->current_symlink, from_version->directory);
      g_assert_true (g_file_test (self->static_subvolume, G_FILE_TEST_IS_DIR));
      g_assert_true (g_file_test (self->variable_subvolume,
                                  G_FILE_TEST_IS_DIR));
    }
  else
    {
      g_test_message ("Beginning first installation of \"%s\", version "
                      "\"%s\"...",
                      self->id, to_version->number);

      g_assert_false (g_file_test (self->current_symlink, G_FILE_TEST_EXISTS));
      g_assert_false (g_file_test (self->static_subvolume, G_FILE_TEST_EXISTS));
      g_assert_false (g_file_test (self->variable_subvolume,
                                   G_FILE_TEST_EXISTS));
      g_assert_false (g_file_test (self->rollback_static_subvolume,
                                   G_FILE_TEST_EXISTS));
      g_assert_false (g_file_test (self->rollback_variable_subvolume,
                                   G_FILE_TEST_EXISTS));
    }

  ribchester_app_store_call_begin_install (self->app_store_proxy, self->id,
                                           to_version->number,
                                           0, NULL, tests_store_result_cb,
                                           &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  succeeded = ribchester_app_store_call_begin_install_finish (self->app_store_proxy,
                                                              &unpack_to,
                                                              result, &error);
  g_assert_no_error (error);
  g_assert_cmpint (succeeded, ==, TRUE);

  g_test_message ("... created snapshot, expecting new app in \"%s\"",
                  unpack_to);
  g_assert_cmpstr (unpack_to, ==, to_version->static_subvolume);

  if (from_version != NULL)
    {
      /* Unlike the older UpgradeApp(), BeginInstall() does not remove the
       * active version or create a new rollback version. */

      tests_assert_symlink (self->current_symlink, from_version->directory);
    }
  else
    {
      /* The active subvolumes haven't magically appeared */
      g_assert_false (g_file_test (self->current_symlink, G_FILE_TEST_EXISTS));
      g_assert_false (g_file_test (self->static_subvolume, G_FILE_TEST_EXISTS));
      g_assert_false (g_file_test (self->variable_subvolume,
                                   G_FILE_TEST_EXISTS));
    }

  tests_assert_symlink (self->installing_symlink, to_version->directory);

  /* It also creates the unpack destination for you */
  g_assert_true (g_file_test (unpack_to, G_FILE_TEST_IS_DIR));

  return g_steal_pointer (&unpack_to);
}

void
tests_app_bundle_commit_install (TestsAppBundle *self,
                                 const TestsVersion *from_version,
                                 const TestsVersion *to_version)
{
  g_autoptr (GAsyncResult) result = NULL;
  GError *error = NULL;
  gboolean succeeded;

  g_return_if_fail (to_version != NULL);

  g_test_message ("Committing installation of \"%s\" version \"%s\"...",
                  self->id, to_version->number);

  if (from_version != NULL)
    {
      tests_assert_symlink (self->current_symlink, from_version->directory);

      g_assert_true (g_file_test (self->static_subvolume,
                                  G_FILE_TEST_IS_DIR));
      g_assert_true (g_file_test (self->variable_subvolume,
                                  G_FILE_TEST_IS_DIR));
    }
  else
    {
      g_assert_false (g_file_test (self->static_subvolume,
                                   G_FILE_TEST_EXISTS));
      g_assert_false (g_file_test (self->variable_subvolume,
                                   G_FILE_TEST_EXISTS));
      g_assert_false (g_file_test (self->rollback_static_subvolume,
                                   G_FILE_TEST_EXISTS));
      g_assert_false (g_file_test (self->rollback_variable_subvolume,
                                   G_FILE_TEST_EXISTS));
    }

  tests_assert_symlink (self->installing_symlink, to_version->directory);
  g_assert_true (g_file_test (self->temporary_subvolume, G_FILE_TEST_IS_DIR));

  ribchester_app_store_call_commit_install (self->app_store_proxy, self->id,
                                            to_version->number,
                                            NULL, tests_store_result_cb,
                                            &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  succeeded = ribchester_app_store_call_commit_install_finish (self->app_store_proxy,
                                                               result, &error);
  g_assert_no_error (error);
  g_assert_cmpint (succeeded, ==, TRUE);

  g_test_message ("... done");

  tests_assert_symlink (self->current_symlink, to_version->directory);
  g_assert_true (g_file_test (self->static_subvolume, G_FILE_TEST_IS_DIR));
  g_assert_true (g_file_test (self->variable_subvolume, G_FILE_TEST_IS_DIR));
  g_assert_false (g_file_test (self->temporary_subvolume, G_FILE_TEST_EXISTS));

  if (from_version != NULL)
    {
      tests_assert_symlink (self->rollback_symlink, from_version->directory);
      g_assert_true (g_file_test (self->rollback_static_subvolume,
                                  G_FILE_TEST_IS_DIR));
      g_assert_true (g_file_test (self->rollback_variable_subvolume,
                                  G_FILE_TEST_IS_DIR));
    }
  else
    {
      g_assert_false (g_file_test (self->rollback_static_subvolume,
                                   G_FILE_TEST_EXISTS));
      g_assert_false (g_file_test (self->rollback_variable_subvolume,
                                   G_FILE_TEST_EXISTS));
    }

  /* This only exists briefly, and has been cleaned up by the time the
   * method returns. */
  g_assert_false (g_file_test (self->old_rollback_static_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->old_rollback_variable_subvolume,
                               G_FILE_TEST_EXISTS));
}

void
tests_app_bundle_roll_back (TestsAppBundle *self,
                            TestsRollbackMode mode,
                            const TestsVersion *from_version,
                            const TestsVersion *to_version)
{
  g_autoptr (GAsyncResult) result = NULL;
  g_autofree gchar *returned_to = NULL;
  gboolean ok;
  GError *error = NULL;

  g_return_if_fail (from_version != NULL);

  /* We can either roll back an installation that is in progress (the
   * temporary subvolume exists), or an upgrade that was already committed
   * (the rollback subvolumes exist) */
  switch (mode)
    {
    case TESTS_ROLLBACK_MODE_CANCEL_INSTALL:
      g_test_message ("Cancelling installation of \"%s\"...", self->id);
      g_assert_true (g_file_test (self->temporary_subvolume,
                                  G_FILE_TEST_IS_DIR));
      /* The rollback subvolumes may or may not exist, we can't say
       * either way */
      break;

    case TESTS_ROLLBACK_MODE_ROLL_BACK:
      g_assert_cmpint (mode, ==, TESTS_ROLLBACK_MODE_ROLL_BACK);
      g_assert_true (to_version != NULL);

      g_test_message ("Rolling back \"%s\" to previous version...", self->id);
      g_assert_false (g_file_test (self->temporary_subvolume,
                                   G_FILE_TEST_EXISTS));
      g_assert_true (g_file_test (self->rollback_static_subvolume,
                                  G_FILE_TEST_IS_DIR));
      g_assert_true (g_file_test (self->rollback_variable_subvolume,
                                  G_FILE_TEST_IS_DIR));
      break;

    default:
      g_return_if_reached ();
    }

  ribchester_app_store_call_roll_back (self->app_store_proxy, self->id,
                                       NULL, tests_store_result_cb,
                                       &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  ok = ribchester_app_store_call_roll_back_finish (self->app_store_proxy,
                                                   &returned_to, result,
                                                   &error);

  g_assert_no_error (error);
  g_assert_cmpint (ok, ==, TRUE);
  g_clear_object (&result);

  switch (mode)
    {
    case TESTS_ROLLBACK_MODE_CANCEL_INSTALL:

      if (to_version == NULL)
        {
          g_assert_cmpstr (returned_to, ==, "");
          g_test_message ("... aborted installation");

          g_assert_false (g_file_test (self->current_symlink,
                                       G_FILE_TEST_EXISTS));
          g_assert_false (g_file_test (self->static_subvolume,
                                       G_FILE_TEST_EXISTS));
          g_assert_false (g_file_test (self->variable_subvolume,
                                       G_FILE_TEST_EXISTS));
          g_assert_false (g_file_test (self->rollback_static_subvolume,
                                       G_FILE_TEST_EXISTS));
          g_assert_false (g_file_test (self->rollback_variable_subvolume,
                                       G_FILE_TEST_EXISTS));
          g_assert_false (g_file_test (self->storage_path, G_FILE_TEST_EXISTS));
        }
      else
        {
          g_assert_cmpstr (returned_to, ==, to_version->number);
          g_test_message ("... aborted upgrade and returned to \"%s\"",
                          returned_to);

          tests_assert_symlink (self->current_symlink, to_version->directory);
          g_assert_true (g_file_test (self->static_subvolume,
                                      G_FILE_TEST_IS_DIR));
          g_assert_true (g_file_test (self->variable_subvolume,
                                      G_FILE_TEST_IS_DIR));
          /* We cannot say whether there will be a rollback subvolume: it
           * depends whether there was one before. */
        }
      break;

    case TESTS_ROLLBACK_MODE_ROLL_BACK:
      g_test_message ("... restored snapshot version \"%s\"", returned_to);
      g_assert_cmpstr (returned_to, ==, to_version->number);

      tests_assert_symlink (self->current_symlink, to_version->directory);
      g_assert_true (g_file_test (self->static_subvolume,
                                  G_FILE_TEST_IS_DIR));
      g_assert_true (g_file_test (self->variable_subvolume,
                                  G_FILE_TEST_IS_DIR));
      g_assert_false (g_file_test (self->rollback_symlink, G_FILE_TEST_EXISTS));
      g_assert_false (g_file_test (self->rollback_static_subvolume,
                                   G_FILE_TEST_EXISTS));
      g_assert_false (g_file_test (self->rollback_variable_subvolume,
                                   G_FILE_TEST_EXISTS));
      break;

    default:
      g_return_if_reached ();
    }

  /* This only exists briefly, and has been cleaned up by the time the
   * method returns. */
  g_assert_false (g_file_test (self->old_current_symlink, G_FILE_TEST_EXISTS));
}

void
tests_app_bundle_uninstall (TestsAppBundle *self,
                            const TestsVersion *version)
{
  g_autoptr (GAsyncResult) result = NULL;
  gboolean ok;
  gboolean status;
  guint error_code;
  GError *error = NULL;

  g_test_message ("Uninstalling \"%s\"...", self->id);

  tests_assert_symlink (self->current_symlink, version->directory);
  g_assert_true (g_file_test (self->static_subvolume, G_FILE_TEST_IS_DIR));
  g_assert_true (g_file_test (self->variable_subvolume, G_FILE_TEST_IS_DIR));

  ribchester_app_store_call_uninstall_app (self->app_store_proxy, self->id,
                                           NULL, tests_store_result_cb,
                                           &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  ok = ribchester_app_store_call_uninstall_app_finish (self->app_store_proxy,
                                                       &status,
                                                       &error_code,
                                                       result,
                                                       &error);
  g_assert_no_error (error);
  g_assert_cmpint (ok, ==, TRUE);
  g_clear_object (&result);

  g_assert_cmpuint (error_code, ==, 0);
  g_assert_cmpint (status, ==, TRUE);
  g_test_message ("... done");

  g_assert_false (g_file_test (self->current_symlink,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->static_subvolume, G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->variable_subvolume, G_FILE_TEST_EXISTS));

  tests_assert_symlink (self->uninstalled_symlink, version->directory);
  g_assert_true (g_file_test (version->storage_path, G_FILE_TEST_IS_DIR));
  g_assert_true (g_file_test (self->uninstalled_static_subvolume,
                              G_FILE_TEST_IS_DIR));
  g_assert_true (g_file_test (self->uninstalled_variable_subvolume,
                              G_FILE_TEST_IS_DIR));
}

void
tests_app_bundle_reinstall (TestsAppBundle *self,
                            const TestsVersion *version)
{
  g_autoptr (GAsyncResult) result = NULL;
  gboolean ok;
  gboolean status;
  guint error_code;
  GError *error = NULL;

  g_test_message ("Reinstalling \"%s\"...", self->id);

  tests_assert_symlink (self->uninstalled_symlink, version->directory);
  g_assert_false (g_file_test (self->current_symlink,
                               G_FILE_TEST_EXISTS));
  g_assert_true (g_file_test (self->uninstalled_static_subvolume,
                              G_FILE_TEST_IS_DIR));
  g_assert_true (g_file_test (self->uninstalled_variable_subvolume,
                              G_FILE_TEST_IS_DIR));
  g_assert_true (g_file_test (version->storage_path, G_FILE_TEST_IS_DIR));

  ribchester_app_store_call_re_install_app (self->app_store_proxy, self->id,
                                            NULL, tests_store_result_cb,
                                            &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  ok = ribchester_app_store_call_re_install_app_finish (self->app_store_proxy,
                                                        &status,
                                                        &error_code,
                                                        result,
                                                        &error);
  g_assert_no_error (error);
  g_assert_cmpint (ok, ==, TRUE);
  g_clear_object (&result);

  g_assert_cmpuint (error_code, ==, 0);
  g_assert_cmpint (status, ==, TRUE);
  g_test_message ("... done");

  tests_assert_symlink (self->current_symlink, version->directory);
  g_assert_true (g_file_test (self->static_subvolume, G_FILE_TEST_IS_DIR));
  g_assert_true (g_file_test (self->variable_subvolume, G_FILE_TEST_IS_DIR));
  g_assert_true (g_file_test (version->storage_path, G_FILE_TEST_IS_DIR));

  g_assert_false (g_file_test (self->uninstalled_symlink,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->uninstalled_static_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->uninstalled_variable_subvolume,
                               G_FILE_TEST_EXISTS));
}

void
tests_app_bundle_remove (TestsAppBundle *self,
                         TestsInstallApi install_api)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GAsyncResult) result = NULL;
  gboolean ok;

  g_assert_true (g_file_test (self->static_subvolume, G_FILE_TEST_IS_DIR) ||
                 g_file_test (self->uninstalled_static_subvolume,
                              G_FILE_TEST_IS_DIR) ||
                 g_file_test (self->rollback_static_subvolume,
                              G_FILE_TEST_IS_DIR) ||
                 g_file_test (self->temporary_subvolume, G_FILE_TEST_IS_DIR));

  g_assert_true (g_file_test (self->variable_subvolume, G_FILE_TEST_IS_DIR) ||
                 g_file_test (self->uninstalled_variable_subvolume,
                              G_FILE_TEST_IS_DIR) ||
                 g_file_test (self->rollback_variable_subvolume,
                              G_FILE_TEST_IS_DIR) ||
                 g_file_test (self->temporary_subvolume, G_FILE_TEST_IS_DIR));

  g_test_message ("Permanently removing \"%s\"...", self->id);

  if (install_api == TESTS_INSTALL_API_BUNDLE_MANAGER1)
    {
      ribchester_bundle_manager1_call_remove_bundle (self->bundle_manager1_proxy,
                                                     self->id, NULL,
                                                     tests_store_result_cb,
                                                     &result);

      while (result == NULL)
        g_main_context_iteration (NULL, TRUE);

      ok = ribchester_bundle_manager1_call_remove_bundle_finish (self->bundle_manager1_proxy,
                                                                 result,
                                                                 &error);
      g_assert_no_error (error);
      g_assert_cmpint (ok, ==, TRUE);
      g_clear_object (&result);
    }
  else
    {
      gboolean status;
      guint error_code;

      g_assert (install_api == TESTS_INSTALL_API_APP_STORE ||
                install_api == TESTS_INSTALL_API_LOW_LEVEL);

      ribchester_app_store_call_remove_app (self->app_store_proxy, self->id,
                                            NULL, tests_store_result_cb,
                                            &result);

      while (result == NULL)
        g_main_context_iteration (NULL, TRUE);

      ok = ribchester_app_store_call_remove_app_finish (self->app_store_proxy,
                                                        &status,
                                                        &error_code,
                                                        result,
                                                        &error);
      g_assert_no_error (error);
      g_assert_cmpint (ok, ==, TRUE);
      g_clear_object (&result);

      g_assert_cmpuint (error_code, ==, 0);
      g_assert_cmpint (status, ==, TRUE);
    }

  g_test_message ("... done");

  g_assert_false (g_file_test (self->static_subvolume, G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->uninstalled_static_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->rollback_static_subvolume,
                               G_FILE_TEST_EXISTS));

  g_assert_false (g_file_test (self->variable_subvolume, G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->uninstalled_variable_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->rollback_variable_subvolume,
                               G_FILE_TEST_EXISTS));

  /* No need to test for individual symlinks: if the storage path doesn't
   * exist, then neither do its contents */
  g_assert_false (g_file_test (self->storage_path, G_FILE_TEST_EXISTS));
}

gboolean
tests_app_bundle_try_remove (TestsAppBundle *self,
                             TestsInstallApi api,
                             GError **error)
{
  g_autoptr (GAsyncResult) result = NULL;

  if (api == TESTS_INSTALL_API_APP_STORE)
    {
      ribchester_app_store_call_remove_app (self->app_store_proxy, self->id,
                                            NULL, tests_store_result_cb,
                                            &result);

      while (result == NULL)
        g_main_context_iteration (NULL, TRUE);

      return ribchester_app_store_call_remove_app_finish (self->app_store_proxy,
                                                          NULL, NULL,
                                                          result, error);
    }
  else
    {
      ribchester_bundle_manager1_call_remove_bundle (self->bundle_manager1_proxy,
                                                     self->id, NULL,
                                                     tests_store_result_cb,
                                                     &result);

      while (result == NULL)
        g_main_context_iteration (NULL, TRUE);

      return ribchester_bundle_manager1_call_remove_bundle_finish (self->bundle_manager1_proxy,
                                                                   result,
                                                                   error);
    }
}

void
tests_app_bundle_delete_rollback_snapshot (TestsAppBundle *self,
                                           const TestsVersion *version)
{
  g_autoptr (GAsyncResult) result = NULL;
  gboolean ok;
  GError *error = NULL;

  g_test_message ("Removing ability to roll back \"%s\" to \"%s\"...",
                  self->id, version->directory);
  tests_assert_symlink (self->rollback_symlink, version->directory);

  ribchester_app_store_call_delete_rollback_snapshot (self->app_store_proxy,
                                                      self->id, NULL,
                                                      tests_store_result_cb,
                                                      &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  ok = ribchester_app_store_call_delete_rollback_snapshot_finish (self->app_store_proxy,
                                                                  result,
                                                                  &error);
  g_assert_no_error (error);
  g_assert_cmpint (ok, ==, TRUE);
  g_clear_object (&result);
  g_test_message ("... done");

  g_assert_false (g_file_test (self->rollback_symlink,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->rollback_static_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (self->rollback_variable_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (version->storage_path, G_FILE_TEST_EXISTS));
}
