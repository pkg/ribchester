/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* Tests for the test infrastructure. */

#include "tests/common.h"

#include <errno.h>

#include <gio/gio.h>
#include <glib/gstdio.h>

static void
test_assert_syscall_0 (void)
{
  GStatBuf stat_buf;

  if (g_test_subprocess ())
    {
      /* Assumed to fail with EEXIST */
      tests_assert_syscall_0 (g_mkdir ("/", 0755));
      return;
    }

  /* Assumed to succeed */
  tests_assert_syscall_0 (g_stat ("/", &stat_buf));

  /* Check the failing case by re-running this method in a subprocess */
  g_test_trap_subprocess (NULL, 0, 0);
  g_test_trap_assert_failed ();
  g_test_trap_assert_stderr (tests_printf ("*%s*", g_strerror (EEXIST)));
}

/*
 * Test that tests_is_mount_point() returns the right result.
 */
static void
test_is_mount_point (void)
{
  /* Should be a mount point */
  g_assert_true (tests_is_mount_point ("/proc"));

  /* Assumed to exist but not be a mount point */
  g_assert_false (tests_is_mount_point ("/etc/init.d"));
  g_assert_false (tests_is_mount_point ("/proc/1/fd"));

  /* Assumed not to exist */
  g_assert_false (tests_is_mount_point ("/we assume this does not exist"));
}

/*
 * Test that tests_printf() returns the right result (and if this test
 * is run under valgrind, also that it doesn't leak).
 */
static void
test_printf (void)
{
  g_assert_cmpstr (tests_printf ("%s %d worlds", "hello", 42), ==,
                   "hello 42 worlds");
}

/*
 * Test that tests_store_result_cb() works as intended.
 */
static void
test_store_result (void)
{
  g_autoptr (GSubprocess) subprocess = NULL;
  g_autoptr (GAsyncResult) result = NULL;
  GError *error = NULL;

  /* This is just a random example of something you can do asynchronously */
  subprocess = g_subprocess_new (G_SUBPROCESS_FLAGS_NONE, &error,
                                 "/bin/true", NULL);
  g_assert_no_error (error);
  g_assert (G_IS_SUBPROCESS (subprocess));

  g_subprocess_communicate_async (subprocess, NULL, NULL,
                                  tests_store_result_cb, &result);

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  g_subprocess_communicate_finish (subprocess, result, NULL, NULL, &error);
  g_assert_no_error (error);
}

int
main (int argc,
    char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/assert-syscall-0", test_assert_syscall_0);
  g_test_add_func ("/is-mount-point", test_is_mount_point);
  g_test_add_func ("/printf", test_printf);
  g_test_add_func ("/store-result", test_store_result);

  return g_test_run ();
}
