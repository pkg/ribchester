/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <errno.h>
#include <pwd.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <canterbury/canterbury.h>
#include <glib.h>
#include <glib/gstdio.h>

#include <ribchester/ribchester.h>
#include "tests/common.h"

typedef struct
{
  TestsAppBundle bundle;
  RibchesterAppStore *app_store_proxy;
  RibchesterBundleManager1 *bundle_manager1_proxy;
  guint ordinary_uid;
  guint ordinary_gid;
  guint unrelated_uid;
  guint unrelated_gid;
} Fixture;

/* None of these tests need more than one version at the moment */
static const gchar * const version_numbers[] = { "0" };

/*
 * Common setup for all tests.
 */
static void
setup (Fixture *f,
       gconstpointer unused G_GNUC_UNUSED)
{
  g_autoptr (GError) error = NULL;

  if (!tests_require_ordinary_uid (&f->ordinary_uid, &f->ordinary_gid,
                                   &f->unrelated_uid, &f->unrelated_gid))
    return;

  if (!tests_require_ribchester (NULL, &f->app_store_proxy,
                                 &f->bundle_manager1_proxy))
    return;

  if (!tests_app_bundle_set_up (&f->bundle, f->app_store_proxy,
                                f->bundle_manager1_proxy, f->ordinary_uid,
                                version_numbers,
                                G_N_ELEMENTS (version_numbers)))
    return;
}

/*
 * Helper: drop privileges. This is a GSpawnChildSetupFunc.
 */
static void
fixture_drop_privileges (gpointer user_data)
{
  Fixture *f = user_data;

  if (getuid () == 0)
    {
      g_assert (f->ordinary_uid >= 1000);
      g_assert (f->ordinary_uid <= 1999);
      g_assert (f->ordinary_gid >= 1000);
      g_assert (f->ordinary_gid <= 1999);

      if (setresgid (f->ordinary_gid, f->ordinary_gid, f->ordinary_gid) != 0)
        g_error ("setresgid: %s", g_strerror (errno));

      if (setresuid (f->ordinary_uid, f->ordinary_uid, f->ordinary_uid) != 0)
        g_error ("setresuid: %s", g_strerror (errno));
    }
}

/*
 * Helper: call PrepareAppBundle in an unprivileged subprocess, assert success.
 */
static const gchar *
fixture_prepare_app_bundle (Fixture *f)
{
  const gchar *argv[] = { "<p-a-b-helper>", "<bundle ID>", "0", NULL };
  gchar *child_stdout = NULL;
  gchar *child_stderr = NULL;
  GError *error = NULL;
  gboolean ok;
  gint exit_status;

  /* Do the actual PrepareAppBundle call as an ordinary user, if we
   * are currently root. */
  argv[0] = g_test_get_filename (G_TEST_BUILT, "prepare-app-bundle-helper",
      NULL);
  argv[1] = f->bundle.id;
  ok = g_spawn_sync (NULL, (gchar **) argv, NULL,
                     G_SPAWN_DEFAULT, fixture_drop_privileges, f,
                     &child_stdout, &child_stderr, &exit_status, &error);
  g_assert_no_error (error);
  g_assert_cmpint (ok, ==, TRUE);

  g_test_message ("%s", child_stdout);

  g_assert_cmpstr (child_stderr, ==, "");
  g_free (child_stderr);
  ok = g_spawn_check_exit_status (exit_status, &error);
  g_assert_no_error (error);
  g_assert_cmpint (ok, ==, TRUE);

  g_assert_cmpstr (child_stdout, !=, "");
  g_assert_cmpint (child_stdout[strlen (child_stdout) - 1], ==, '\n');
  child_stdout[strlen (child_stdout) - 1] = '\0';
  g_test_queue_free (child_stdout);
  return child_stdout;
}

typedef enum
{
  FAIL_BAD_BUNDLE_ID = (1 << 0),
  FAIL_BAD_FLAGS = (1 << 1),
  FAIL_STILL_ROOT = (1 << 2)
} FailFlags;

/*
 * @flags: ways in which we should get PrepareAppBundle wrong
 *
 * Helper: call PrepareAppBundle in an unprivileged subprocess, assert
 * failure with exit code 1. Queue stdout to be freed after the test.
 *
 * Returns: child stdout
 */
static const gchar *
fixture_prepare_app_bundle_fails (Fixture *f,
    FailFlags flags)
{
  const gchar *argv[] = { "<p-a-b-helper>", "<bundle ID>", "0", NULL };
  gchar *child_stdout = NULL;
  gchar *child_stderr = NULL;
  GError *error = NULL;
  gboolean ok;
  gint exit_status;

  argv[0] = g_test_get_filename (G_TEST_BUILT, "prepare-app-bundle-helper",
      NULL);
  argv[1] = f->bundle.id;

  if (flags & FAIL_BAD_BUNDLE_ID)
    argv[1] = "ThisIsntABundle";

  if (flags & FAIL_BAD_FLAGS)
    argv[2] = "42";

  ok = g_spawn_sync (NULL, (gchar **) argv, NULL,
        G_SPAWN_DEFAULT,
        ((flags & FAIL_STILL_ROOT) ? NULL : fixture_drop_privileges),
        ((flags & FAIL_STILL_ROOT) ? NULL : f),
        &child_stdout, &child_stderr, &exit_status, &error);
  g_assert_no_error (error);
  g_assert_cmpint (ok, ==, TRUE);

  g_test_message ("%s", child_stdout);

  g_assert_cmpstr (child_stderr, ==, "");
  g_free (child_stderr);
  ok = g_spawn_check_exit_status (exit_status, &error);
  g_assert_error (error, G_SPAWN_EXIT_ERROR, 1);
  g_assert_cmpint (ok, ==, FALSE);
  g_clear_error (&error);

  g_assert_cmpstr (child_stdout, !=, "");
  g_assert_cmpint (child_stdout[strlen (child_stdout) - 1], ==, '\n');
  child_stdout[strlen (child_stdout) - 1] = '\0';
  g_test_queue_free (child_stdout);
  return child_stdout;
}

/*
 * Test-case: successfully prepare a bundle, in the simple successful case.
 */
static void
test_prep_works (Fixture *f,
    gconstpointer unused G_GNUC_UNUSED)
{
  const gchar *prepared = NULL;
  GStatBuf stat_buf;
  const TestsVersion *version = &f->bundle.versions[0];

  if (g_test_failed ())
    return;

  g_assert_false (g_file_test (f->bundle.static_subvolume,
                               G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (f->bundle.variable_subvolume,
                               G_FILE_TEST_EXISTS));

  tests_app_bundle_install_simple (&f->bundle, TESTS_INSTALL_API_ANY,
                                   NULL, version);

  g_assert_cmpint (g_stat (f->bundle.static_subvolume, &stat_buf), ==, 0);
  g_assert_cmpuint (stat_buf.st_uid, ==, 0);
  g_assert_cmpuint (stat_buf.st_gid, ==, 0);
  g_assert_cmpuint (stat_buf.st_mode & S_IFMT, ==, S_IFDIR);
  g_assert_cmpuint (stat_buf.st_mode & ~S_IFMT, ==, 0755);

  g_assert_cmpint (g_stat (f->bundle.variable_subvolume, &stat_buf), ==, 0);
  g_assert_cmpuint (stat_buf.st_uid, ==, 0);
  g_assert_cmpuint (stat_buf.st_gid, ==, 0);
  g_assert_cmpuint (stat_buf.st_mode & S_IFMT, ==, S_IFDIR);
  g_assert_cmpuint (stat_buf.st_mode & ~S_IFMT, ==, 0755);

  prepared = fixture_prepare_app_bundle (f);
  g_assert_cmpstr (prepared, ==, f->bundle.persistence_path);

  /* .../users is drwx--x--x root:root */
  g_assert_cmpint (g_stat (f->bundle.users_path, &stat_buf), ==, 0);
  g_assert_cmpuint (stat_buf.st_uid, ==, 0);
  g_assert_cmpuint (stat_buf.st_gid, ==, 0);
  g_assert_cmpuint (stat_buf.st_mode & S_IFMT, ==, S_IFDIR);
  g_assert_cmpuint (stat_buf.st_mode & ~S_IFMT, ==, 0711);

  /* .../users/1000 is drwx------ 1000:root */
  g_assert_cmpint (g_stat (f->bundle.persistence_path, &stat_buf), ==, 0);
  g_assert_cmpuint (stat_buf.st_uid, ==, f->ordinary_uid);
  g_assert_cmpuint (stat_buf.st_gid, ==, 0);
  g_assert_cmpuint (stat_buf.st_mode & S_IFMT, ==, S_IFDIR);
  g_assert_cmpuint (stat_buf.st_mode & ~S_IFMT, ==, 0700);

  g_assert_true (g_file_test (f->bundle.static_subvolume, G_FILE_TEST_IS_DIR));
  g_assert_true (g_file_test (f->bundle.variable_subvolume,
                              G_FILE_TEST_IS_DIR));
  tests_app_bundle_remove (&f->bundle, TESTS_INSTALL_API_ANY);
  g_assert_false (g_file_test (f->bundle.static_subvolume, G_FILE_TEST_EXISTS));
  g_assert_false (g_file_test (f->bundle.variable_subvolume,
                               G_FILE_TEST_EXISTS));
}

/*
 * Test-case: try various ways we can fail to prepare a bundle due to
 * invalid arguments.
 */
static void
test_prep_invalid_arguments (Fixture *f,
    gconstpointer unused G_GNUC_UNUSED)
{
  GStatBuf stat_buf;
  const TestsVersion *version = &f->bundle.versions[0];

  if (g_test_failed ())
    return;

  tests_app_bundle_install_simple (&f->bundle, TESTS_INSTALL_API_ANY,
                                   NULL, version);

  g_test_expect_message (G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE,
                         "org.apertis.Ribchester.Error.*:*");
  g_message ("%s", fixture_prepare_app_bundle_fails (f, FAIL_BAD_FLAGS));
  g_test_assert_expected_messages ();

  g_test_expect_message (G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE,
                         "org.apertis.Ribchester.Error.*:*");
  g_message ("%s", fixture_prepare_app_bundle_fails (f, FAIL_BAD_BUNDLE_ID));
  g_test_assert_expected_messages ();

  if (getuid () == 0)
    {
      g_test_expect_message (G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE,
                             "org.apertis.Ribchester.Error.*:*");
      g_message ("%s", fixture_prepare_app_bundle_fails (f, FAIL_STILL_ROOT));
      g_test_assert_expected_messages ();
    }
  else
    {
      g_test_skip ("not running as root");
    }

  /* None of those actually created it. */
  if (g_stat (f->bundle.users_path, &stat_buf) == 0)
    {
      g_test_message ("users path should not have been created");
      g_test_fail ();
    }
  else
    {
      int saved_errno = errno;

      g_assert_cmpint (saved_errno, ==, ENOENT);
    }

  tests_app_bundle_remove (&f->bundle, TESTS_INSTALL_API_ANY);
}

static void
test_prep_permissions (Fixture *f,
    gconstpointer unused G_GNUC_UNUSED)
{
  const gchar *prepared = NULL;
  GStatBuf stat_buf;
  const TestsVersion *version = &f->bundle.versions[0];

  if (g_test_failed ())
    return;

  /* We need to be able to use chown() for this test */
  if (!tests_require_running_as_root ())
    return;

  tests_app_bundle_install_simple (&f->bundle, TESTS_INSTALL_API_ANY,
                                   NULL, version);

  /* too restrictive */
  tests_assert_syscall_0 (chmod (f->bundle.variable_path, 0750));
  g_test_expect_message (G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE,
      "org.apertis.Ribchester.Error.*:*");
  g_message ("%s", fixture_prepare_app_bundle_fails (f, 0));
  g_test_assert_expected_messages ();

  /* too permissive */
  tests_assert_syscall_0 (chmod (f->bundle.variable_path, 0775));
  g_test_expect_message (G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE,
      "org.apertis.Ribchester.Error.*:*");
  g_message ("%s", fixture_prepare_app_bundle_fails (f, 0));
  g_test_assert_expected_messages ();

  /* correct permissions but incorrect owner */
  tests_assert_syscall_0 (chmod (f->bundle.variable_path, 0711));
  tests_assert_syscall_0 (chown (f->bundle.variable_path,
                                 f->unrelated_uid, f->unrelated_gid));
  g_test_expect_message (G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE,
                         "org.apertis.Ribchester.Error.*:*");
  g_message ("%s", fixture_prepare_app_bundle_fails (f, 0));
  g_test_assert_expected_messages ();

  /* back to the correct uid so we can test subdirectory permissions */
  tests_assert_syscall_0 (chown (f->bundle.variable_path, 0, 0));
  /* pre-create users directory */
  tests_assert_syscall_0 (mkdir (f->bundle.users_path, 0755));

  /* users directory: too restrictive */
  tests_assert_syscall_0 (chmod (f->bundle.users_path, 0710));
  g_test_expect_message (G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE,
                         "org.apertis.Ribchester.Error.*:*");
  g_message ("%s", fixture_prepare_app_bundle_fails (f, 0));
  g_test_assert_expected_messages ();

  /* too permissive */
  tests_assert_syscall_0 (chmod (f->bundle.users_path, 0771));
  g_test_expect_message (G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE,
                         "org.apertis.Ribchester.Error.*:*");
  g_message ("%s", fixture_prepare_app_bundle_fails (f, 0));
  g_test_assert_expected_messages ();

  /* correct permissions but incorrect owner */
  tests_assert_syscall_0 (chmod (f->bundle.users_path, 0711));
  tests_assert_syscall_0 (chown (f->bundle.users_path,
                                 f->unrelated_uid, f->unrelated_gid));
  g_test_expect_message (G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE,
      "org.apertis.Ribchester.Error.*:*");
  g_message ("%s", fixture_prepare_app_bundle_fails (f, 0));
  g_test_assert_expected_messages ();

  /* back to the correct uid so we can test subdirectory permissions */
  tests_assert_syscall_0 (chown (f->bundle.users_path, 0, 0));

  /* per-user directory: wrong owner */
  tests_assert_syscall_0 (mkdir (f->bundle.persistence_path, 0700));
  tests_assert_syscall_0 (chown (f->bundle.persistence_path,
                                 f->unrelated_uid, f->unrelated_gid));
  g_test_expect_message (G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE,
      "org.apertis.Ribchester.Error.*:*");
  g_message ("%s", fixture_prepare_app_bundle_fails (f, 0));
  g_test_assert_expected_messages ();

  /* back to a valid uid */
  tests_assert_syscall_0 (chown (f->bundle.persistence_path, 0, 0));

  /* now it works */
  prepared = fixture_prepare_app_bundle (f);
  g_assert_cmpstr (prepared, ==, f->bundle.persistence_path);

  /* .../users is drwx--x--x root:root */
  g_assert_cmpint (g_stat (f->bundle.users_path, &stat_buf), ==, 0);
  g_assert_cmpuint (stat_buf.st_uid, ==, 0);
  g_assert_cmpuint (stat_buf.st_gid, ==, 0);
  g_assert_cmpuint (stat_buf.st_mode & S_IFMT, ==, S_IFDIR);
  g_assert_cmpuint (stat_buf.st_mode & ~S_IFMT, ==, 0711);

  /* .../users/1000 is drwx------ 1000:root */
  g_assert_cmpint (g_stat (f->bundle.persistence_path, &stat_buf), ==, 0);
  g_assert_cmpuint (stat_buf.st_uid, ==, f->ordinary_uid);
  g_assert_cmpuint (stat_buf.st_gid, ==, 0);
  g_assert_cmpuint (stat_buf.st_mode & S_IFMT, ==, S_IFDIR);
  g_assert_cmpuint (stat_buf.st_mode & ~S_IFMT, ==, 0700);

  /* it's idempotent (works when called repeatedly)  */
  prepared = fixture_prepare_app_bundle (f);
  g_assert_cmpstr (prepared, ==, f->bundle.persistence_path);

  /* still the same permissions */
  g_assert_cmpint (g_stat (f->bundle.users_path, &stat_buf), ==, 0);
  g_assert_cmpuint (stat_buf.st_uid, ==, 0);
  g_assert_cmpuint (stat_buf.st_gid, ==, 0);
  g_assert_cmpuint (stat_buf.st_mode & S_IFMT, ==, S_IFDIR);
  g_assert_cmpuint (stat_buf.st_mode & ~S_IFMT, ==, 0711);

  /* still the same permissions */
  g_assert_cmpint (g_stat (f->bundle.persistence_path, &stat_buf), ==, 0);
  g_assert_cmpuint (stat_buf.st_uid, ==, f->ordinary_uid);
  g_assert_cmpuint (stat_buf.st_gid, ==, 0);
  g_assert_cmpuint (stat_buf.st_mode & S_IFMT, ==, S_IFDIR);
  g_assert_cmpuint (stat_buf.st_mode & ~S_IFMT, ==, 0700);

  tests_app_bundle_remove (&f->bundle, TESTS_INSTALL_API_ANY);
}

static void
teardown (Fixture *f,
          gconstpointer unused G_GNUC_UNUSED)
{
  tests_app_bundle_tear_down (&f->bundle);
  g_clear_object (&f->app_store_proxy);
  g_clear_object (&f->bundle_manager1_proxy);
}

int
main (int argc,
    char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/prep/works", Fixture, NULL, setup, test_prep_works, teardown);
  g_test_add ("/prep/invalid-arguments", Fixture, NULL, setup,
              test_prep_invalid_arguments, teardown);
  g_test_add ("/prep/permissions", Fixture, NULL, setup, test_prep_permissions,
              teardown);

  return g_test_run ();
}
