/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <stdio.h>
#include <stdlib.h>

#include <gio/gio.h>
#include <glib.h>

#include <ribchester/ribchester.h>

static RibchesterAppStore *mntAppStrProxy;
static void name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data);
static void name_appeared (GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data);
static void ribchester_proxy_clb( GObject *source_object, GAsyncResult *res, gpointer user_data);
static gboolean b_ribchester_test_fi(gpointer pUserData);

static void name_vanished(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	g_print("\n Name Vanished");
	if(NULL != mntAppStrProxy)
		g_object_unref(mntAppStrProxy);
}

static void name_appeared (GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data)
{
	/* Asynchronously creates a proxy for the D-Bus interface */
	ribchester_app_store_proxy_new (connection,
			G_DBUS_PROXY_FLAGS_NONE,
			"org.apertis.Ribchester",
			"/org/apertis/Ribchester/AppStore",
			NULL,
			ribchester_proxy_clb,
			NULL);
}


static void check_mount_status (RibchesterAppStore *proxy, gchar *subvolname, gboolean mountStatus, gchar *mountPath)
{
	g_print ("CALLBACK :Mount status for %s is %d. Mount path = %s \n", subvolname, mountStatus, mountPath);
}



static void data_sync_status(RibchesterAppStore *proxy, gchar *subvolname ,
		guint arg_SyncErrInfo , gpointer pUserData)
{
	g_print ("CALLBACK :Data Sync status for %s is %d.   \n", subvolname, arg_SyncErrInfo);
	g_print("Sync Status:  %s  \n",		arg_SyncErrInfo == 0 ? "SUCCESS":"FAILED" );
	g_timeout_add(10 , b_ribchester_test_fi, NULL);
}


static void ribchester_proxy_clb( GObject *source_object, GAsyncResult *res, gpointer user_data)
{
	GError *error;

	/* finishes the proxy creation and gets the proxy ptr */
	mntAppStrProxy = ribchester_app_store_proxy_new_finish(res , &error);

	if(mntAppStrProxy != NULL)
	{
		g_signal_connect(mntAppStrProxy, "mount-status", (GCallback) check_mount_status, NULL);
		g_signal_connect(mntAppStrProxy, "data-sync-status", (GCallback) data_sync_status, NULL);
		g_timeout_add(10 , b_ribchester_test_fi, NULL);
	}
}

#if 0
static void rename_subvol_done (GObject *source_object, GAsyncResult *res, gpointer user_data)
{
	gchar *name;
	gboolean status;
	ribchester_app_store_call_rename_subvolume_finish (mntAppStrProxy, &name, &status, res, NULL);
	g_print("Name after rename = %s \n", name);
	g_signal_connect(mntAppStrProxy, "mount-status", (GCallback) check_mount_status, NULL);  
	ribchester_app_store_call_mount_subvolume (mntAppStrProxy, name, "/Applications/News", NULL, NULL, NULL);
}
#endif


//static void create_and_mount_app (void)
//{
//	ribchester_app_store_call_create_subvolume (mntAppStrProxy, "News", NULL, create_subvol_done, NULL);
//}

gint main(gint argc, gchar *argv[])
{

	GMainLoop *mainloop = NULL;

	gint watch;

	/* Starts watching name on the bus specified by bus_type */
	/* calls name_appeared_handler and name_vanished_handler when the name is known to have a owner or known to lose its owner */
	watch = g_bus_watch_name (G_BUS_TYPE_SYSTEM,
			"org.apertis.Ribchester",
			G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
			name_appeared,
			name_vanished,
			NULL,
			NULL);
	g_print(" Watch Id = %d\n" ,watch );
	mainloop = g_main_loop_new (NULL, FALSE);
	g_main_loop_run (mainloop);

	exit (0);
}

static void create_subvol_done (GObject *source_object, GAsyncResult *res,
		gpointer user_data)
{
	gchar *name = NULL;
	gboolean status;
	guint inErrInfo;
	GError *error = NULL;

	if (!ribchester_app_store_call_create_subvolume_finish (mntAppStrProxy,
		&name, &status, &inErrInfo, res, &error))
	{
		g_print ("CreateSubvolume reports D-Bus error: %s #%d: %s\n",
			g_quark_to_string (error->domain),
			error->code, error->message);
	}
	else
	{
		g_print ("CreateSubvolume reply received:\n");
		g_print ("\tname: \"%s\"\n", name);
		g_print ("\tstatus: %s\n", status ? "true" : "false");
		g_print ("\terror code: #%u\n", inErrInfo);
	}

	g_free (name);
	//ribchester_app_store_call_rename_subvolume (mntAppStrProxy, name, "News-10", NULL, rename_subvol_done, NULL);
	g_timeout_add(10 , b_ribchester_test_fi, NULL);
	//	ribchester_app_store_call_mount_subvolume (mntAppStrProxy, name, "/Applications/News", NULL, NULL, NULL);
	g_clear_error (&error);
}

static void delete_subvol_done (GObject *source_object, GAsyncResult *res,
		gpointer user_data)
{
	gboolean status;
	guint inErrInfo;
	GError *error = NULL;

	if (!ribchester_app_store_call_delete_subvolume_finish (mntAppStrProxy,
		&status, &inErrInfo, res, &error))
	{
		g_print ("DeleteSubvolume reports D-Bus error: %s #%d: %s\n",
			g_quark_to_string (error->domain),
			error->code, error->message);
	}
	else
	{
		g_print ("DeleteSubvolume reply received:\n");
		g_print ("\tstatus: %s\n", status ? "true" : "false");
		g_print ("\terror code: #%u\n", inErrInfo);
	}

	g_timeout_add(10 , b_ribchester_test_fi, NULL);
	g_clear_error (&error);
}



static void rename_subvol_done (GObject *source_object, GAsyncResult *res,
		gpointer user_data)
{
	gchar *name = NULL;
	gboolean status;
	guint inErrInfo;
	GError *error = NULL;

	if (!ribchester_app_store_call_rename_subvolume_finish (mntAppStrProxy,
		&name, &status, &inErrInfo, res, &error))
	{
		g_print ("RenameSubvolume reports D-Bus error: %s #%d: %s\n",
			g_quark_to_string (error->domain),
			error->code, error->message);
	}
	else
	{
		g_print ("RenameSubvolume reply received:\n");
		g_print ("\tname after rename: %s\n", name);
		g_print ("\tstatus: %s\n", status ? "true" : "false");
		g_print ("\terror code: #%u\n", inErrInfo);
	}

	g_free (name);
	g_timeout_add(10 , b_ribchester_test_fi, NULL);
	g_clear_error (&error);
}



static void mount_subvol_done (GObject *source_object, GAsyncResult *res,
		gpointer user_data)
{
	guint inErrInfo;
	GError *error = NULL;

	if (!ribchester_app_store_call_mount_subvolume_finish (mntAppStrProxy,
		&inErrInfo, res, &error))
	{
		g_print ("MountSubvolume reports D-Bus error: %s #%d: %s\n",
			g_quark_to_string (error->domain),
			error->code, error->message);
	}
	else if (inErrInfo != 0)
	{
		g_print ("MountSubvolume reports error: code #%u\n", inErrInfo);
	}
	else
	{
		g_print ("MountSubvolume reports success\n");
	}

	g_timeout_add(10 , b_ribchester_test_fi, NULL);
	g_clear_error (&error);
}

static void unmount_subvol_done (GObject *source_object, GAsyncResult *res,
		gpointer user_data)
{
	gboolean status;
	guint inErrInfo;
	GError *error = NULL;

	if (!ribchester_app_store_call_unmount_subvolume_finish (mntAppStrProxy,
		&status, &inErrInfo, res, &error))
	{
		g_print ("UnmountSubvolume reports D-Bus error: %s #%d: %s\n",
			g_quark_to_string (error->domain),
			error->code, error->message);
	}
	else
	{
		g_print ("UnmountSubvolume reply received:\n");
		g_print ("\tstatus: %s\n", status ? "true" : "false");
		g_print ("\terror code: #%u\n", inErrInfo);
	}

	g_timeout_add(10 , b_ribchester_test_fi, NULL);
	g_clear_error (&error);
}

static void rollback_app_done(GObject *source_object, GAsyncResult *res,
		gpointer user_data)
{
	g_autofree gchar *out_version = NULL;
	GError *error = NULL;

	if (!ribchester_app_store_call_roll_back_finish (mntAppStrProxy,
		&out_version, res, &error))
	{
		g_print ("RollBackApp reports D-Bus error: %s #%d: %s\n",
			g_quark_to_string (error->domain),
			error->code, error->message);
	}
	else
	{
		g_print ("RollBackApp reply received:\n");
		g_print ("\tsnapshot version: \"%s\"\n", out_version);
	}

	g_timeout_add(10 , b_ribchester_test_fi, NULL);
	g_clear_error (&error);
}

static void
v_ribchester_create_subvolume (void)
{
	gchar pAppName[100];

	g_print ("Enter app name (e.g. com.example.App):\n");
	scanf ("%99s" ,pAppName );
	ribchester_app_store_call_create_subvolume(mntAppStrProxy,pAppName, NULL, create_subvol_done, pAppName);
}

static void
v_ribchester_delete_subvolume (void)
{
	gchar pAppName[100];

	g_print ("Enter subvolume name (e.g. com.example.App):\n");
	scanf ("%99s", pAppName);
	ribchester_app_store_call_delete_subvolume(mntAppStrProxy,pAppName, NULL, delete_subvol_done, NULL);
}

static void
v_ribchester_rename_subvolume (void)
{
	gchar pAppName[100];
	gchar pAppName2[100];

	g_print ("Enter old subvolume name (e.g. com.example.App):\n");
	scanf ("%99s", pAppName);
	g_print ("Enter new subvolume name (e.g. com.example.App2):\n");
	scanf ("%99s", pAppName2);

	ribchester_app_store_call_rename_subvolume(mntAppStrProxy,pAppName,pAppName2, NULL, rename_subvol_done, NULL);
}

static void
v_ribchester_mount_subvolume (void)
{
	gchar pAppName[100];
	gchar pAppName2[100];

	g_print ("Enter name of subvolume to mount (e.g. com.example.App):\n");
	scanf ("%99s", pAppName);
	g_print ("Enter path where the subvolume should be mounted (e.g. /Applications/com.example.App):\n");
	scanf ("%99s", pAppName2);

	ribchester_app_store_call_mount_subvolume(mntAppStrProxy, pAppName, pAppName2, NULL, mount_subvol_done, pAppName2);
}

static void
v_ribchester_unmount_subvolume (void)
{
	gchar pAppName[100];

	g_print ("Enter name of the subvolume to unmount (e.g. com.example.App):\n");
	scanf ("%99s", pAppName);
	ribchester_app_store_call_unmount_subvolume(mntAppStrProxy, pAppName,   NULL, unmount_subvol_done, NULL);
}

static void
v_ribchester_rollback_app (void)
{

	gchar pAppName[100];

	g_print ("Enter Name of the app subvolume to be rolled back (e.g. com.example.App):\n");
	scanf ("%99s", pAppName);
	ribchester_app_store_call_roll_back (mntAppStrProxy, pAppName, NULL, rollback_app_done, NULL);
}


static void
v_ribchester_set_boot_flags (void)
{
	gint inBootFlag;
	printf(" Enter \n0 for normal boot \n1 for boot on system update \n");
	scanf("%d" ,&inBootFlag);
	if(0 == inBootFlag || 1 == inBootFlag)
	{
	GError *pError = NULL ;
	ribchester_app_store_call_set_update_status_sync(mntAppStrProxy ,
			inBootFlag , NULL , &pError);
	if(NULL != pError)
	{
		g_warning("Error Seting boot flags %s \n" ,pError->message);
		g_error_free(pError);
		pError = NULL ;
	}
	}
	else
	{
		g_print("Invalid parameter entered \n");
	}
	g_timeout_add(10 , b_ribchester_test_fi, NULL);
}

static gboolean b_ribchester_test_fi(gpointer pUserData)
{
	int inOption ;
	g_print("%s %d \n", __FUNCTION__ , __LINE__);
	printf(" \n\n\n\n Press 1 to test create sub-volume\n");
	printf(" Press 2 to test delete subvoulume \n");
	printf(" Press 3 to test rename subvolume \n");
	printf(" Press 4 to test mount subvolume \n");
	printf(" Press 5 to test unmount subvolume\n");
	printf(" Press 7 to test rollback app\n");
	printf(" Press 8 to test set boot flags\n");
	printf(" Press 0 to quit \n\n\n\n\n");
	scanf("%d", &inOption) ;
	switch (inOption)
	{
	case 1:
		v_ribchester_create_subvolume();
		break;
	case 2:
		v_ribchester_delete_subvolume();
		break;
	case 3:
		v_ribchester_rename_subvolume();
		break;
	case 4:
		v_ribchester_mount_subvolume();
		break;
	case 5:
		v_ribchester_unmount_subvolume();
		break;
	case 7:
		v_ribchester_rollback_app();
		break;
	case 8:
		v_ribchester_set_boot_flags();
		break;
	case 0:
		exit(0);
		break;
	default:
		g_print(" Invalid Option \n");
		break;
	}

	return FALSE ;
}
