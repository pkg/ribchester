m4_define([ribchester_major], [0])
m4_define([ribchester_minor], [2020])
m4_define([ribchester_micro], [2])

m4_define([ribchester_version], [ribchester_major.ribchester_minor.ribchester_micro])


# Before making a release, the RIBCHESTER_LT_VERSION string should be modified. The
# string is of the form c:r:a. Follow these instructions sequentially:
# 1. If the library source code has changed at all since the last update, then
# increment revision (‘c:r:a’ becomes ‘c:r+1:a’).
# 2. If any interfaces have been added, removed, or changed since the last
# update, increment current, and set revision to 0.
# 3. If any interfaces have been added since the last public release, then
# increment age.
# 4. If any interfaces have been removed or changed since the last public
# release, then set age to 0.

m4_define([ribchester_current], [6])
m4_define([ribchester_revision], [1])
m4_define([ribchester_age], [1])

m4_define([ribchester_lt_version],[ribchester_current:ribchester_revision:ribchester_age])

m4_define([ribchester_api_version], [0])

AC_INIT([ribchester], [ribchester_version])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_HEADERS([config.h])
AM_MAINTAINER_MODE([enable])
AX_IS_RELEASE([git-directory])

AC_SUBST([RIBCHESTER_LT_CURRENT], [ribchester_current])
AC_SUBST([RIBCHESTER_LT_REVISION], [ribchester_revision])
AC_SUBST([RIBCHESTER_LT_AGE], [ribchester_age])
AC_SUBST(RIBCHESTER_VERSION,ribchester_version)
AC_SUBST([RIBCHESTER_API_VERSION], [ribchester_api_version])

AM_INIT_AUTOMAKE([subdir-objects -Wno-portability tar-ustar])
AM_SILENT_RULES([yes])

AC_USE_SYSTEM_EXTENSIONS
AC_PROG_CC
AC_PROG_CXX
AC_SYS_LARGEFILE

AC_DISABLE_STATIC
AC_PROG_LIBTOOL

LT_INIT

AC_ARG_VAR([GLIB_MKENUMS], [the glib-mkenums tool])
AC_PATH_PROG([GLIB_MKENUMS], [glib-mkenums])
AS_IF([test -z "$GLIB_MKENUMS"],
  [AC_MSG_ERROR([glib-mkenums not found])])

GOBJECT_INTROSPECTION_CHECK([1.31.1])

dnl check for hotdoc
HOTDOC_CHECK([0.8], [c, dbus])

AC_ARG_VAR(GDBUS_CODEGEN, [the gdbus-codegen programme ])
AC_PATH_PROG(GDBUS_CODEGEN, gdbus-codegen)
AS_IF([test -z "$GDBUS_CODEGEN"],
  [AC_MSG_ERROR([gdbus-codegen not found])])

AX_PKG_CHECK_MODULES([GLIB], [glib-2.0 >= 2.48],
  [gmodule-2.0 gio-2.0 gthread-2.0 gio-unix-2.0])
AX_PKG_CHECK_MODULES([MOUNT], [], [mount])
AX_PKG_CHECK_MODULES([CANTERBURY], [],
  [canterbury-0 >= 0.9, canterbury-platform-0])
AX_PKG_CHECK_MODULES([SYSTEMD], [], [libsystemd])
AX_PKG_CHECK_MODULES([POLKIT], [], [polkit-gobject-1])
AX_PKG_CHECK_MODULES([OSTREE], [], [ostree-1])

AC_ARG_WITH([systemdsystemunitdir],
AS_HELP_STRING([--with-systemdsystemunitdir=DIR], [Directory for systemd service files]),
    [],
    [with_systemdsystemunitdir='${prefix}/lib/systemd/system'])
AC_SUBST([systemdsystemunitdir], [$with_systemdsystemunitdir])

# disable -Werror even on non-release builds until active development is resumed
AX_COMPILER_FLAGS([ERROR_CFLAGS], [ERROR_LDFLAGS], [yes])

# Installed tests
AC_REQUIRE_AUX_FILE([tap-driver.sh])

AC_ARG_ENABLE(modular_tests,
	 AS_HELP_STRING([--disable-modular-tests],
	 [Disable build of test programs (default: no)]),,
	 [enable_modular_tests=yes])

AC_ARG_ENABLE(installed_tests,
	 AS_HELP_STRING([--disable-installed-tests],
	 [Install test programs (default: enable)]),
	 [],
	 [enable_installed_tests=yes])

AM_CONDITIONAL(BUILD_MODULAR_TESTS,
	 [test "$enable_modular_tests" = "yes" ||
	 test "$enable_installed_tests" = "yes"])
AM_CONDITIONAL(BUILDOPT_INSTALL_TESTS, test "$enable_installed_tests" = "yes")

GLIB_GSETTINGS

# code coverage
AX_CODE_COVERAGE

AC_CONFIG_FILES([
Makefile
ribchester.pc
])

AC_OUTPUT

dnl Summary

echo "
		     ribchester 
                      -------------- 
         documentation                  : ${enable_documentation}
	 code coverage 			: ${enable_code_coverage}
	 compiler warings		: ${enable_compile_warnings}
	 Test suite 			: ${enable_modular_tests}
	 Install tests 			: ${enable_installed_tests}
"
