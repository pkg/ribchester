#!/usr/bin/make -f
# -*- makefile -*-
# Sample debian/rules that uses debhelper.
# This file was originally written by Joey Hess and Craig Small.
# As a special exception, when this file is copied by dh-make into a
# dh-make output file, you may use that output file without restriction.
# This special exception was added by Craig Small in version 0.37 of dh-make.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1
#export DH_OPTIONS=-v 

include /usr/share/dpkg/default.mk

%:
	dh $@ --with gir

override_dh_autoreconf:
	NOCONFIGURE=1 dh_autoreconf ./autogen.sh --

override_dh_auto_configure:
	dh_auto_configure \
		-- \
		--enable-documentation \
		--enable-installed-tests \
		--libexecdir='$${prefix}/lib' \
		--with-systemdsystemunitdir=/lib/systemd/system \
		$(NULL)

override_dh_auto_test:
	VERBOSE=1 dh_auto_test

override_dh_install:
	# Libtool .la files are undesired in Debian packages
	rm -f debian/tmp/usr/lib/*/*.la
	# Fail the build if anything else wasn't installed
	dh_install --fail-missing
	dh_apparmor -pribchester-core \
		--profile-name=usr.lib.ribchester.ribchester-core
	dh_apparmor -pribchester-full \
		--profile-name=usr.bin.ribchester
	dh_apparmor -pribchester-tests \
		--profile-name=usr.lib.installed-tests.ribchester

override_dh_strip:
	dh_strip --dbgsym-migration='ribchester-dbg (<< 0.1706.1-0co2~)'

# All public symbols must be in a .symbols file, unless this is overridden
# below.
override_dh_makeshlibs:
	dh_makeshlibs -- -c4

override_dh_systemd_start:
	dh_systemd_start -pribchester-common \
		--no-stop-on-upgrade \
		--no-restart-after-upgrade \
		var-Applications.mount \
		var-Applications.automount \
		Applications.mount \
		Applications.automount \
		run-ribchester-general.mount \
		run-ribchester-general.automount \
		${NULL}
	dh_systemd_start -pribchester-core \
		ribchester-core.service \
		${NULL}
	dh_systemd_start -pribchester-full \
		ribchester.service \
		${NULL}

# If this is not a release, relax checks
ifeq ($(DEB_DISTRIBUTION),UNRELEASED)

# We can't expect the symbols file to be fully up to date for a snapshot,
# so only fail if we broke ABI. This overrides dh_makeshlibs -- -c4 above.
export DPKG_GENSYMBOLS_CHECK_LEVEL = 1

endif
