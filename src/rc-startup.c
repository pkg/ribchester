/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*******************************************************************************
 * @Project Includes
 ******************************************************************************/

#include "rc-internal.h"

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>

#include <canterbury/canterbury-platform.h>
#include <gio/gdesktopappinfo.h>
#include <gio/gio.h>
#include <systemd/sd-daemon.h>

#include "mounts.h"

typedef enum _enBasicMounts enBasicMounts;
enum _enBasicMounts
{
	SYSTEM_ALT_RW_PATH,
	VARIANT_ALT_RW_PATH,
	HOME_RW_PATH,
	BASIC_MOUNTS_LAST
};

const gchar *pBasicMounts[BASIC_MOUNTS_LAST] =
{
		SYS_ALTERNATE_RW_PATHS,
		VARIANTS_ALTERNATE_RW_PATHS,
		HOME_RW_PATHS,
};


typedef enum _enFwDirs enFwDirs;
enum _enFwDirs
{
	MM_SYSTEM_SUBVOL,
	MM_APPSTORE_DOWNLOADS,
	MM_SYSTEM_IMAGE_DOWNLOADS,
	MM_DIRS_LAST
};

/* Subdirectory of rc_general_rw_mount_point () reserved for system data
 * (i.e. anything that is not an app-bundle). */
#define SYSTEM_DATA_SUBVOL "applications/System"

static void rc_mount_standard_paths (void);

static const gchar *pFwDirs[MM_DIRS_LAST];

/*
 * rc_startup_initialize_directories:
 *
 * Initialize working directory paths used by ribchester-full
 */
void
rc_startup_initialize_directories (void)
{
  pFwDirs [MM_APPSTORE_DOWNLOADS] =
      g_build_filename (rc_general_rw_mount_point (), SYSTEM_DATA_SUBVOL,
                        "Appstore-Downloads", NULL);

  /* FIXME: creating this inside the "applications" subvolume is a
   * historical accident; now that we have /run/ribchester/general
   * it should probably be elsewhere, since it isn't really anything
   * to do with app-bundles */
  pFwDirs [MM_SYSTEM_SUBVOL] =
      g_build_filename (rc_general_rw_mount_point (), SYSTEM_DATA_SUBVOL,
                        "System-Subvolumes", NULL);

  pFwDirs [MM_SYSTEM_IMAGE_DOWNLOADS] =
      g_build_filename ("/", "Applications",
                        "System-Image-Downloads", NULL);
}

const gchar *
rc_get_general_partition_path (void)
{
  return rc_general_rw_mount_point ();
}

inline  const gchar* rc_get_system_subvol_path (void)
{
	return pFwDirs[MM_SYSTEM_SUBVOL];
}

inline const gchar* rc_get_system_image_path (void)
{
	return pFwDirs[MM_SYSTEM_IMAGE_DOWNLOADS];
}

inline const gchar* rc_get_apps_dwnld_path (void)
{
	return pFwDirs[MM_APPSTORE_DOWNLOADS];
}

static void
rc_queue_last_mode_apps (RcBundleManager *bundle_manager)
{
	gchar **appList = NULL;
	guint i;
	/* Read the application schemas and read the priority. Apps mentioned in this list have the highest priority for mount */
	GSettings*  schema = g_settings_new(APP_DETAILS_SCHEMA);

	if(NULL != schema)
	{
		appList = g_settings_get_strv (schema, LAST_MODE_APPS);
	}

	for (i = 0; appList[i] != NULL; i++)
	{
		if (cby_is_bundle_id (appList[i]))
			rc_bundle_manager_add_startup_app (bundle_manager,
				appList[i]);
		else
			WARNING ("Not a syntactically valid app-bundle: \"%s\"",
				appList[i]);
	}

	g_strfreev (appList);
	g_clear_object (&schema);
}

#if 0
static void check_application_boot_configuration (void)
{
	if(NULL == proxyChaiwalaUpdater)
	{
		RIBCHESTER_TRACE ("Cannot read Boot Type. No apps will be mounted \n");
	}
	else
	{
		GError *error;
		chaiwala_updater_call_get_boot_type_sync (proxyChaiwalaUpdater, &bootType, NULL, &error);
		if (NULL != error)
		{
			RIBCHESTER_TRACE ("Cannot read Boot Type. No apps will be mounted. Err: %s \n", error->message);
		}
		else
		{
			/*
			   Get the type of boot currently underway:
			   0 - Normal boot (same subvolume as previous boot)
			   1 - Succesful update (booted subvolume is new)
			   2 - Rollback, failed update (boot failed after an update, rollback happened)
			   3 - Rollback, unknown reason (boot failed after an ordinary reboot, rollback happened) */
			switch (bootType)
			{
			case 1:
				break;
			case 0:
			case 2:
			case 3:
			default:
				mount_application_subvolumes();
				break;
			}
		}
	}
}
#endif

/* FIXME: use fd-based I/O at some point */
static void rc_create_system_related_dirs( void)
{
	guint uinCount = 0;
	for(; uinCount < MM_DIRS_LAST; uinCount++)
	{
		if(NULL != pFwDirs[uinCount])
		{
			/* Create the system subvolume folder */
			if (g_mkdir_with_parents (pFwDirs[uinCount],
				DIR_CREATE_MODE) < 0 && errno != EEXIST)
			{
				WARNING ("Cannot create \"%s\": %s", pFwDirs[uinCount], g_strerror (errno));
				continue;
			}
		}
	}
}


static gboolean
create_subvol_paths (GError **error)
{
  g_autofree gchar *applications = g_build_filename (rc_general_rw_mount_point (),
                                                     "applications", NULL);
  g_autofree gchar *system = g_build_filename (rc_general_rw_mount_point (),
                                               SYSTEM_DATA_SUBVOL, NULL);
  g_auto (RcFileDescriptor) general_fd = -1;
  g_auto (RcFileDescriptor) applications_fd = -1;
  GError *local_error = NULL;

  general_fd = rc_open_dir_at (NULL, AT_FDCWD, rc_general_rw_mount_point (),
                               RC_PATH_FLAGS_NONE, error);

  if (general_fd < 0)
    return FALSE;

  if (!rc_create_bind_source_at (rc_general_rw_mount_point (), general_fd,
                                     "applications", 0, 0, &local_error))
    {
      if (g_error_matches (local_error, RC_ERROR, RC_ERROR_EXISTS))
        {
          g_clear_error (&local_error);
        }
      else
        {
          g_propagate_error (error, local_error);
          return FALSE;
        }
    }

  applications_fd = rc_open_dir_at (rc_general_rw_mount_point (), general_fd,
                                    "applications", RC_PATH_FLAGS_NONE, error);

  if (applications_fd < 0)
    return FALSE;

  if (!rc_create_bind_source_at (applications, applications_fd,
                                     "System", 0, 0, &local_error))
    {
      if (g_error_matches (local_error, RC_ERROR, RC_ERROR_EXISTS))
        {
          g_clear_error (&local_error);
        }
      else
        {
          g_propagate_error (error, local_error);
          return FALSE;
        }
    }

  rc_create_system_related_dirs ();
  return TRUE;
}

static gboolean rc_create_subvol_for_mounting(gchar *pSubVolBasePath,
		gchar *pBaseROPath  )
{
	g_autoptr (GError) error = NULL;
	gchar *pBaseSubVolPath = g_build_filename("/",pSubVolBasePath,
			pBaseROPath, NULL);

	RIBCHESTER_TRACE("%s %d pBaseSubVolPath = %s pBaseROPath = %s \n" ,
			__FUNCTION__ , __LINE__ , pBaseSubVolPath , pBaseROPath );
	/* If the particular path is already mounted, then there is nothing to do */
	if(TRUE == rc_is_path_mounted (pBaseSubVolPath ))
	{
		g_message (" %s is already mounted. Nothing to do \n", pBaseSubVolPath);
		return  TRUE;
	}

	/* Check if the path exists and is a directory. If it is not,
	 * create it and copy data from  the system storage area */
	if (!rc_path_check_directory (pBaseSubVolPath, NULL))
	{
		g_auto (RcFileDescriptor) fd;
		g_autofree gchar *pSubVolName = g_path_get_basename(pBaseSubVolPath);
		g_autofree gchar *pSubVolParent = g_path_get_dirname(pBaseSubVolPath);

		fd = rc_open_dir_at (NULL, AT_FDCWD, pSubVolParent,
								RC_PATH_FLAGS_NONE, &error);

		if (fd < 0)
			return FALSE;

		/* Now create the new btrfs subvolume for the given path.
		 * Since it is fresh subvolume
		 * copy the contents from the system storage area into this */
		if (!rc_create_bind_source_at (pSubVolParent,
			fd, pSubVolName, 0, 0, &error))
		{
			g_message("Unable to create subvol: %s (error %s)\n", pBaseSubVolPath, error->message);
			return FALSE;
		}
	}
	return TRUE ;
}



static gboolean rc_check_if_base_subvol_created(
		gchar *pFileSystemROPath , const gchar *pSubVolBasePath)
{
	gboolean bSubVolCreated = FALSE ;
	/* split text wherever space and new line character are encountered */
	gchar *pProgBasePath = g_strdup(pSubVolBasePath);

	gchar **pDirTree = g_strsplit_set(pFileSystemROPath,"/",-1);
	gint inCount = 0 ;
	while(NULL != pDirTree[inCount])
	{
		if(strlen(pDirTree[inCount] ) > 0)
		{
			gchar *pTemp;

			bSubVolCreated = rc_create_subvol_for_mounting(
					pProgBasePath ,	pDirTree[inCount]);
			if(FALSE == bSubVolCreated)
			{
				/* Free if any */
				FREE_MEM_IF_NOT_NULL(pProgBasePath);
				g_warning("MountManager:MountSubVol:Failed to create subvol %s \n",pDirTree[inCount]);
				bSubVolCreated = FALSE ;
				break;
			}

			pTemp = pProgBasePath;
			pProgBasePath = g_build_filename("/" ,pProgBasePath ,pDirTree[inCount] , NULL  );
			FREE_MEM_IF_NOT_NULL(pTemp);
		}
		inCount++ ;
	}
	FREE_MEM_IF_NOT_NULL(pProgBasePath);
	g_strfreev(pDirTree);
	return bSubVolCreated;
}




static gboolean rc_check_and_create_alt_paths(
		gchar *pAltDirToCreate )
{
	gboolean bProceedCopy = FALSE ;

	if(FALSE == g_file_test(pAltDirToCreate , G_FILE_TEST_EXISTS))
	{
		if (g_mkdir_with_parents (pAltDirToCreate, DIR_CREATE_MODE) != 0)
		{
			g_warning("Error occurred while creating dir %s \n ",pAltDirToCreate);
			return FALSE;
		}
		bProceedCopy = TRUE ;
	}
	else
	{
		/* check if it contains any contents */
		GDir *pDir = g_dir_open (pAltDirToCreate, 0, NULL);
		if(pDir != NULL)
		{
			const gchar *pFileName  =  g_dir_read_name(pDir) ;
			if (NULL == pFileName )
				bProceedCopy = TRUE ;
		}
		g_dir_close(pDir);
	}
	return bProceedCopy ;
}



static void rc_copy_data_from_read_only_partition(
		gchar **pVarCopySrc , gchar *pBasePath , gchar *pAltMountPoint,
		gboolean bMountHome , guint inPriority )
{
	guint uinCount = 0 ;
	const	gchar *pHostName = NULL ;
	while (	pVarCopySrc[uinCount] != NULL )
	{
		/* Add the Data to Queue */
		gchar *pAltDirToCreate = (gchar *)g_build_filename ("/",
				pBasePath,pVarCopySrc[uinCount] , NULL);

		RIBCHESTER_TRACE("pFileSystemROPath  =  %s \n" , pVarCopySrc[uinCount]  );
		if(TRUE == rc_check_and_create_alt_paths( pAltDirToCreate ))
		{
			/* Note: All the string data has to be freed once the operation is completed */
			SubvolCopyInfo *pCopyInfo = g_new0(SubvolCopyInfo , 1);

			if(TRUE == bMountHome)
			{
				pHostName = g_get_host_name();
				g_assert(NULL != pHostName);
				pCopyInfo->pSourceDirPath = (gchar *)g_build_filename ("/",
						pAltMountPoint ,pHostName ,pVarCopySrc[uinCount], NULL);
				FREE_MEM_IF_NOT_NULL(pVarCopySrc[uinCount]);

				pCopyInfo->enCopyType = COPY_MEDIA_CONTENTS ;
			}
			else
			{
				pCopyInfo->enCopyType = COPY_SYSTEM_DATA ;
				pCopyInfo->pSourceDirPath = pVarCopySrc[uinCount] ;
			}
			pCopyInfo->pDestDirPath = pAltDirToCreate;
			pCopyInfo->pDestSubVolName = NULL ;
			pCopyInfo->priority =	inPriority;
			pCopyInfo->pMountPoint  = g_strdup(pAltMountPoint) ;
			rc_add_to_startup_copy_queue(pCopyInfo);
		}
		uinCount++;
	}
}



static void rc_mount_standard_paths(void)
{
	GSettings*  pSchema = g_settings_new(MOUNT_POINT_SCHEMA);
	const gchar *pHostName = NULL ;
	if(pSchema != NULL)
	{
		guint uinCount = 0;
		for(;uinCount < BASIC_MOUNTS_LAST ; uinCount++)
		{
			GVariant *variant_arg = g_settings_get_value(pSchema , pBasicMounts[uinCount]);
			GVariantIter iter;
			gchar *pAltMountPoint = NULL ;
			gint  inPriority  ;
			gchar **pVarCopySrc;

			g_variant_iter_init (&iter, variant_arg);

			/* refer "GVariant Format Strings" */
			while (g_variant_iter_next (&iter, "(si^as)",&pAltMountPoint, &inPriority , &pVarCopySrc) )
			{
				gchar *pBasePath = NULL;
				const gchar *pSubVolBasePath = NULL;
				gboolean bMountHome = FALSE;

				if(NULL == pAltMountPoint
						|| strlen(pAltMountPoint) < 1 || inPriority < 1 )
				{
					g_warning("Invalid Schema key-value pair specified   " );
					continue;
				}
				RIBCHESTER_TRACE(" pAltMountPoint = %s  pPriority = %d \n" ,pAltMountPoint , inPriority);

				if(0 == g_strcmp0( pBasicMounts[uinCount] , HOME_RW_PATHS))
				{
					bMountHome = TRUE ;
					pHostName = g_get_host_name();
	                                g_assert(NULL != pHostName);

					/* FIXME: this assumes that the
					 * target subvolume (in practice /home)
					 * is at a path on the general
					 * partition corresponding to its
					 * mount point (e.g.
					 * /run/ribchester/general/home),
					 * and that the path within that
					 * subvolume matches the hostname
					 * (which has not been true since
					 * we renamed the default user to
					 * "user", and would never be
					 * a useful assumption on a
					 * multi-user system in any case). */
					pBasePath = g_build_filename (
							rc_get_general_partition_path (), pAltMountPoint, pHostName, NULL);
					pSubVolBasePath = g_build_filename (
							rc_get_general_partition_path (), NULL);
				}
				else
				{
					pBasePath = (gchar *)g_build_filename ("/",
							rc_get_system_subvol_path(),  NULL);
					pSubVolBasePath = rc_get_system_subvol_path();
				}

				if(FALSE == rc_check_if_base_subvol_created(
						pAltMountPoint,	pSubVolBasePath))
				{
					g_warning("Error occurred while copying. Ignoring %s \n ",pAltMountPoint);
					continue ;
				}

				if(NULL == pVarCopySrc)
					continue;

				/* Initiate Copy of data from RO file system to General RW file system */
				rc_copy_data_from_read_only_partition(
						pVarCopySrc , pBasePath ,pAltMountPoint , bMountHome , inPriority);

				/*  check and mount if no paths are present for copying */
				if( TRUE == rc_check_mount_possibility(pAltMountPoint , 0))
				{
					gchar *pBaseSubVolPath = NULL ;

					if(FALSE == bMountHome)
					{
						pBaseSubVolPath = g_build_filename("/",rc_get_system_subvol_path(),
								pAltMountPoint, NULL);
					}
					else
					{
						/* FIXME: similar assumptions
						 * to those described above */
						pBaseSubVolPath = g_build_filename (rc_get_general_partition_path (),
								pAltMountPoint, NULL);
					}

					rc_mount_async (pBaseSubVolPath,
						pAltMountPoint,
						MOUNT_PRIO_HIGH,
						NULL, NULL, NULL);
					g_free (pBaseSubVolPath);
				}

				FREE_MEM_IF_NOT_NULL(pBasePath);
			}
		}
		rc_initiate_startup_copy();
	}
	else
	{
		g_critical("MountManager:BasicMount: Unable to find settings entry schema");
	}
}

static void
mount_startup_apps_cb (GObject *source_object,
                       GAsyncResult *result,
                       gpointer user_data)
{
  GError *error = NULL;

  if (rc_bundle_manager_mount_startup_apps_finish (RC_BUNDLE_MANAGER (source_object),
                                                   result, &error))
    DEBUG ("Successfully mounted startup apps");
  else
    WARNING ("Failed to mount startup apps: %s", error->message);

  g_clear_error (&error);

  DEBUG ("notifying systemd that we are ready");
  sd_notify (0, "READY=1");
}

static gboolean
rc_handle_normal_boot_sequence (RcBundleManager *bundle_manager,
                                GError **error)
{
	if (create_subvol_paths (error))
	{
		/* Standard paths specified in schema are always mounted. App mount is decided
				   based on the application boot flags */
		/* App mount management will change after the chaiwala updater service is ready */
		rc_mount_standard_paths();

		rc_queue_last_mode_apps (bundle_manager);
		rc_bundle_manager_queue_app_bundles (bundle_manager);
		rc_bundle_manager_mount_startup_apps_async (bundle_manager,
			NULL, mount_startup_apps_cb, NULL);
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

gboolean
rc_startup_tasks (RcBundleManager *bundle_manager,
                  GError **error)
{
	gint inBootType = rc_read_boot_flags();

	RIBCHESTER_TRACE("%s %d Boot Type = %d \n",__FUNCTION__ , __LINE__ ,inBootType);

	switch(inBootType)
	{
	case RIBCHESTER_BOOT_TYPE_NORMAL:
	{
		return rc_handle_normal_boot_sequence (bundle_manager, error);
	}
	break;
	case RIBCHESTER_BOOT_TYPE_SUCCESSFUL_UPDATE:
	{
		return rc_handle_normal_boot_sequence (bundle_manager, error);
	}
	break;
	case RIBCHESTER_BOOT_TYPE_FAILED_UPDATE:
	{

	}
	break;
	case RIBCHESTER_BOOT_TYPE_ROLLBACK:
	{

	}
	break;
	case RIBCHESTER_BOOT_TYPE_UNKNOWN:
	default:
	{
		g_warning("Ribchester: Unable to determine the boot type.");
		return rc_handle_normal_boot_sequence (bundle_manager, error);
	}
	break;
	}

	return TRUE;
}

gint
rc_read_boot_flags (void)
{
	int inBootType = RIBCHESTER_BOOT_TYPE_UNKNOWN;
	return inBootType;
}

