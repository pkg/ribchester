/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 * Copyright © 2016-2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "bundle-manager-provider.h"

#include <canterbury/canterbury-platform.h>
#include <glib/gstdio.h>
#include <gio/gunixfdlist.h>

#include "rc-appstore-transaction.h"
#include <ribchester/ribchester.h>

/*
 * RcBundleManagerProvider:
 *
 * The implementation of the #RibchesterBundleManager1 D-Bus interface.
 * This interface provides installation, upgrades and removal
 * for store app-bundles.
 */

struct _RcBundleManagerProvider
{
  /*< private >*/
  RibchesterBundleManager1Skeleton parent_instance;
  RcBundleManager *bundle_manager;
  PolkitAuthority *authority;
};

static void bundle_manager1_iface_init (RibchesterBundleManager1Iface *iface);

G_DEFINE_TYPE_WITH_CODE (RcBundleManagerProvider, rc_bundle_manager_provider,
                         RIBCHESTER_TYPE_BUNDLE_MANAGER1_SKELETON,
                         G_IMPLEMENT_INTERFACE (RIBCHESTER_TYPE_BUNDLE_MANAGER1,
                                                bundle_manager1_iface_init))

static gboolean
remove_bundle_cb (RibchesterBundleManager1 *object,
                  GDBusMethodInvocation *invocation,
                  const gchar *bundle_id)
{
  RcBundleManagerProvider *self = RC_BUNDLE_MANAGER_PROVIDER (object);
  GError *error = NULL;

  if (!rc_bundle_manager_remove_sync (self->bundle_manager, bundle_id, &error))
    {
      g_dbus_method_invocation_return_gerror (invocation, error);
      g_error_free (error);
      return TRUE;
    }

  ribchester_bundle_manager1_complete_remove_bundle (object, invocation);
  return TRUE;
}

static void
rc_bundle_manager_provider_constructed (GObject *object)
{
  RcBundleManagerProvider *self = RC_BUNDLE_MANAGER_PROVIDER (object);

  g_return_if_fail (RC_IS_BUNDLE_MANAGER (self->bundle_manager));

  G_OBJECT_CLASS (rc_bundle_manager_provider_parent_class)->constructed (object);
}

static gboolean
install_bundle_cb (RibchesterBundleManager1 *object,
                   GDBusMethodInvocation *invocation,
                   GUnixFDList *fd_list,
                   GVariant *bundle)
{
  RcBundleManagerProvider *self = RC_BUNDLE_MANAGER_PROVIDER (object);
  g_autoptr (RcAppStoreTransactionProvider) transaction = NULL;
  g_autofree gchar *path = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree gint *bundle_fds = NULL;
  gint handle = g_variant_get_handle (bundle);
  const gchar *sender;

  /* Only one fd should be passed the one for the bundle handle */
  if (handle > g_unix_fd_list_get_length (fd_list)
      || g_unix_fd_list_get_length (fd_list) != 1)
    {
      g_dbus_method_invocation_return_error (invocation,
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_INVALID_ARGS,
                                             "Incorrect fd list");
      return TRUE;
    }

  bundle_fds = g_unix_fd_list_steal_fds (fd_list, NULL);
  sender = g_dbus_method_invocation_get_sender (invocation);
  transaction = rc_app_store_transaction_provider_new (self->bundle_manager,
                                                       self->authority,
                                                       bundle_fds[handle],
                                                       sender);

  path = g_strdup_printf ("/org/apertis/Ribchester/AppStoreTransaction_%p",
                          transaction);

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (transaction),
                                         g_dbus_method_invocation_get_connection (invocation),
                                         path, &error))
    {
      g_dbus_method_invocation_return_gerror (invocation, error);
      return TRUE;
    }

  ribchester_bundle_manager1_complete_install_bundle (object, invocation,
                                                      NULL, path);
  rc_app_store_transaction_provider_start (transaction);

  return TRUE;
}

static const gchar *install_bundle_methods[] = { "InstallBundle", NULL };
static const gchar *manage_bundles_methods[] = { "RemoveBundle", NULL };


static const struct {
  const gchar *action_id;
  const gchar **methods;
} action_id_mapping[] = {
  { "org.apertis.ribchester.install-bundle",
    install_bundle_methods },
  { "org.apertis.ribchester.manage-applications",
    manage_bundles_methods },
};

static const gchar *
rc_bundle_manager_provider_lookup_action_id (const gchar *method)
{
  guint i;

  for (i = 0 ; i < G_N_ELEMENTS (action_id_mapping); i++ )
    {
      if (g_strv_contains (action_id_mapping[i].methods, method))
        return action_id_mapping[i].action_id;
    }

  return NULL;
}


static gboolean
rc_bundle_manager_provider_authorize (GDBusInterfaceSkeleton *interface,
                                      GDBusMethodInvocation *invocation,
                                      gpointer user_data)
{
  RcBundleManagerProvider *self = RC_BUNDLE_MANAGER_PROVIDER (interface);
  g_autoptr (PolkitSubject) subject = NULL;
  g_autoptr (PolkitAuthorizationResult) result = NULL;
  g_autoptr (GError) error = NULL;
  const gchar *sender;
  const gchar *action_id;
  const gchar *method;
  PolkitCheckAuthorizationFlags flags =
    POLKIT_CHECK_AUTHORIZATION_FLAGS_ALLOW_USER_INTERACTION;

  method = g_dbus_method_invocation_get_method_name (invocation);
  action_id = rc_bundle_manager_provider_lookup_action_id (method);

  if (action_id == NULL)
    {
      g_dbus_method_invocation_return_error (invocation,
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_FAILED,
                                             "Unknown method: %s",
                                             method);
      return FALSE;
    }

  sender = g_dbus_method_invocation_get_sender (invocation);
  subject = polkit_system_bus_name_new (sender);

  result =
    polkit_authority_check_authorization_sync (self->authority,
                                               subject,
                                               action_id,
                                               NULL,
                                               flags,
                                               NULL,
                                               &error);

  if (result == NULL)
    {
      g_dbus_method_invocation_return_error (invocation,
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_FAILED,
                                             "Checking polkit authorization "
                                             "failed: %s",
                                             error->message);
      return FALSE;
    }

  if (!polkit_authorization_result_get_is_authorized (result))
    {
      g_dbus_method_invocation_return_error (invocation,
                                             G_DBUS_ERROR,
                                             G_DBUS_ERROR_ACCESS_DENIED,
                                             "Action not authorized.");
      return FALSE;
    }

  return TRUE;
}

static void
rc_bundle_manager_provider_init (RcBundleManagerProvider *self)
{
  g_signal_connect (self, "g-authorize-method",
                    G_CALLBACK (rc_bundle_manager_provider_authorize),
                    NULL);
}

typedef enum
{
  PROP_BUNDLE_MANAGER = 1,
  PROP_AUTHORITY,
} Property;

static void
rc_bundle_manager_provider_get_property (GObject *object,
                                         guint prop_id,
                                         GValue *value,
                                         GParamSpec *pspec)
{
  RcBundleManagerProvider *self = RC_BUNDLE_MANAGER_PROVIDER (object);

  switch ((Property) prop_id)
    {
      case PROP_BUNDLE_MANAGER:
        g_value_set_object (value, self->bundle_manager);
        break;

      case PROP_AUTHORITY:
        g_value_set_object (value, self->authority);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
rc_bundle_manager_provider_set_property (GObject *object,
                                         guint prop_id,
                                         const GValue *value,
                                         GParamSpec *pspec)
{
  RcBundleManagerProvider *self = RC_BUNDLE_MANAGER_PROVIDER (object);

  switch ((Property) prop_id)
    {
      case PROP_BUNDLE_MANAGER:
        /* construct-only */
        g_assert (self->bundle_manager == NULL);
        self->bundle_manager = g_value_dup_object (value);
        break;

      case PROP_AUTHORITY:
        /* construct-only */
        g_assert (self->authority == NULL);
        self->authority = g_value_dup_object (value);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
rc_bundle_manager_provider_dispose (GObject *object)
{
  RcBundleManagerProvider *self = RC_BUNDLE_MANAGER_PROVIDER (object);

  g_clear_object (&self->bundle_manager);
  g_clear_object (&self->authority);

  G_OBJECT_CLASS (rc_bundle_manager_provider_parent_class)->dispose (object);
}

static void
rc_bundle_manager_provider_class_init (RcBundleManagerProviderClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);
  GParamSpec *property_specs[PROP_AUTHORITY + 1] = { NULL };

  property_specs[PROP_BUNDLE_MANAGER] =
    g_param_spec_object ("bundle-manager", "Bundle manager",
                         "The bundle manager used to implement this interface",
                         RC_TYPE_BUNDLE_MANAGER,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
  property_specs[PROP_AUTHORITY] =
    g_param_spec_object ("authority", "Authority",
                         "The Polkit Authority to check permissions against",
                         POLKIT_TYPE_AUTHORITY,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  object_class->constructed = rc_bundle_manager_provider_constructed;
  object_class->get_property = rc_bundle_manager_provider_get_property;
  object_class->set_property = rc_bundle_manager_provider_set_property;
  object_class->dispose = rc_bundle_manager_provider_dispose;

  g_object_class_install_properties (object_class,
                                     G_N_ELEMENTS (property_specs),
                                     property_specs);
}

RcBundleManagerProvider *
rc_bundle_manager_provider_new (RcBundleManager *bundle_manager,
                                PolkitAuthority *authority)
{
  return g_object_new (RC_TYPE_BUNDLE_MANAGER_PROVIDER,
                       "bundle-manager", bundle_manager,
                       "authority", authority,
                       NULL);
}

static void
bundle_manager1_iface_init (RibchesterBundleManager1Iface *iface)
{
  iface->handle_remove_bundle = remove_bundle_cb;
  iface->handle_install_bundle = install_bundle_cb;
}
