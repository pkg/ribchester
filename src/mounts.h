/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <glib.h>

#include "fdio.h"

/* Partition on which we create applications' subvolumes,
 * from which they are mounted into the appropriate place. */
const gchar *rc_general_rw_mount_point (void);

/* Device node symlink that backs rc_general_rw_mount_point () */
const gchar *rc_general_rw_device_node (void);

gchar *rc_get_device_from_mount (const gchar *path);
gboolean rc_is_device_mounted_at_path (const gchar *device,
                                       const gchar *path);
gboolean rc_is_path_mounted (const gchar *path);

gboolean rc_unmount (const gchar *mount_point,
                     GError **error);

gboolean rc_bind_mount (const gchar *source_path,
                        const gchar *mount_point,
                        GError **error);
gboolean rc_create_bind_source_at (const gchar *log_parent_path,
                                   RcFileDescriptor parent,
                                   const gchar *name,
                                   guint uid,
                                   guint gid,
                                   GError **error);
gboolean rc_delete_bind_source (const gchar *parent_path,
                                const gchar *basename,
                                GError **error);
gboolean rc_snapshot_bind_source (const gchar *source_path,
                                  const gchar *parent_path,
                                  const gchar *name,
                                  GError **error);
