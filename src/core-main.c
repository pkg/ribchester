/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <string.h>

#include <glib.h>
#include <gio/gio.h>
#include <polkit/polkit.h>
#include <systemd/sd-daemon.h>

#include "app-helper.h"
#include "bundle-manager.h"
#include "bundle-manager-provider.h"
#include <ribchester/ribchester.h>
#include "util.h"

static void
stop_main_loop_cb (GObject *nil G_GNUC_UNUSED,
                   GAsyncResult *result,
                   gpointer user_data)
{
  g_main_loop_quit (user_data);
}

static void
name_lost_cb (GDBusConnection *system_bus,
              const gchar *name,
              gpointer user_data)
{
  GTask *task = user_data;

  g_task_return_new_error (task, RC_ERROR, RC_ERROR_FAILED,
                           "Lost or could not acquire bus name \"%s\"", name);
}

static void
name_acquired_cb (GDBusConnection *system_bus,
                  const gchar *name,
                  gpointer user_data)
{
  GTask *task = user_data;
  int ret = sd_notify (0, "READY=1");

  if (ret > 0)
    {
      DEBUG ("Acquired name \"%s\" and notified systemd that we are ready",
             name);
    }
  else if (ret == 0)
    {
      DEBUG ("Acquired name \"%s\", but not started by systemd", name);
    }
  else
    {
      g_task_return_new_error (task, RC_ERROR, RC_ERROR_FAILED,
                               "Unable to notify systemd that we are ready: %s",
                               strerror (-ret));
    }
}

static void
mount_startup_apps_cb (GObject *source,
                       GAsyncResult *result,
                       gpointer user_data)
{
  RcBundleManager *bundle_manager = RC_BUNDLE_MANAGER (source);
  g_autoptr (GTask) run_task = user_data;
  g_autoptr (GError) error = NULL;
  GDBusConnection *system_bus = g_task_get_task_data (run_task);

  if (!rc_bundle_manager_mount_startup_apps_finish (bundle_manager, result,
                                                    &error))
    {
      g_task_return_error (run_task, g_steal_pointer (&error));
      return;
    }

  g_bus_own_name_on_connection (system_bus,
                                RC_BUS_NAME,
                                G_BUS_NAME_OWNER_FLAGS_NONE,
                                name_acquired_cb,
                                name_lost_cb,
                                g_object_ref (run_task), g_object_unref);
}

int
main (int argc,
      char *argv[])
{
  g_autoptr (GDBusConnection) system_bus = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GMainLoop) main_loop = NULL;
  g_autoptr (GTask) run_task = NULL;
  g_autoptr (PolkitAuthority) authority = NULL;
  g_autoptr (RcBundleManager) bundle_manager = NULL;
  g_autoptr (RcBundleManagerProvider) bundle_manager_provider = NULL;
  g_autoptr (RcAppHelper) app_helper = NULL;

  rc_setenv_disable_services ();

  system_bus = g_bus_get_sync (G_BUS_TYPE_SYSTEM, NULL, &error);

  if (system_bus == NULL)
    {
      CRITICAL ("Unable to connect to D-Bus: %s", error->message);
      return 1;
    }

  authority = polkit_authority_get_sync (NULL, &error);

  if (authority == NULL)
    {
      CRITICAL ("Unable to set up polkit authority: %s", error->message);
      return 1;
    }

  bundle_manager = rc_bundle_manager_new (system_bus, &error);

  if (bundle_manager == NULL)
    {
      CRITICAL ("Unable to initialize app-bundle manager: %s", error->message);
      return 1;
    }

  app_helper = rc_app_helper_new (bundle_manager);
  g_return_val_if_fail (app_helper != NULL, 1);

  bundle_manager_provider = rc_bundle_manager_provider_new (bundle_manager,
                                                            authority);
  g_return_val_if_fail (bundle_manager_provider != NULL, 1);

  if (!g_dbus_interface_skeleton_export (
          G_DBUS_INTERFACE_SKELETON (app_helper), system_bus,
          "/org/apertis/Canterbury/PrivilegedAppHelper1", &error) ||
      !g_dbus_interface_skeleton_export (
          G_DBUS_INTERFACE_SKELETON (bundle_manager_provider), system_bus,
          RC_OBJECT_PATH_BUNDLE_MANAGER1, &error))
    {
      /* can't happen: the only reason we can fail to export it is if
       * we have exported something else on the same path */
      CRITICAL ("%s", error->message);
      return 1;
    }

  main_loop = g_main_loop_new (NULL, FALSE);
  run_task = g_task_new (NULL, NULL, stop_main_loop_cb, main_loop);
  g_task_set_task_data (run_task, g_object_ref (system_bus), g_object_unref);

  rc_bundle_manager_queue_app_bundles (bundle_manager);
  rc_bundle_manager_mount_startup_apps_async (bundle_manager, NULL,
                                              mount_startup_apps_cb,
                                              g_object_ref (run_task));

  g_main_loop_run (main_loop);
  g_assert (g_task_get_completed (run_task));

  if (!g_task_propagate_boolean (run_task, &error))
    {
      CRITICAL ("Unable to finish initialization: %s", error->message);
      return 1;
    }

  return 0;
}
