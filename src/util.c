/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016-2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "util.h"

/*
 * rc_setenv_disable_services:
 *
 * Set environment variables to disable use of daemons for GIO, GSettings
 * and similar pluggable modules.
 *
 * This function has the same lack of thread-safety as g_setenv(), and
 * can only be called safely before the main program has had an opportunity
 * to start any threads.
 *
 * See:
 * https://bugs.freedesktop.org/show_bug.cgi?id=95487
 * https://bugzilla.gnome.org/show_bug.cgi?id=767182
 * https://bugzilla.gnome.org/show_bug.cgi?id=767183
 */
void
rc_setenv_disable_services (void)
{
  g_setenv ("GIO_USE_VFS", "local", TRUE);
  g_setenv ("GIO_USE_VOLUME_MONITOR", "unix", TRUE);
  g_setenv ("GSETTINGS_BACKEND", "memory", TRUE);
  g_setenv ("GVFS_DISABLE_FUSE", "1", TRUE);
}
