/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "fdio.h"

#include <errno.h>
#include <fcntl.h>
#include <linux/magic.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/vfs.h>
#include <unistd.h>

#include <ribchester/ribchester.h>

#include "util.h"

#ifndef __NR_copy_file_range
  #if defined(__x86_64__)
    #define __NR_copy_file_range 326
  #elif defined(__ARM_EABI__)
    #define __NR_copy_file_range 391
  #elif defined __aarch64__
    #define __NR_copy_file_range 285
  #else
    #error copy_file_range syscall number unknown
  #endif
#endif

/*
 * rc_open_dir_at:
 * @log_path: (nullable): something identifying @dirfd for debug/error messages,
 *  or %NULL to use a placeholder
 * @dirfd: an open file descriptor to a directory, or `AT_FDCWD`
 * @path: a path relative to @dirfd
 * @flags: flags affecting how path is interpreted
 * @error: used to raise a %RC_ERROR if @basename cannot be opened
 *
 * Open @path relative to @dirfd, or raise %RC_ERROR_NOT_FOUND if it
 * does not exist, or another %RC_ERROR if it exists but cannot be opened.
 *
 * If @dirfd is `AT_FDCWD`, then @path is evaluated relative to the current
 * working directory, as it would be for `openat(2)`. See the `openat`
 * documentation for details.
 *
 * If the last component of @path is a symbolic link, the presence or
 * absence of %RC_PATH_FLAGS_NOFOLLOW_SYMLINKS in @flags determines whether
 * the symbolic link is dereferenced. Symbolic links earlier in @path
 * are always followed.
 *
 * @log_path may be an absolute or relative path, or a placeholder
 * such as `<app-bundles directory>`. It will be used in debug/error messages
 * as though it was the path to @dirfd. This function never uses @log_path
 * for the actual file I/O.
 *
 * Returns: a file descriptor >= 0, or -1 on error
 */
RcFileDescriptor
rc_open_dir_at (const gchar *log_path,
                RcFileDescriptor dirfd,
                const gchar *path,
                RcPathFlags flags,
                GError **error)
{
  int openat_flags = O_RDONLY | O_DIRECTORY | O_NONBLOCK | O_CLOEXEC;
  const gchar *separator = G_DIR_SEPARATOR_S;
  RcFileDescriptor fd;

  g_return_val_if_fail (dirfd == AT_FDCWD || dirfd >= 0, -1);
  g_return_val_if_fail (path != NULL, -1);
  g_return_val_if_fail (error == NULL || *error == NULL, -1);
  g_return_val_if_fail (flags == (flags & RC_PATH_FLAGS_NOFOLLOW_SYMLINKS), -1);

  if (log_path == NULL)
    {
      if (dirfd == AT_FDCWD)
        log_path = ".";
      else
        log_path = "…";
    }

  if (g_path_is_absolute (path))
    {
      log_path = "";
      separator = "";
    }

  if (flags & RC_PATH_FLAGS_NOFOLLOW_SYMLINKS)
    openat_flags |= O_NOFOLLOW;

  fd = openat (dirfd, path, openat_flags);

  if (fd < 0)
    {
      if (errno == ENOENT)
        g_set_error (error, RC_ERROR, RC_ERROR_NOT_FOUND,
                     "\"%s%s%s\" does not exist", log_path, separator, path);
      else
        g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                     "Cannot open \"%s%s%s\": %s", log_path, separator, path,
                     g_strerror (errno));

      return -1;
    }

  return fd;
}

/*
 * The first fd after stdin, stdout, stderr
 */
#define FIRST_NORMAL_FD 3

/*
 * rc_directory_iter_new:
 * @fd: a file descriptor pointing to a directory
 *
 * Returns: a `DIR *` to iterate through the members of @fd
 */
RcDirectoryIter *
rc_directory_iter_new (RcFileDescriptor fd,
                       GError **error)
{
  g_auto (RcFileDescriptor) copy = -1;
  RcDirectoryIter *ret;

  g_return_val_if_fail (fd != AT_FDCWD, NULL);
  g_return_val_if_fail (fd >= 0, NULL);

  copy = fcntl (fd, F_DUPFD_CLOEXEC, FIRST_NORMAL_FD);

  if (copy < 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Unable to duplicate file descriptor %d: %s",
                   fd, g_strerror (errno));
      return NULL;
    }

  g_assert (copy >= FIRST_NORMAL_FD);

  ret = fdopendir (copy);

  if (ret == NULL)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Unable to open file descriptor %d as directory: %s",
                   fd, g_strerror (errno));
      return NULL;
    }

  /* If fdopendir() succeeds, it takes ownership */
  copy = -1;

  return ret;
}

/**
 * rc_durable_symlink_at:
 * @target: the target of the new symbolic link
 * @log_parent_path: (nullable): the location of @parent_fd, or %NULL to use
 *  a placeholder; this is only used for debug/error messages
 * @parent_fd: an open fd in the directory to use as the parent (`AT_FDCWD`
 *  is not allowed here)
 * @link_name: symlink name, which must not contain path separators
 * @error: used to raise a %RC_ERROR on failure; in particular,
 *  %RC_ERROR_EXISTS means that @link_name already existed and was not
 *  overwritten.
 *
 * Create a symbolic link named @link_name relative to @parent_fd, pointing
 * to @target.
 *
 * If @link_name already exists, it will cause %RC_ERROR_EXISTS,
 * unless %RC_PATH_FLAGS_OVERWRITE was given.
 *
 * The symbolic link creation is atomic, in the sense that if @link_name
 * exists, and the process crashes during execution of this function,
 * @link_name is guaranteed to either have its old contents or be a symlink
 * pointing to @target: it will not be deleted or have different contents.
 * Similarly, if @link_name does not exist, it will either be replaced with
 * a symbolic link to @target or continue to not exist.
 *
 * As much as possible, the same atomicity properties apply if the entire
 * system crashes or loses power.
 *
 * This function may create a file named @link_name + `.tmp` in @parent_fd,
 * which might remain present if the process or system crashes. Do not rely
 * on the existence or contents of this file, which is not necessarily
 * created atomically. The only manipulation that should be done to this
 * file is deletion.
 *
 * Returns: %TRUE if successful
 */
gboolean
rc_durable_symlink_at (const gchar *target,
                       const gchar *log_parent_path,
                       RcFileDescriptor parent_fd,
                       const gchar *link_name,
                       RcPathFlags flags,
                       GError **error)
{
  g_autofree gchar *tmp = NULL;

  g_return_val_if_fail (target != NULL, FALSE);
  g_return_val_if_fail (parent_fd != AT_FDCWD, FALSE);
  g_return_val_if_fail (parent_fd >= 0, FALSE);
  g_return_val_if_fail (link_name != NULL, FALSE);
  g_return_val_if_fail (link_name[0] != '\0', FALSE);
  /* The current implementation does not actually require that the link name
   * doesn't contain these, but if we need to call fsync (parent_fd) later,
   * it will make life simpler if this is true. */
  g_return_val_if_fail (strchr (link_name, '/') == NULL, FALSE);
  g_return_val_if_fail (strcmp (link_name, ".") != 0, FALSE);
  g_return_val_if_fail (strcmp (link_name, "..") != 0, FALSE);
  /* This is the only flag that makes sense here */
  g_return_val_if_fail (flags == (flags & RC_PATH_FLAGS_OVERWRITE), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (log_parent_path == NULL)
    log_parent_path = "…";

  tmp = g_strconcat (link_name, ".tmp", NULL);

  /* The temporary file might be left over from a previous manipulation to
   * the same directory. */
  if (unlinkat (parent_fd, tmp, 0) != 0 && errno != ENOENT)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Unable to remove temporary file \"%s/%s\": %s",
                   log_parent_path, tmp, g_strerror (errno));
      return FALSE;
    }

  if (symlinkat (target, parent_fd, tmp) != 0)
    {
      if (errno == EEXIST)
        {
          /* If this fails with EEXIST, then we must be racing with another
           * process that was also trying to make the same symlink. That would
           * be bad, so bail out. We don't use RC_ERROR_EXISTS because it would
           * be somewhat misleading when it isn't @link_name that exists,
           * and we don't currently have a specific RC_ERROR_CONFLICTING (or
           * similar) because it isn't clear that we'd ever want to handle it
           * differently. */
          g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                       "Unable to create symbolic link \"%s/%s\" → \"%s\": %s "
                       "(conflicting with another process also trying to "
                       "create \"%s\"?)",
                       log_parent_path, tmp, target, g_strerror (errno),
                       link_name);
        }
      else
        {
          g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                       "Unable to create symbolic link \"%s/%s\" → \"%s\": %s",
                       log_parent_path, tmp, target, g_strerror (errno));
        }

      return FALSE;
    }

  /* On btrfs, a renameat() that overwrites a file is atomic and durable,
   * but a renameat() that does not overwrite a file is not; so make sure
   * there is something there for us to overwrite. Another symbolic link to
   * the same file will do.
   *
   * A secondary purpose of this symlinkat() call is that it detects the case
   * where the file already exists, when we don't want to be overwriting
   * an existing file. We can't use renameat2() with RENAME_NOREPLACE
   * because the kernel we use on i.mx6 doesn't support that. */
  if (symlinkat (target, parent_fd, link_name) == 0)
    {
      /* OK: @link_name didn't already exist. To ensure that btrfs commits it,
       * we'll still do an atomic overwrite with another copy of the same
       * thing. */
    }
  else if (errno == EEXIST)
    {
      if (!(flags & RC_PATH_FLAGS_OVERWRITE))
        {
          g_set_error (error, RC_ERROR, RC_ERROR_EXISTS,
                       "\"%s/%s\" already exists",
                       log_parent_path, link_name);

          if (unlinkat (parent_fd, tmp, 0) != 0)
            {
              WARNING ("\"%s/%s\" already exists, but unable to remove "
                       "\"%s/%s\": %s",
                       log_parent_path, link_name, log_parent_path, tmp,
                       g_strerror (errno));
            }

          return FALSE;
        }
    }
  else
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Unable to create symbolic link \"%s/%s\" → \"%s\": %s",
                   log_parent_path, link_name, target, g_strerror (errno));
      return FALSE;
    }

  /* By the time we get here, we know it's OK to overwrite link_name:
   * either we wanted to overwrite it, or it is now a symbolic link to
   * the same thing as tmp anyway. */
  if (renameat (parent_fd, tmp, parent_fd, link_name) != 0)
    {
      if (errno == ENOENT)
        {
          /* If tmp has been deleted, that's probably because we're racing
           * with another process that just passed its unlinkat() call.
           * Bail out, and don't use RC_ERROR_NOT_FOUND (similar reasoning
           * to the EEXIST case above). */
          g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                       "Unable to rename \"%s/%s\" to \"%s/%s\": %s "
                       "(conflicting with another process also trying to "
                       "create \"%s\"?)",
                       log_parent_path, tmp, log_parent_path, link_name,
                       g_strerror (errno), link_name);
        }
      else
        {
          g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                       "Unable to rename \"%s/%s\" to \"%s/%s\": %s",
                       log_parent_path, tmp, log_parent_path, link_name,
                       g_strerror (errno));

          if (unlinkat (parent_fd, tmp, 0) != 0)
            {
              WARNING ("Unable to rename \"%s/%s\" to \"%s/%s\", but cannot "
                       "remove it either: %s",
                       log_parent_path, tmp, log_parent_path, link_name,
                       g_strerror (errno));
            }
        }

      return FALSE;
    }

  return TRUE;
}


typedef struct {
  RcFileDescriptor input;
  RcFileDescriptor output;
} CloneOperationData;

static void
clone_operation_data_free (gpointer data)
{
  g_slice_free (CloneOperationData, data);
}

static void
clone_operation_data_run_thread (GTask *task,
                                 gpointer source_object,
                                 gpointer task_data,
                                 GCancellable *cancellable)
{
  CloneOperationData *data = task_data;
  struct stat fd_stat;
  loff_t off_in = 0;
  loff_t off_out = 0;
  off_t len;
  int ret = 0;

  if (fstat (data->input, &fd_stat) < 0)
    goto error;

  len = fd_stat.st_size;
  /* First try copy_file_range, available in linux >= 4.6. */
  while (len > 0)
    {
      if (g_task_return_error_if_cancelled (task))
        return;

      ret = copy_file_range (data->input, &off_in,
                             data->output, &off_out,
                             MIN (len, 1024 * 1024 * 1024),
                             0);

      if (ret == 0)
        break;

      if (ret < 0)
        {
          if (errno == EINTR)
            continue; /* Interrupted, retry */
          else
            break;
        }

      len -= ret;
    }

  if (ret != -1)
    {
      g_task_return_boolean (task, TRUE);
      return;
    }

  if (errno != ENOSYS && errno != EXDEV)
    goto error;

  /* copy_file_range didn't succeed, back to old school
   * copying on the CPU */
  {
    guint8 buffer[4096];
    off_t offset = 0;
    ssize_t bytes;

    for (;;)
      {
        off_t boffset = 0;

        if (g_task_return_error_if_cancelled (task))
          return;

        bytes = pread (data->input, buffer, sizeof (buffer), offset);
        if (bytes == 0)
          break;

        if (bytes < 0)
          {
            if (errno == EINTR)
              continue;

            goto error;
          }

        while (bytes > 0)
          {
            ret = pwrite (data->output, buffer + boffset, bytes, offset);

            if (ret < 0)
              {
                if (errno == EINTR)
                  continue;

                goto error;
              }

            offset += ret;
            boffset += ret;
            bytes -= ret;
          }
      }
  }

  g_task_return_boolean (task, TRUE);

  return;
error:
  g_task_return_new_error (task,
                           RC_ERROR, RC_ERROR_FAILED,
                           "%s", g_strerror (errno));

}

/*
 * rc_file_descriptor_clone_content_sync:
 * @input: Input file descriptor
 * @output: output file descriptor
 * @cancellable: (nullable): a #GCancellable object or %NULL
 * @error: Return location for a #GError
 *
 * Synchronous version of rc_file_descriptor_clone_content_async()
 *
 * Returns: boolean to indicate whether the clone oepration was successful
 * otherwise @error willl be set
 */
gboolean
rc_file_descriptor_clone_content_sync (RcFileDescriptor input,
                                       RcFileDescriptor output,
                                       GCancellable *cancellable,
                                       GError **error)
{
  CloneOperationData *data = g_slice_new (CloneOperationData);
  g_autoptr (GTask) task = g_task_new (NULL, cancellable, NULL, NULL);

  data->input = input;
  data->output = output;

  g_task_set_task_data (task, data, clone_operation_data_free);
  g_task_run_in_thread_sync (task, clone_operation_data_run_thread);

  return g_task_propagate_boolean (task, error);
}

/*
 * rc_file_descriptor_clone_content_async:
 * @input: Input file descriptor
 * @output: output file descriptor
 * @cancellable: (nullable): a #GCancellable object or %NULL
 * @callback: callback to be called when the operation is finished
 * @user_data: The data to pass to @callback
 *
 * Clone the content from @input to @output, both should be normal files. An
 * optimized clone will be attempted to be done (e.g. a reflink where supported)
 * falling back to a straight copy if not possible.
 *
 * Both the @input and @output file descriptors should be kept open while this
 * operation is in progress.
 *
 * This is an asynchronous funciton; use
 * rc_file_descriptor_clone_content_finish() from @callback to get the its
 * return value.
 */
void
rc_file_descriptor_clone_content_async (RcFileDescriptor input,
                                        RcFileDescriptor output,
                                        GCancellable *cancellable,
                                        GAsyncReadyCallback callback,
                                        gpointer user_data)
{
  CloneOperationData *data = g_slice_new (CloneOperationData);
  g_autoptr (GTask) task = g_task_new (NULL, cancellable, callback, user_data);

  data->input = input;
  data->output = output;

  g_task_set_task_data (task, data, clone_operation_data_free);
  g_task_run_in_thread (task, clone_operation_data_run_thread);
}

/*
 * rc_file_descriptor_clone_content_finish:
 * @res: asynchronous results
 * @error: Return location for a #GError
 *
 * see rc_file_descriptor_clone_content_async()
 *
 * Returns: boolean to indicate whether the clone oepration was successful
 * otherwise @error willl be set
 */
gboolean
rc_file_descriptor_clone_content_finish (GAsyncResult *res, GError **error)
{
  return g_task_propagate_boolean (G_TASK (res), error);
}
