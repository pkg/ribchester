/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rc-internal.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <libmount.h>
#include <canterbury/canterbury-platform.h>
#include <polkit/polkit.h>

#include "app-helper.h"
#include "bundle-manager-provider.h"
#include "rc-appstore.h"

static GMainLoop *main_loop;
static RcAppHelper *app_helper;
static RcAppStoreProvider *app_store;
static RcBundleManager *bundle_manager;
static RcBundleManagerProvider *bundle_manager_provider;

/*
   TO DO's
 	* <prio 1>
   	*
   	* Identify the factory reset image.
	* Bifurcate accounts and user data from the system
	* 		DEVICE DATA	(across versions)
	* 		SYSTEM DATA(Version Specific)
	* 			includes APP SPECIFIC DATA
	* 		USER SHARED DATA.
	*
	* <prio 2>
   	* Check space before taking snapshot
	* Test generic interface
	* last mode schema update during shutdown
	* Make sure not to accept blank space when creating/renaming/mounting subvolumes.
	* Proper fix for reading Application schemas. Not based on assumption

   	1. <done>Data backup on update<done\>
	2. <check\><done>Should we always ensure a backup on upgrade - /var/lib/Service && extensions to be snapshotted??<done\>
	4. <done>update schemas (Maintain schemas for specific images - store apps schema)<done\>
	7. <done>Read boot flags on startup<done\>
	8. PDI creation after mount process(dropped)
	9. <done>Handle Use case: Update-> Uninstall->Reinstall sequence where two snapshots are present for the same app
		and the snapshot with old time stamp is restored instead of the latest version.<done/><check/>



	Platform:
	1.	clean the var/cache/apt/ run sudo apt-get clean to remove all backup debians as the
	rootfs is  not meant to be modified when RO
	2. Remove man directory from target
	3. Implement ReadOnly FileSystem
			http://www.linuxfromscratch.org/hints/downloads/files/readonly_rootfs.txt
			https://sites.google.com/site/linuxpendrive/rorootfs
			https://wiki.debian.org/ReadonlyRoot
			https://wiki.gentoo.org/wiki/Btrfs_system_root



 */

RibchesterData pRibchesterData;

static GAsyncQueue* mountQueue = NULL;
static GThread* mountThread = NULL;

static gint priority_insert (gconstpointer entry1, gconstpointer entry2, gpointer userData);
static gpointer mount_thread (gpointer data);

static void
subvol_mount_data_free (SubvolMountData *self)
{
  g_free (self->subvolPath);
  g_free (self->mountPath);
  g_free (self->commonName);
  g_free (self);
}

static gboolean
record_successful_mount_cb (gpointer user_data)
{
  SubvolMountData *mount_data = user_data;

  if (app_store != NULL)
    ribchester_app_store_emit_mount_status (RIBCHESTER_APP_STORE (app_store),
                                            mount_data->commonName, TRUE,
                                            mount_data->mountPath);

  subvol_mount_data_free (mount_data);
  return G_SOURCE_REMOVE;
}

static gboolean
record_failed_mount_cb (gpointer user_data)
{
  SubvolMountData *mount_data = user_data;

  if (app_store != NULL)
    ribchester_app_store_emit_mount_status (RIBCHESTER_APP_STORE (app_store),
                                            mount_data->commonName, FALSE,
                                            mount_data->mountPath);

  subvol_mount_data_free (mount_data);
  return G_SOURCE_REMOVE;
}

static gpointer mount_thread (gpointer data)
{
  while (1)
    {
      g_autoptr (GError) error = NULL;
      GTask *task = g_async_queue_pop (mountQueue);
      SubvolMountData *mount_data = g_task_get_task_data (task);

      /* Create mount point if needed */
      if (!rc_chkdir (mount_data->mountPath))
        {
          if (g_mkdir_with_parents (mount_data->mountPath,
                                    DIR_CREATE_MODE) < 0)
            {
              g_set_error (&error, RC_ERROR, RC_ERROR_FAILED,
                           "Cannot create directory %s: %s",
                           mount_data->mountPath, g_strerror(errno));
           }
        }

      if (!error)
        {
          rc_bind_mount (mount_data->subvolPath,
                         mount_data->mountPath, &error);
        }

      if (!error)
        {
          /* We shouldn't act on main-thread objects from here,
           * because we're in a child thread. Post the result
           * back to the main thread. */
          g_main_context_invoke (NULL, record_successful_mount_cb, mount_data);
          g_task_return_boolean (task, TRUE);
        }
      else
        {
          DEBUG ("%s", error->message);
          g_main_context_invoke (NULL, record_failed_mount_cb, mount_data);
          /* The task will take ownership of @error */
          g_task_return_error (task, g_steal_pointer (&error));
        }

       g_object_unref (task);
    }

  return NULL;
}

static gint priority_insert (gconstpointer entry1, gconstpointer entry2, gpointer userData)
{
	GTask *task1 = G_TASK (entry1);
	GTask *task2 = G_TASK (entry2);
	SubvolMountData* mntData1 = g_task_get_task_data (task1);
	SubvolMountData* mntData2 = g_task_get_task_data (task2);

	if( (NULL != mntData1) && (NULL != mntData2) )
		return (mntData1->priority > mntData2->priority ? -1 : mntData1->priority == mntData2->priority ? 0 : +1);

	return 0;
}

/*
 * @subvol_path: the absolute path to the subvolume to be mounted
 * @mount_point: the absolute path where @subvol_path should be mounted
 */
void
rc_mount_async (const gchar *subvol_path,
    const gchar *mount_point,
    mountPriority priority,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data)
{
  SubvolMountData *mount_data;
  GTask *task;

  g_return_if_fail (g_path_is_absolute (subvol_path));
  g_return_if_fail (g_path_is_absolute (mount_point));

  mount_data = g_new0 (SubvolMountData, 1);
  mount_data->subvolPath = g_strdup (subvol_path);
  mount_data->mountPath = g_strdup (mount_point);
  /* FIXME: surely this should be @priority? but this is what the code
   * always did */
  mount_data->priority = MOUNT_PRIO_HIGH;
  mount_data->commonName = g_path_get_basename (subvol_path);

  task = g_task_new (NULL, cancellable, callback, user_data);
  g_task_set_task_data (task, mount_data, NULL);
  g_task_set_source_tag (task, rc_mount_async);
  g_async_queue_push_sorted (mountQueue, task,
      (GCompareDataFunc) priority_insert, NULL);
}

gboolean
rc_mount_finish (GAsyncResult *result,
    GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, NULL), FALSE);
  g_return_val_if_fail (g_async_result_is_tagged (result, rc_mount_async),
      FALSE);
  return g_task_propagate_boolean (G_TASK (result), error);
}

gboolean rc_chkdir (const gchar *path)
{
	gboolean isDir = FALSE;
	struct stat buf;

	if (stat(path, &buf) < 0) 
	{
		RIBCHESTER_TRACE( "Unable to access '%s': %s\n", path, strerror(errno));
	}
	else 
	{
		if (S_ISDIR(buf.st_mode)) 
		{
			isDir = TRUE;
		}
	}

	return isDir; 
}

gint rc_remove_dir (const gchar *path)
{
	gint retVal = rmdir (path);

	if(retVal < 0)
		RIBCHESTER_TRACE( "Unable to delete '%s' folder due to foll error: %s\n", path, strerror(errno));

	return retVal;
}

static void _rc_initialize_to_default(void)
{
	pRibchesterData.pCopyThreadPool = NULL ;
	rc_startup_initialize_directories ();
}

void
__rc_quit_service (const gchar *pErrorMessage,
	gint inExitStatus)
{

	/* Todo: Generate Log mechanism to enable tracing */
	if(NULL != pErrorMessage)
		g_warning("Mount-Manager:Terminate:Error:%s\n",pErrorMessage);

	exit(inExitStatus);
}

static void
rc_on_bus_acquired (GDBusConnection *connection,
	const gchar *name,
	gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (PolkitAuthority) authority;

  authority = polkit_authority_get_sync (NULL, &error);
  if (authority == NULL)
    {
      /* Can't get a polkit authority which means we can't authorize any
       * methods */
      CRITICAL ("%s", error->message);
      g_main_loop_quit (main_loop);
      return;
    }

  g_return_if_fail (bundle_manager != NULL);
  app_helper = rc_app_helper_new (bundle_manager);
  app_store = rc_app_store_provider_new (bundle_manager, authority);
  bundle_manager_provider =
      rc_bundle_manager_provider_new (bundle_manager, authority);

  if (!g_dbus_interface_skeleton_export (
          G_DBUS_INTERFACE_SKELETON (app_helper), connection,
          "/org/apertis/Canterbury/PrivilegedAppHelper1", &error) ||
      !g_dbus_interface_skeleton_export (
          G_DBUS_INTERFACE_SKELETON (bundle_manager_provider), connection,
          RC_OBJECT_PATH_BUNDLE_MANAGER1, &error) ||
      !g_dbus_interface_skeleton_export (
          G_DBUS_INTERFACE_SKELETON (app_store), connection,
          "/org/apertis/Ribchester/AppStore", &error))
    {
      /* can't happen: the only reason we can fail to export it is if
       * we have exported something else on the same path */
      CRITICAL ("%s", error->message);
      g_main_loop_quit (main_loop);
    }
}

static void
rc_on_name_lost (GDBusConnection *connection,
	const gchar *name,
	gpointer user_data)
{
	/* free all D-Bus handlers */
	g_clear_object (&app_helper);
	g_clear_object (&app_store);

	DEBUG ("D-Bus name lost (or never acquired), terminating.");
	g_main_loop_quit (main_loop);
}


gint main(gint argc, gchar *argv[])
{
	g_autoptr (GDBusConnection) system_bus = NULL;
	GError *error = NULL;

	rc_setenv_disable_services ();

	/* Initialize all variables to default state*/
	_rc_initialize_to_default();

	system_bus = g_bus_get_sync (G_BUS_TYPE_SYSTEM, NULL, &error);

	if (system_bus == NULL)
	{
		CRITICAL ("Unable to connect to D-Bus: %s", error->message);
		goto out;
	}

	bundle_manager = rc_bundle_manager_new (system_bus, &error);

	if (bundle_manager == NULL)
	{
		CRITICAL ("Unable to initialize bundle manager: %s",
			error->message);
		goto out;
	}

	g_bus_own_name (	G_BUS_TYPE_SYSTEM,           // bus type
			RC_BUS_NAME,
			G_BUS_NAME_OWNER_FLAGS_NONE,  // bus own flag, can be used to take away the bus and give it to another service
			rc_on_bus_acquired,              // callback invoked when the bus is acquired
			NULL,                            // callback invoked when interface name is acquired
			rc_on_name_lost,                 // callback invoked when name is lost to another service or other reason
			NULL,                         // user data
			NULL);                        // user data free func

	/* Create a mount thread and a queue for all mounts. All mount requests are pushed into the
	   queue in the main thread and are mounted in the worker thread. hash table stores a mao
	   of subvolpath->mount details for any mounted subvolume */
	mountQueue = g_async_queue_new();
	mountThread = g_thread_new ("mount_thread", mount_thread, NULL);

	/* Read Boot flags from the boot partition and proceed start up
	 * task based on that */
	if (!rc_startup_tasks (bundle_manager, &error))
	{
		CRITICAL ("%s", error->message);
		goto out;
	}

	main_loop = g_main_loop_new (NULL, FALSE);
	g_main_loop_run (main_loop);

out:
	g_clear_pointer (&main_loop, g_main_loop_unref);
	g_clear_object (&app_helper);
	g_clear_object (&app_store);
	g_clear_object (&bundle_manager);

	/* under normal circumstances we run forever, so if the main loop
	 * exits, something must be wrong */
	return 1;
}




