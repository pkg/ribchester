/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __RIBCHESTER_INTERNAL_H__
#define __RIBCHESTER_INTERNAL_H__



/*******************************************************************************
 * @System Includes
 ******************************************************************************/
#include <glib.h>
#include <gio/gio.h>

/*******************************************************************************
 * @Project Includes
 ******************************************************************************/
#include <ribchester/ribchester.h>

#include "bundle-manager.h"
#include "fdio.h"
#include "mounts.h"
#include "util.h"

/*******************************************************************************
 * Enable / Disable Development codes for target
 ******************************************************************************/
/* @ENABLE_FLAG_READ : Enable reading actual flag status
 * Preferred : 0*/
#define ENABLE_FLAG_READ 0


/* @ENABLE_PDI : Enable reading boot flags from PDI . Temp fix
 * Preferred : 0*/
#define ENABLE_PDI 0

/* ******************* GSchema Names **************************/
#define MOUNT_POINT_SCHEMA "org.apertis.Ribchester.BasicMountPaths"
#define GENERAL_MOUNT_SCHEMA "org.apertis.Ribchester.GeneralMntMgr"
#define APP_DETAILS_SCHEMA "org.apertis.Ribchester.AppDetails"
/* ******************* GSchema Names Ends **************************/




/* ******************* GSchema Keys **************************/


/*_______________________ Expected Keys  in @MOUNT_POINT_SCHEMA ________*/
#define SYS_ALTERNATE_RW_PATHS	"sys-alternate-rw-path"
#define VARIANTS_ALTERNATE_RW_PATHS "variants-rw-path"
#define HOME_RW_PATHS "home-rw-path"

/*_______________________ Expected Keys  in @GENERAL_MOUNT_SCHEMA ________*/
#define GENERAL_RW_PARTION_KEY "mount-path"

/*_______________________ Expected Keys  in @APP_DETAILS_SCHEMA ________*/
#define LAST_MODE_APPS "last-mode-apps"

/*_______________________ Expected Keys in each  Application schemas ________*/
/* @STOREAPPS_APPLICATION_NAME: For appstore apps , this key is read to obtain the
 * application name. This is because btrfs subvolume is created for the application
 * based on its name.
 */
#define STOREAPPS_APPLICATION_NAME "app-name"
#define INBUILT_APPS_WORKING_DIR "working-directory"
#define INBUILT_APPS_KEY "exec-type"
#define INBUILT_APPS_TYPE "application"
#define INBUILT_STARTUP_TYPE "startup-application"
/* ******************* GSchema Keys Ends**************************/


/* *********************** System Specific *******************/

#define FLAGS_PARTITION "/dev/disk/by-partlabel/flags"
#define BACKUP_FLAGS_PARTITION "/dev/disk/by-partlabel/flags_backup"


/* ******************* Internal **************************/
#define SNAPSHOT_NAME_PREFIX "Snapshot-"
#define GENERIC_APP_BACKUP_PREFIX "Backup-"
#define INBUILT_APPS_SCHEMA_PREFIX "com"
#define RIBCHESTER_COPY_THREAD "Mount Mgr Copy-Thread"
/* Strings that is expected in schemas to know the priority for copying data.
 * Any string mismatch will lead to low priority assignment for such copy process */
#define COPY_HAS_HIGH_PRIORITY		"high"
#define COPY_HAS_LOW_PRIORITY		"low"
#define COPY_HAS_MEDIUM_PRIORITY	"medium"

/* The permissions for normal directories: rwxr-xr-x */
#define DIR_CREATE_MODE 0755

#define DB_FIELD_NAME "sysupdatestate"

/* FIXME: replace all calls to this with the equivalent g_clear_pointer(),
 * or use g_autofree. https://phabricator.apertis.org/T1130 */
#define FREE_MEM_IF_NOT_NULL(pointer) g_clear_pointer (&pointer, g_free)

typedef struct _RibchesterData RibchesterData;
typedef struct _SubvolCopyInfo SubvolCopyInfo;
typedef struct _SubvolMountData SubvolMountData;
typedef enum _MountPriority mountPriority;
typedef enum _SubVolCopyType SubVolCopyType;

enum _MountPriority
{
	MOUNT_PRIO_LOW,
	MOUNT_PRIO_MEDIUM,
	MOUNT_PRIO_HIGH
};

enum _SubVolCopyType
{
	COPY_SYSTEM_DATA,
	COPY_GENERIC,
	COPY_MEDIA_CONTENTS
};


struct _SubvolCopyInfo
{
	gchar *pSourceDirPath ;		/* Source Path from where data has to be copied */
	gchar *pDestDirPath ;  		/* Destination Path from where data has to be copied */
	gchar *pDestSubVolName;		/* Name of the btrfs subvol for which data sync was done */
	gchar *pMountPoint;			/* Preferred Mount Point in case of start-up copy*/
	guint priority; 			/* Priority for copy operation */
	SubVolCopyType enCopyType ; /* Type of the copy initiated */
};

struct _SubvolMountData
{
	gchar *subvolName; /* Name of the subvolume as known by the system */
	gchar *subvolPath; /* Path where subvolume is available */
	gchar *mountPath;  /* Path where subvolume is mounted */
	gchar *commonName; /* name with which the applications recognise the subvolume */
	mountPriority priority; /* Priority for mount operation */
};



struct _RibchesterData
{
	GThreadPool* pCopyThreadPool;
};



/* Generic functions */
void rc_add_to_startup_copy_queue(SubvolCopyInfo *pCopyInfo);
void rc_initiate_startup_copy (void);
gboolean rc_check_mount_possibility(
		gchar* pMountPoint ,guint uinThreadNo);


gboolean rc_chkdir (const gchar *path);
gint rc_remove_dir (const gchar *path);
void rc_mount_async (const gchar *subvol_path,
    const gchar *mount_point,
    mountPriority priority,
    GCancellable *cancellable,
    GAsyncReadyCallback callback,
    gpointer user_data);
gboolean rc_mount_finish (GAsyncResult *result, GError **error);
//gpointer rc_activate_data_copy (gpointer data);

void __rc_quit_service (const gchar *pErrorMessage,
	gint inExitStatus) G_GNUC_NORETURN;

void rc_startup_initialize_directories (void);
const gchar *rc_get_general_partition_path (void);
const gchar* rc_get_system_subvol_path (void);
const gchar* rc_get_system_image_path (void);
const gchar* rc_get_apps_dwnld_path (void);
const gchar* rc_get_system_registry_path (void);

gint rc_read_boot_flags (void);

gboolean rc_path_check_directory (const gchar *subvol_path,
                                  GError **error);

gboolean rc_startup_tasks (RcBundleManager *bundle_manager,
                           GError **error);

/* FIXME: replace calls to this macro with plain DEBUG() at some point.
 * https://phabricator.apertis.org/T1129 */
#define RIBCHESTER_TRACE(format, ...) DEBUG (format, ##__VA_ARGS__)

#endif
