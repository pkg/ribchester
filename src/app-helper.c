/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "app-helper.h"

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <canterbury/canterbury-platform.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <gio/gio.h>

#include <ribchester/ribchester.h>

#include "rc-internal.h"

struct _RcAppHelper
{
  /*< private >*/
  CbyPrivilegedAppHelper1Skeleton parent_instance;
  RcBundleManager *bundle_manager;
};

G_DEFINE_TYPE (RcAppHelper, rc_app_helper,
               CBY_TYPE_PRIVILEGED_APP_HELPER1_SKELETON)

typedef enum {
    PROP_BUNDLE_MANAGER = 1,
} Property;

static void
rc_app_helper_get_property (GObject *object,
    guint prop_id,
    GValue *value,
    GParamSpec *pspec)
{
  RcAppHelper *self = RC_APP_HELPER (object);

  switch ((Property) prop_id)
    {
      case PROP_BUNDLE_MANAGER:
        g_value_set_object (value, self->bundle_manager);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
rc_app_helper_set_property (GObject *object,
    guint prop_id,
    const GValue *value,
    GParamSpec *pspec)
{
  RcAppHelper *self = RC_APP_HELPER (object);

  switch ((Property) prop_id)
    {
      case PROP_BUNDLE_MANAGER:
        /* construct-only */
        g_assert (self->bundle_manager == NULL);
        self->bundle_manager = g_value_dup_object (value);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
rc_app_helper_dispose (GObject *object)
{
  RcAppHelper *self = RC_APP_HELPER (object);

  g_clear_object (&self->bundle_manager);

  G_OBJECT_CLASS (rc_app_helper_parent_class)->dispose (object);
}

typedef struct
{
  RcBundleManager *bundle_manager;
  gchar *bundle_id;
  guint uid;
} PrepareAppBundleData;

static void
prepare_app_bundle_data_free (PrepareAppBundleData *data)
{
  g_free (data->bundle_id);
  g_object_unref (data->bundle_manager);
  g_slice_free (PrepareAppBundleData, data);
}

static void
prepare_app_bundle_done_cb (GObject *object,
    GAsyncResult *result,
    gpointer user_data)
{
  RcAppHelper *self = RC_APP_HELPER (object);
  GDBusMethodInvocation *invocation = user_data;
  GError *error = NULL;
  gchar *persistence_path = g_task_propagate_pointer (G_TASK (result), &error);

  if (persistence_path != NULL)
    cby_privileged_app_helper1_complete_prepare_app_bundle (
        CBY_PRIVILEGED_APP_HELPER1 (self), invocation, persistence_path);
  else
    g_dbus_method_invocation_return_gerror (invocation, error);

  g_clear_error (&error);
  g_clear_object (&invocation);
}

static void
prepare_app_bundle_do_io (GTask *task,
    gpointer source_object G_GNUC_UNUSED,
    gpointer task_data,
    GCancellable *cancellable)
{
  PrepareAppBundleData *data = task_data;
  GError *error = NULL;
  gchar *path;

  path = rc_bundle_manager_prepare_sync (data->bundle_manager, data->bundle_id,
                                         data->uid, cancellable, &error);

  if (path != NULL)
    g_task_return_pointer (task, path, g_free);
  else
    g_task_return_error (task, error);
}

static void
prepare_app_bundle_process_info_cb (GObject *nil G_GNUC_UNUSED,
    GAsyncResult *result,
    gpointer user_data)
{
  GTask *task = user_data;
  PrepareAppBundleData *data = g_task_get_task_data (task);
  GError *error = NULL;
  const gchar *label;
  CbyProcessInfo *process_info =
    cby_process_info_new_for_dbus_invocation_finish (result, &error);

  if (process_info == NULL)
    {
      g_task_return_new_error (task, RC_ERROR, RC_ERROR_FAILED,
          "Unable to identify caller: %s", error->message);
      goto finally;
    }

  if (cby_process_info_get_process_type (process_info) !=
      CBY_PROCESS_TYPE_PLATFORM)
    {
      g_task_return_new_error (task, RC_ERROR, RC_ERROR_FAILED,
          "PrepareAppBundle may only be called by platform processes");
      goto finally;
    }

  label = cby_process_info_get_apparmor_label (process_info);

  if (g_strcmp0 (label, "unconfined") != 0 &&
      g_strcmp0 (label, "/usr/bin/canterbury") != 0 &&
      g_strcmp0 (label, "/usr/bin/canterbury-exec") != 0 &&
      g_strcmp0 (label, "/usr/lib/installed-tests/ribchester/*") != 0)
    {
      g_task_return_new_error (task, RC_ERROR, RC_ERROR_FAILED,
          "PrepareAppBundle may only be called by Canterbury or an "
          "unconfined process");
      goto finally;
    }

  data->uid = cby_process_info_get_user_id (process_info);

  if (data->uid == CBY_PROCESS_INFO_NO_USER_ID)
    {
      g_task_return_new_error (task, RC_ERROR, RC_ERROR_FAILED,
          "Unable to determine user calling PrepareAppBundle");
      goto finally;
    }

  if (data->uid == 0)
    {
      g_task_return_new_error (task, RC_ERROR, RC_ERROR_FAILED,
          "root may not run app bundles");
      goto finally;
    }

  /* Go into a thread for everything that does I/O, so we don't block the
   * main loop. The result will be posted back to this thread. */
  g_task_run_in_thread (task, prepare_app_bundle_do_io);

finally:
  g_clear_object (&process_info);
  g_clear_error (&error);
  g_object_unref (task);
}

static gboolean
rc_app_helper_prepare_app_bundle (RcAppHelper *self,
    GDBusMethodInvocation *invocation,
    const gchar *bundle_id,
    guint flags,
    gpointer nil G_GNUC_UNUSED)
{
  GTask *task = g_task_new (self, NULL, prepare_app_bundle_done_cb,
      g_object_ref (invocation));
  PrepareAppBundleData *data;

  if (flags != 0)
    {
      g_task_return_new_error (task, RC_ERROR, RC_ERROR_FAILED,
          "PrepareAppBundle does not support any flags yet");
      goto finally;
    }

  data = g_slice_new0 (PrepareAppBundleData);
  data->bundle_manager = g_object_ref (self->bundle_manager);
  data->bundle_id = g_strdup (bundle_id);
  g_task_set_task_data (task, data,
      (GDestroyNotify) prepare_app_bundle_data_free);
  cby_process_info_new_for_dbus_invocation_async (invocation, NULL,
      prepare_app_bundle_process_info_cb, g_object_ref (task));

finally:
  g_object_unref (task);
  return TRUE;
}

static void
register_user_manager_done_cb (GObject *object,
                               GAsyncResult *result,
                               gpointer user_data)
{
  RcAppHelper *self = RC_APP_HELPER (object);
  g_autoptr (GDBusMethodInvocation) invocation = user_data;
  g_autoptr (GError) error = NULL;
  GTask *task = G_TASK (result);

  if (g_task_propagate_boolean (task, &error))
    cby_privileged_app_helper1_complete_register_user_manager (
        CBY_PRIVILEGED_APP_HELPER1 (self),
        invocation);
  else
    g_dbus_method_invocation_return_gerror (invocation, error);
}

static void
register_user_manager_process_info_cb (GObject *nil G_GNUC_UNUSED,
                                       GAsyncResult *result,
                                       gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (CbyProcessInfo) process_info =
      cby_process_info_new_for_dbus_invocation_finish (result, &error);
  RcAppHelper *self = g_task_get_source_object (task);
  GDBusMethodInvocation *invocation = g_task_get_task_data (task);
  const gchar *sender = g_dbus_method_invocation_get_sender (invocation);
  const gchar *label;

  if (process_info == NULL)
    {
      g_task_return_new_error (task, RC_ERROR, RC_ERROR_FAILED,
                               "Unable to identify caller: %s",
                               error->message);
      return;
    }

  if (cby_process_info_get_process_type (process_info) !=
      CBY_PROCESS_TYPE_PLATFORM)
    {
      g_task_return_new_error (task, RC_ERROR, RC_ERROR_FAILED,
                               "RegisterUserManager may only be called by "
                               "platform processes");
      return;
    }

  label = cby_process_info_get_apparmor_label (process_info);

  if (g_strcmp0 (label, "unconfined") != 0 &&
      g_strcmp0 (label, "/usr/bin/canterbury") != 0 &&
      g_strcmp0 (label, "/usr/lib/canterbury/canterbury-core") != 0 &&
      g_strcmp0 (label, "/usr/lib/installed-tests/ribchester/*") != 0)
    {
      g_task_return_new_error (task, RC_ERROR, RC_ERROR_FAILED,
                               "RegisterUserManager may only be called by "
                               "Canterbury or an unconfined process");
      return;
    }

  rc_bundle_manager_add_user_manager (self->bundle_manager, sender);
  g_task_return_boolean (task, TRUE);
}

static gboolean
rc_app_helper_register_user_manager (RcAppHelper *self,
                                     GDBusMethodInvocation *invocation,
                                     gpointer nil G_GNUC_UNUSED)
{
  g_autoptr (GTask) task = g_task_new (self, NULL,
                                       register_user_manager_done_cb,
                                       g_object_ref (invocation));

  g_task_set_task_data (task, g_object_ref (invocation), g_object_unref);
  cby_process_info_new_for_dbus_invocation_async (invocation, NULL,
      register_user_manager_process_info_cb, g_object_ref (task));

  return TRUE; /* handled */
}

static void
rc_app_helper_init (RcAppHelper *self)
{
}

static void
rc_app_helper_constructed (GObject *object)
{
  RcAppHelper *self = RC_APP_HELPER (object);

  g_return_if_fail (RC_IS_BUNDLE_MANAGER (self->bundle_manager));

  g_signal_connect (self, "handle-prepare-app-bundle",
                    G_CALLBACK (rc_app_helper_prepare_app_bundle), NULL);

  g_signal_connect (self, "handle-register-user-manager",
                    G_CALLBACK (rc_app_helper_register_user_manager), NULL);
}

static void
rc_app_helper_class_init (RcAppHelperClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);
  GParamSpec *property_specs[PROP_BUNDLE_MANAGER + 1] = { NULL };

  property_specs[PROP_BUNDLE_MANAGER] =
    g_param_spec_object ("bundle-manager", "Bundle manager",
                         "The bundle manager used to implement this interface",
                         RC_TYPE_BUNDLE_MANAGER,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  object_class->constructed = rc_app_helper_constructed;
  object_class->get_property = rc_app_helper_get_property;
  object_class->set_property = rc_app_helper_set_property;
  object_class->dispose = rc_app_helper_dispose;

  g_object_class_install_properties (object_class,
                                     G_N_ELEMENTS (property_specs),
                                     property_specs);
}

RcAppHelper *
rc_app_helper_new (RcBundleManager *bundle_manager)
{
  return g_object_new (RC_TYPE_APP_HELPER,
                       "bundle-manager", bundle_manager,
                       NULL);
}
