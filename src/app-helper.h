/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef RC_APP_HELPER_H
#define RC_APP_HELPER_H

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <canterbury/canterbury-platform.h>

#include "bundle-manager.h"

G_BEGIN_DECLS

#define RC_TYPE_APP_HELPER (rc_app_helper_get_type ())

G_DECLARE_FINAL_TYPE (RcAppHelper, rc_app_helper, RC, APP_HELPER,
                      CbyPrivilegedAppHelper1Skeleton)

RcAppHelper *rc_app_helper_new (RcBundleManager *bundle_manager);

G_END_DECLS

#endif
