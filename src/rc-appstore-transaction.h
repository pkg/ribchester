/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef RC_APP_STORE_TRANSACTION_H
#define RC_APP_STORE_TRANSACTION_H

#include <glib-object.h>
#include <glib.h>

#include <polkit/polkit.h>

#include "bundle-manager.h"
#include "ribchester/org.apertis.Ribchester.AppStore.Transaction.h"

G_BEGIN_DECLS

#define RC_TYPE_APP_STORE_TRANSACTION_PROVIDER (rc_app_store_transaction_provider_get_type ())

G_DECLARE_FINAL_TYPE (RcAppStoreTransactionProvider,
                      rc_app_store_transaction_provider,
                      RC,
                      APP_STORE_TRANSACTION_PROVIDER,
                      RibchesterAppStoreTransactionSkeleton)

RcAppStoreTransactionProvider *rc_app_store_transaction_provider_new (RcBundleManager *bundle_manager,
                                                                      PolkitAuthority *authority,
                                                                      int fd,
                                                                      const gchar *initiator);

void rc_app_store_transaction_provider_start (RcAppStoreTransactionProvider *self);

G_END_DECLS

#endif
