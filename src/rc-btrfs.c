/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rc-internal.h"

#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <ftw.h>

#include <libmount.h>

#define ROOT_BTRFS_SUBVOLID 0

/*
 * rc_path_check_directory:
 * @subdir_path: a path
 * @error: Return location for a #GError
 *
 * Test whether @subdir_path is a valid directory path.
 *
 * Returns: %TRUE if @subdir_path is a valid directory path.
 */
gboolean
rc_path_check_directory (const gchar *subdir_path, GError **error)
{
  struct stat buf;

  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (stat (subdir_path, &buf) < 0)
    {
      if (errno == ENOENT)
        g_set_error (error, RC_ERROR, RC_ERROR_NOT_FOUND,
                     "\"%s\" does not exist", subdir_path);
      else
        g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                     "Cannot open \"%s\": %s", subdir_path,
                     g_strerror (errno));
      return FALSE;
    }

  if (!S_ISDIR (buf.st_mode))
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "\"%s\" is not a directory", subdir_path);
      return FALSE;
    }

  return TRUE;
}
