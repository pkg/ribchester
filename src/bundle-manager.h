/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __RIBCHESTER_BUNDLE_MANAGER_H__
#define __RIBCHESTER_BUNDLE_MANAGER_H__

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include "fdio.h"

G_BEGIN_DECLS

#define RC_TYPE_BUNDLE_MANAGER (rc_bundle_manager_get_type ())

G_DECLARE_FINAL_TYPE (RcBundleManager, rc_bundle_manager, RC, BUNDLE_MANAGER,
                      GObject)

RcBundleManager *rc_bundle_manager_new (GDBusConnection *system_bus,
                                        GError **error);

gchar *rc_bundle_manager_prepare_sync (RcBundleManager *self,
                                       const gchar *bundle_id,
                                       guint user_id,
                                       GCancellable *cancellable,
                                       GError **error);

gboolean rc_bundle_manager_remove_sync (RcBundleManager *self,
                                        const gchar *bundle_id,
                                        GError **error);

gboolean rc_bundle_manager_delete_rollback_snapshot_sync (RcBundleManager *self,
                                                          const gchar *bundle_id,
                                                          GError **error);

gchar *rc_bundle_manager_begin_install_sync (RcBundleManager *self,
                                             const gchar *bundle_id,
                                             const gchar *version,
                                             GError **error);

void rc_bundle_manager_begin_install_async (RcBundleManager *self,
                                            const gchar *id,
                                            const gchar *version,
                                            GCancellable *cancellable,
                                            GAsyncReadyCallback callback,
                                            gpointer user_data);

gchar *rc_bundle_manager_begin_install_finish (RcBundleManager *self,
                                               GAsyncResult *res,
                                               GError** error);


gboolean rc_bundle_manager_commit_install_sync (RcBundleManager *self,
                                                const gchar *bundle_id,
                                                const gchar *version,
                                                GError **error);

void rc_bundle_manager_commit_install_async (RcBundleManager *self,
                                             const gchar *bundle_id,
                                             const gchar *version,
                                             GCancellable *cancellable,
                                             GAsyncReadyCallback callback,
                                             gpointer user_data);

gboolean rc_bundle_manager_commit_install_finish (RcBundleManager *self,
                                                  GAsyncResult *res,
                                                  GError **error);

gchar *rc_bundle_manager_roll_back_sync (RcBundleManager *self,
                                         const gchar *bundle_id,
                                         GError **error);

gboolean rc_bundle_manager_uninstall_sync (RcBundleManager *self,
                                           const gchar *bundle_id,
                                           GError **error);

gboolean rc_bundle_manager_reinstall_sync (RcBundleManager *self,
                                           const gchar *bundle_id,
                                           GError **error);

void rc_bundle_manager_queue_app_bundles (RcBundleManager *bundle_manager);
void rc_bundle_manager_add_startup_app (RcBundleManager *self,
                                        const gchar *bundle_id);
void rc_bundle_manager_mount_startup_apps_async (RcBundleManager *self,
                                                 GCancellable *cancellable,
                                                 GAsyncReadyCallback callback,
                                                 gpointer user_data);
gboolean rc_bundle_manager_mount_startup_apps_finish (RcBundleManager *self,
                                                      GAsyncResult *result,
                                                      GError **error);

void rc_bundle_manager_update_component_index_async (RcBundleManager *self,
                                                     GCancellable *cancellable,
                                                     GAsyncReadyCallback callback,
                                                     gpointer user_data);
gboolean rc_bundle_manager_update_component_index_finish (RcBundleManager *self,
                                                          GAsyncResult *result,
                                                          GError **error);
gboolean rc_bundle_manager_update_component_index_sync (RcBundleManager *self,
                                                        GCancellable *cancellable,
                                                        GError **error);
const gchar *rc_bundle_manager_get_temp_path (RcBundleManager *self);

void rc_bundle_manager_add_user_manager (RcBundleManager *self,
                                         const gchar *system_bus_unique_name);

G_END_DECLS

#endif
