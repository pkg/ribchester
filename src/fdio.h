/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __RIBCHESTER_FDIO_H__
#define __RIBCHESTER_FDIO_H__

#include <dirent.h>
#include <sys/types.h>

#include <glib.h>
#include <glib/gstdio.h>
#include <gio/gio.h>

G_BEGIN_DECLS

/* FIXME: maybe upstream this into glib-unix when we have more implementation
 * experience with it */
/*
 * RcFileDescriptor:
 *
 * An open file descriptor, or -1 to indicate the absence of a file descriptor.
 *
 * This is just a typedef for `int`, but unlike `int`, it can be used in
 * `g_auto (RcFileDescriptor)` to have the fd close automatically when it
 * goes out of scope.
 */
typedef int RcFileDescriptor;

static inline void
_rc_file_descriptor_close (RcFileDescriptor fd)
{
  g_close (fd, NULL);
}

G_DEFINE_AUTO_CLEANUP_FREE_FUNC (RcFileDescriptor, _rc_file_descriptor_close,
                                 -1)

/*
 * rc_file_descriptor_steal:
 * @fdp: a pointer to a file descriptor, which will be set to -1
 *
 * The equivalent of g_steal_pointer() for file descriptors.
 *
 * Returns: what the value of `*fdp` was on entry
 */
static inline RcFileDescriptor
rc_file_descriptor_steal (RcFileDescriptor *fdp)
{
  RcFileDescriptor tmp = *fdp;

  *fdp = -1;
  return tmp;
}

/*
 * rc_file_descriptor_clear:
 * @fdp: a pointer to a file descriptor, which will be set to -1
 *
 * The equivalent of g_clear_object() for file descriptors.
 */
static inline void
rc_file_descriptor_clear (RcFileDescriptor *fdp)
{
  RcFileDescriptor tmp = *fdp;

  *fdp = -1;
  g_close (tmp, NULL);
}

/*
 * RcDirectoryIter:
 *
 * An open directory reader.
 */
typedef DIR RcDirectoryIter;

G_DEFINE_AUTOPTR_CLEANUP_FUNC (RcDirectoryIter, closedir)

RcDirectoryIter *rc_directory_iter_new (RcFileDescriptor fd,
                                        GError **error);

/*
 * RcPathFlags:
 * @RC_PATH_FLAGS_NONE: Nothing special
 * @RC_PATH_FLAGS_NOFOLLOW_SYMLINKS: If the last component of a path is a
 *  symbolic link, act on the symbolic link instead of the file it refers to.
 *
 * Flags affecting path resolution.
 */
typedef enum
{
  RC_PATH_FLAGS_NONE = 0,
  RC_PATH_FLAGS_NOFOLLOW_SYMLINKS = (1 << 0),
  RC_PATH_FLAGS_OVERWRITE = (1 << 1),
} RcPathFlags;

RcFileDescriptor rc_open_dir_at (const gchar *log_path,
                                 RcFileDescriptor dirfd,
                                 const gchar *path,
                                 RcPathFlags flags,
                                 GError **error);

gboolean rc_durable_symlink_at (const gchar *target,
                                const gchar *log_parent_path,
                                RcFileDescriptor parent_fd,
                                const gchar *link_name,
                                RcPathFlags flags,
                                GError **error);

gboolean rc_file_descriptor_clone_content_sync (RcFileDescriptor input,
                                                RcFileDescriptor output,
                                                GCancellable *cancellable,
                                                GError **error);

void rc_file_descriptor_clone_content_async (RcFileDescriptor input,
                                             RcFileDescriptor output,
                                             GCancellable *cancellable,
                                             GAsyncReadyCallback callback,
                                             gpointer user_data);

gboolean rc_file_descriptor_clone_content_finish (GAsyncResult *res,
                                                  GError **error);


G_END_DECLS

#endif
