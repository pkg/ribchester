/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "rc-internal.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_COPY_THREADS 2
#define COPY_THREAD_POOL_OWNS_ALL_THREAD TRUE

extern RibchesterData pRibchesterData ;


/**
 * _RunningThreadInfo
 * @pCurrentMountPoint 		: Parameter that indicates the base of source copy path which eventually has to be mounted
 * @uinCopyPrio				: Indicates the priority of the operation handled by the thread
 * @bThreadRunning			: Indicates whether the thread is running or killed.
 *
 *
 * Data structure to hold the information about thread operation.
 */
typedef struct _RunningThreadInfo RunningThreadInfo;
struct _RunningThreadInfo
{
	gchar *pCurrentMountPoint;
	guint 	uinCopyPrio;
	gboolean bThreadRunning;
};

/* @pRunningThreadInfo:
 * Data structure to hold the information about thread operation.
 *
 */

static RunningThreadInfo pRunningThreadInfo[MAX_COPY_THREADS];

/* @pROSysCopyQ: Maintain queue system to copy the contents from
 * RO file system to the RW partition upon first boot of the system
 * based on the priority.*/
static GQueue *pROSysCopyQ = NULL ;

/* @SYS_COPY_Q_LOCK: Mutex lock mechanism to control access over @pRunningThreadInfo
 * which is used to control the scheduling of the copying thread pool.
 *
 */
G_LOCK_DEFINE(SYS_COPY_Q_LOCK);

/* @ACTIVE_THREAD_LOCK: Mutex lock mechanism to control access to data structure
 * holding the info of current active threads and to update the type of
 * operation they are handling.
 */
G_LOCK_DEFINE(ACTIVE_THREAD_LOCK);

static void rc_free_copythread_data_structure(SubvolCopyInfo *pCopyInfo)
{
	if(NULL == pCopyInfo)
		return;
	FREE_MEM_IF_NOT_NULL(pCopyInfo->pDestSubVolName);
	FREE_MEM_IF_NOT_NULL(pCopyInfo->pSourceDirPath);
	FREE_MEM_IF_NOT_NULL(pCopyInfo->pDestDirPath);
	FREE_MEM_IF_NOT_NULL(pCopyInfo->pMountPoint);
	pCopyInfo = NULL ;
}

static gint rc_sys_copy_thread_priority_insert (gconstpointer pThread1Data,
		gconstpointer pThread2Data, gpointer pUserData)
{
	SubvolCopyInfo *pCopyInfo1 = (SubvolCopyInfo*) pThread1Data;
	SubvolCopyInfo *pCopyInfo2 = (SubvolCopyInfo*) pThread2Data;

	if( (NULL != pCopyInfo1) && (NULL != pCopyInfo2) )
		return (pCopyInfo1->priority < pCopyInfo2->priority ? -1 : pCopyInfo1->priority == pCopyInfo2->priority ? 0 : +1);

	return 0;
}

void rc_add_to_startup_copy_queue(SubvolCopyInfo *pCopyInfo)
{
	if(NULL == pROSysCopyQ)
		pROSysCopyQ = g_queue_new();
	g_queue_insert_sorted (pROSysCopyQ , pCopyInfo ,rc_sys_copy_thread_priority_insert , NULL );
}


static gint rc_check_pending_copy_for_mount_point(
		gconstpointer pQueueData , gconstpointer pUserData)
{
	gchar *pMountPoint = (gchar*) pUserData ;
	SubvolCopyInfo *pCopyInfo = (SubvolCopyInfo*)pQueueData ;
	if(NULL == pCopyInfo)
		return 1;
	/* return Zero if found */
	return g_strcmp0(pCopyInfo->pMountPoint , pMountPoint);
}


static guint
rc_register_thread_activity (SubvolCopyInfo *pCopyInfo,
	guint uinThreadNo)
{
	guint uinCount = 0;

	/************************ LOCK ******************************/
	G_LOCK(ACTIVE_THREAD_LOCK);
	for(;uinThreadNo == 0 && uinCount < MAX_COPY_THREADS ; uinCount++)
	{
		if(FALSE == pRunningThreadInfo[uinCount].bThreadRunning)
		{
			uinThreadNo = ++uinCount ;
#if 0
			printf("______________________________\n\n");
			printf("Current thread ID = [ %d ] \n",uinThreadNo);
			printf("\n\n______________________________\n\n");
#endif

			break;
		}
	}
	if(uinThreadNo != 0)
	{
		pRunningThreadInfo[uinThreadNo-1].bThreadRunning = TRUE ;
		pRunningThreadInfo[uinThreadNo-1].pCurrentMountPoint = pCopyInfo->pMountPoint ;
		pRunningThreadInfo[uinThreadNo-1].uinCopyPrio = pCopyInfo->priority;
	}
	G_UNLOCK(ACTIVE_THREAD_LOCK);
	/*__________________________  UNLOCK _________________________*/
	return uinThreadNo ;
}

static gboolean
rc_do_copy (SubvolCopyInfo *pCopyInfo)
{
	gchar* pCopyCommand = g_strdup_printf("cp -r %s/* %s",
			pCopyInfo->pSourceDirPath, pCopyInfo->pDestDirPath);
	gboolean bCopySucess = FALSE ;
	if(NULL != pCopyCommand)
	{
		if(system ( (gchar *)pCopyCommand) < 0)
		{
			g_warning("Copy error \n");
		}
		else
			bCopySucess = TRUE ;
		g_free(pCopyCommand);
		pCopyCommand = NULL ;
	}
	return bCopySucess ;
}


gboolean rc_check_mount_possibility(
		gchar *pMountPoint,guint uinThreadNo)
{
	gboolean bReadyToMount = TRUE;
	guint uinCount = 0;
	GList *pProductInQ = NULL;

	/************************ LOCK ******************************/
	G_LOCK(ACTIVE_THREAD_LOCK);
	for(;uinCount < MAX_COPY_THREADS ; uinCount++)
	{
		/* IGnore the current thread data */
		if(uinThreadNo == uinCount + 1)
			continue;
		if(0 == g_strcmp0(pRunningThreadInfo[uinCount].pCurrentMountPoint ,
				pMountPoint ))
		{
			G_UNLOCK(ACTIVE_THREAD_LOCK);
			return FALSE;
		}
	}
	G_UNLOCK(ACTIVE_THREAD_LOCK);
	/*__________________________  UNLOCK _________________________*/


	/************************ LOCK ******************************/
	G_LOCK(SYS_COPY_Q_LOCK);
	if(NULL != pROSysCopyQ)
	{
		pProductInQ = g_queue_find_custom(pROSysCopyQ ,
				pMountPoint,
				rc_check_pending_copy_for_mount_point);
	}
	G_UNLOCK(SYS_COPY_Q_LOCK);
	/*__________________________  UNLOCK _________________________*/
	if(NULL != pProductInQ)
		bReadyToMount = FALSE ;

	return bReadyToMount ;
}


/* Sequence:
 * Register thread activity.
 * Initiate copying.
 *
 * check if any acctive thread is handling the copy for current mount.
 * check if any pending copy is present in the queue for current mount.
 * If not initiate mount
 * proceed with next in the queue
 *
 *
 *
 * NOTE:  printf is used wherever required inside thread functions as
 * 	g_print used to crash some times
 *
 */
static gpointer copy_thread (gpointer pThreadData)
{
	SubvolCopyInfo *pCopyInfo = (SubvolCopyInfo *)pThreadData ;
	/* @uinThreadNo : 0 indicates uninitalized or exceed of maximum thread limit */
	guint uinThreadNo = 0;
	gboolean bContinueCopy = TRUE ;
	while(TRUE == bContinueCopy)
	{
		if(NULL != pCopyInfo)
		{
			printf("%s %d pCopyInfo->pMountPoint = %s \n" , __FUNCTION__ , __LINE__,pCopyInfo->pMountPoint);
			/* Register thread activity. */
			uinThreadNo = rc_register_thread_activity(pCopyInfo , uinThreadNo) ;
			if(0 == uinThreadNo)
			{
				g_warning(" All sys copy threads are active. exiting current thread");
				break ;
			}
			rc_do_copy(pCopyInfo);
			if( TRUE == rc_check_mount_possibility(pCopyInfo->pMountPoint , uinThreadNo))
			{
				/* Todo: avoid global variables in thread*/
				gchar *pBaseSubVolPath = NULL;

				if(pCopyInfo->enCopyType == COPY_SYSTEM_DATA)
				{
					pBaseSubVolPath = g_build_filename("/",rc_get_system_subvol_path(),
							pCopyInfo->pMountPoint, NULL);
				}
				else 	if(pCopyInfo->enCopyType == COPY_MEDIA_CONTENTS)
				{
					/* FIXME: this assumes that the
					 * target subvolume (in practice /home)
					 * is at a path on the general
					 * partition corresponding to its
					 * mount point (e.g.
					 * /run/ribchester/general/home),
					 * which does not seem like something
					 * we can assert in general?
					 * But it's true for /home. */
					pBaseSubVolPath = g_build_filename (rc_get_general_partition_path (),
							pCopyInfo->pMountPoint, NULL);
				}

				rc_mount_async (pBaseSubVolPath,
					pCopyInfo->pMountPoint,
					MOUNT_PRIO_HIGH,
					NULL, NULL, NULL);
				g_free (pBaseSubVolPath);
			}
			/*  Free pCopyInfo structure */
			rc_free_copythread_data_structure(pCopyInfo);

		}
		/************************ LOCK ******************************/
		G_LOCK(SYS_COPY_Q_LOCK);
		pCopyInfo = g_queue_pop_head(pROSysCopyQ) ;
		G_UNLOCK(SYS_COPY_Q_LOCK);
		/*__________________________  UNLOCK _________________________*/
		if(NULL == pCopyInfo)
			break;
	}
	if(uinThreadNo != 0)
	{
		/************************ LOCK ******************************/
		/*  Exiting from thread */
		G_LOCK(ACTIVE_THREAD_LOCK);
		pRunningThreadInfo[uinThreadNo-1].bThreadRunning = FALSE ;
		pRunningThreadInfo[uinThreadNo-1].pCurrentMountPoint = NULL ;
		pRunningThreadInfo[uinThreadNo-1].uinCopyPrio = 0;
		G_UNLOCK(ACTIVE_THREAD_LOCK);
		/*__________________________  UNLOCK _________________________*/
	}
	/* Todo :
	 * If this is the last thread then ,Do sys start-up copy clean up process
	 */

#if 0
	printf("______________________________\n\n");
	printf("Exiting thread ID = [ %d ] \n",uinThreadNo);
	printf("\n\n______________________________\n\n");
#endif

	/* Exit from the current thread and initiate clean up of thread resources */
	g_thread_exit(NULL);
	return NULL ;
}

void
rc_initiate_startup_copy (void)
{
	guint uinCount = 0;

	printf("%s %d \n",__FUNCTION__ , __LINE__);
	for(;uinCount < MAX_COPY_THREADS ; uinCount++)
	{
		SubvolCopyInfo *pCopyInfo = NULL;

		pRunningThreadInfo[uinCount].bThreadRunning = FALSE;
		G_LOCK(SYS_COPY_Q_LOCK);
		if(NULL != pROSysCopyQ)
		{
			pCopyInfo = g_queue_pop_head(pROSysCopyQ) ;
		}
		G_UNLOCK(SYS_COPY_Q_LOCK);

		if(NULL != pCopyInfo)
		{
			g_thread_new ( "sys_copy_thread", copy_thread , pCopyInfo);
		}
	}
}

