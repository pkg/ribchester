/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mounts.h"

#include <errno.h>
#include <fcntl.h>
#include <ftw.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <libmount.h>

#include <ribchester/ribchester.h>

#include "util.h"

#define MOUNT_PATHS_FILE "/proc/self/mountinfo"
/* this was once BTRFS_VOL_NAME_MAX */
#define NAME_MAX 255

/* g_autoptr needs a typedef to work with */
typedef struct libmnt_context RcLibmntContext;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (RcLibmntContext, mnt_free_context)

/* If this static assertion fails, change the printf format used for
 * getpid(), below */
G_STATIC_ASSERT (sizeof (pid_t) <= sizeof (long));

/*
 * rc_general_rw_mount_point:
 *
 * Initialize work directories path used by Ribchester core
 * The legacy value is /run/ribchester/general, but
 * /var/lib/ribchester is used if it exists
 */
const gchar *
rc_general_rw_mount_point (void)
{
  static const gchar *s_rc_general_rw_mount_point = NULL;

  if (s_rc_general_rw_mount_point == NULL)
    {
      if (g_file_test ("/var/lib/ribchester", G_FILE_TEST_IS_DIR))
        s_rc_general_rw_mount_point = "/var/lib/ribchester";
      else
        s_rc_general_rw_mount_point = "/run/ribchester/general";
    }

  return s_rc_general_rw_mount_point;
}

gchar *
rc_get_device_from_mount (const gchar *path)
{
  gchar *device;
  struct libmnt_fs *fs;
  struct libmnt_table *table;

  table = mnt_new_table_from_file (MOUNT_PATHS_FILE);

  if (table == NULL)
    {
      DEBUG ("Unable to open %s", MOUNT_PATHS_FILE);
      return NULL;
    }

  fs = mnt_table_find_target (table, path, MNT_ITER_BACKWARD);

  if (fs == NULL)
    {
      DEBUG ("Unable to find device associated with mount point '%s'", path);
      return NULL;
    }

  device = g_strdup (mnt_fs_get_srcpath (fs));

  mnt_reset_table (table);
  mnt_free_table (table);
  return device;
}

gboolean
rc_is_device_mounted_at_path (const gchar *device,
                              const gchar *path)
{
  gboolean ret;
  struct libmnt_table *table;

  table = mnt_new_table_from_file (MOUNT_PATHS_FILE);

  if (table == NULL)
    {
      DEBUG ("Unable to open %s", MOUNT_PATHS_FILE);
      return FALSE;
    }

  ret = (mnt_table_find_pair (table, device, path, MNT_ITER_FORWARD) != NULL);

  mnt_reset_table (table);
  mnt_free_table (table);
  return ret;
}

gboolean
rc_is_path_mounted (const gchar *path)
{
  gboolean ret;
  struct libmnt_table *table;

  table = mnt_new_table_from_file (MOUNT_PATHS_FILE);

  if (table == NULL)
    {
      DEBUG ("Unable to open %s", MOUNT_PATHS_FILE);
      return FALSE;
    }

  ret = (mnt_table_find_target (table, path, MNT_ITER_BACKWARD) != NULL);

  mnt_reset_table (table);
  mnt_free_table (table);
  return ret;
}

/*
 * @mount_point: absolute path of a mount point
 * @error: used to raise %RC_ERROR_NOT_FOUND if nothing is mounted
 *  at @mount_point, or another %RC_ERROR if the unmount operation fails
 *  for another reason
 *
 * Unmount the topmost filesystem mounted at @mount_point.
 *
 * Unlike the older 3-argument function with the same name, this function
 * does not delete @mount_point.
 */
gboolean
rc_unmount (const gchar *mount_point,
            GError **error)
{
  g_return_val_if_fail (mount_point != NULL, FALSE);
  g_return_val_if_fail (g_path_is_absolute (mount_point), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  DEBUG ("Mount point: \"%s\"", mount_point);

  /* Unmount the path - Give a lazy option */
  if (umount2 (mount_point, MNT_DETACH) < 0)
    {
      int saved_errno = errno;

      if (saved_errno == ENOENT || saved_errno == EINVAL)
        g_set_error (error, RC_ERROR, RC_ERROR_NOT_FOUND,
                     "Nothing is mounted at \"%s\"", mount_point);
      else
        g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                     "Unable to unmount \"%s\": %s", mount_point,
                     g_strerror (saved_errno));

      return FALSE;
    }

  return TRUE;
}

static gboolean
check_valid_subvolume_name (const gchar *name,
                            GError **error)
{
  gsize len;

  /* Check for invalid characters in the name (the kernel will
   * do this for us anyway, but this is a good opportunity to give
   * a better error message). */
  if (strcmp (name, ".") == 0 || strcmp (name, "..") == 0 ||
      strchr (name, '/') != NULL)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
              "Invalid name \"%s\" for subvolume", name);
      return FALSE;
    }

  len = strlen (name);

  /* In older versions of Ribchester the check was >= BTRFS_VOL_NAME_MAX.
   * Conservatively assume that we need extra space for \0 */
  if (len == 0 || len >= NAME_MAX)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Subvolume name \"%s\" is too long or zero-length, "
                   "cannot create it", name);
      return FALSE;
    }

  return TRUE;
}

/*
 * rc_bind_mount:
 * @source_path: absolute path of the sub directory,
 * @mount_point: absolute path of the location to mount the sub directory,
 * @error: if not %NULL, used to raise a %RC_ERROR on failure
 *
 * Bind mount @source_path at @mount_point.
 *
 * This function is thread-safe.
 *
 * Returns: %TRUE on success
 */
gboolean
rc_bind_mount (const gchar *source_path,
               const gchar *mount_point,
               GError **error)
{
  g_autoptr (RcLibmntContext) context = NULL;
  int ret;

  g_return_val_if_fail (source_path != NULL, FALSE);
  g_return_val_if_fail (mount_point != NULL, FALSE);
  g_return_val_if_fail (g_path_is_absolute (source_path), FALSE);
  g_return_val_if_fail (g_path_is_absolute (mount_point), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  context = mnt_new_context ();

  if (context == NULL)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Cannot create mount context");
      return FALSE;
    }

  ret = mnt_context_set_fstype (context, "auto");

  if (ret != 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Cannot set filesystem type: %s", g_strerror (-ret));
      return FALSE;
    }

  ret = mnt_context_set_options (context, "bind");

  if (ret != 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Cannot set mount options: %s", g_strerror (-ret));
      return FALSE;
    }

  ret = mnt_context_set_source (context, source_path);

  if (ret != 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Cannot set mount source: %s", g_strerror (-ret));
      return FALSE;
    }

  ret = mnt_context_disable_helpers (context, TRUE);

  if (ret != 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Cannot disable mount helpers: %s", g_strerror (-ret));
      return FALSE;
    }

  ret = mnt_context_set_target (context, mount_point);

  if (ret != 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Cannot set mount target: %s", g_strerror (-ret));
      return FALSE;
    }

  ret = mnt_context_mount (context);

  if (ret > 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "unable to bind mount \"%s\" at \"%s\": "
                   "system call failed: %s",
                   source_path, mount_point, g_strerror (ret));
      return FALSE;
    }
  else if (ret < 0)
    {
      /* some non-system-call error */
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "unable to bind mount \"%s\" at \"%s\": %s",
                   source_path, mount_point, g_strerror (-ret));
      return FALSE;
    }

  DEBUG ("mount completed successfully");
  return TRUE;
}

/*
 * rc_create_bind_source_at:
 * @log_parent_path: (nullable): the name of the parent directory, for
 *  debugging only. If %NULL, a placeholder string is used.
 * @parent: an open file descriptor to the parent directory
 *  (the special token `AT_FDCWD` is not allowed here)
 * @name: the subdirectory to create
 * @uid: the owner of the new directory
 * @gid: the group-owner of the new directory
 * @error: used to raise a %RC_ERROR on failure
 *
 * Create a directory that can be used as a bind source named @name
 * in @parent.
 *
 * If @name is not syntactically valid (a single path component without `/`,
 * strictly shorter than 255 bytes, and neither `.`, `..` nor
 * empty), this function returns a recoverable runtime error.
 * This was to ensure that names that would not be compatible with the
 * use of btrfs subvolumes are not used, even if support for btrfs subvolumes 
 * has now been dropped.
 *
 * Returns: %TRUE if the bind source was created successfully
 */
gboolean
rc_create_bind_source_at (const gchar *log_parent_path,
                          RcFileDescriptor parent,
                          const gchar *name,
                          guint uid,
                          guint gid,
                          GError **error)
{
  g_return_val_if_fail (parent != AT_FDCWD, FALSE);
  g_return_val_if_fail (parent >= 0, FALSE);
  g_return_val_if_fail (name != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (!check_valid_subvolume_name (name, error))
    {
      return FALSE;
    }

  if (log_parent_path == NULL)
    log_parent_path = "…";

  /* Create the bind source. In particular, this will not fail
   * if the requested directory already exists. */

  if (mkdirat (parent, name, 0755) < 0)
    {
      int saved_errno = errno;
      RcError code = RC_ERROR_FAILED;

      if (saved_errno != EEXIST)
        {
          g_set_error (error, RC_ERROR, code,
                       "Unable to create bind source \"%s/%s\": %s",
                       log_parent_path, name, g_strerror (saved_errno));
          return FALSE;
        }
    }

  if (fchownat (parent, name, uid, gid, AT_SYMLINK_NOFOLLOW) < 0)
    {
      g_set_error (error, RC_ERROR, RC_ERROR_FAILED,
                   "Unable to set ownership of bind source "
                   "\"%s/%s\": %s",
                   log_parent_path, name, g_strerror (errno));
      return FALSE;
    }

  return TRUE;
}

static int
unlink_cb (const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf)
{
  if (g_remove (fpath) != 0)
    {
      int saved_errno = errno;

      WARNING ("Cannot remove \"%s\": %s", fpath, g_strerror (saved_errno));

      errno = saved_errno;

      return -1;
    }

  return 0;
}

static int
rmrf (char *path)
{
  return nftw (path, unlink_cb, 64, FTW_DEPTH | FTW_PHYS | FTW_MOUNT);
}

/*
 * rc_delete_bind_source:
 * @parent_path: the name of the parent directory
 * @basename: the bind source directory to delete
 * @error: used to raise a %RC_ERROR on failure; in particular,
 *  if the bind source already did not exist, %RC_ERROR_NOT_FOUND will be raised
 *
 * Delete @basename from @parent. The content will be recursively removed.
 *
 * Returns: %TRUE if the bind_source directory was deleted successfully
 */
gboolean
rc_delete_bind_source (const gchar *parent_path,
                       const gchar *basename,
                       GError **error)
{
  g_autofree gchar *full_path = NULL;
  g_autoptr (GError) local_error = NULL;
  g_auto (RcFileDescriptor) parent = -1;

  g_return_val_if_fail (parent_path != NULL, FALSE);
  g_return_val_if_fail (basename != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (!check_valid_subvolume_name (basename, error))
    return FALSE;

  full_path = g_build_filename (parent_path, basename, NULL);

  parent = rc_open_dir_at (NULL, AT_FDCWD, parent_path,
                           RC_PATH_FLAGS_NOFOLLOW_SYMLINKS, error);

  if (parent < 0)
    return FALSE;

  if (rmrf (full_path) < 0)
    {
      int saved_errno = errno;
      RcError error_code = RC_ERROR_FAILED;

      if (saved_errno == ENOENT)
        error_code = RC_ERROR_NOT_FOUND;

      g_set_error (error, RC_ERROR, error_code,
                   "Unable to delete \"%s\" from \"%s\": %s",
                   basename, parent_path, g_strerror (saved_errno));

      return FALSE;
    }

  return TRUE;
}

static gboolean
copy_recursively (const gchar *source,
                  const gchar *target,
                  GCancellable *cancellable,
                  GError **error)
{
  GFileType source_type;
  const gchar *current;
  g_autoptr (GFile) source_file = NULL;
  g_autoptr (GFile) target_file = NULL;
  g_autoptr (GDir) dir = NULL;

  /* Check the cancellable */
  if (g_cancellable_set_error_if_cancelled (cancellable, error))
    return FALSE;

  /* Copy file */
  source_file = g_file_new_for_path (source);
  target_file = g_file_new_for_path (target);

  source_type = g_file_query_file_type (source_file,
                                        G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS,
                                        cancellable);

  /* If it is not a directory, copy it and return */
  if (source_type != G_FILE_TYPE_DIRECTORY)
    {
      if (!g_file_copy (source_file, target_file,
                        G_FILE_COPY_NOFOLLOW_SYMLINKS |
                        G_FILE_COPY_ALL_METADATA,
                        cancellable, NULL, NULL, error))
         return FALSE;

     return TRUE;
    }

  /* If it is a directory, copy it recursively */
  if (!g_file_make_directory (target_file, cancellable, error))
    return FALSE;

  if (!g_file_copy_attributes (source_file, target_file,
                               G_FILE_COPY_NOFOLLOW_SYMLINKS |
                               G_FILE_COPY_ALL_METADATA,
                               cancellable, error))
    return FALSE;

  /* Parse source directory listing */
  dir = g_dir_open (source, 0, error);
  if (!dir)
    return FALSE;

  /* Recursively copy content at parent */
  while ((current = g_dir_read_name (dir)) != NULL)
    {
      g_autofree gchar *fsource = NULL;
      g_autofree gchar *ftarget = NULL;

      fsource = g_build_filename (source, current, NULL);
      ftarget = g_build_filename (target, current, NULL);

      if (!copy_recursively (fsource, ftarget, cancellable, error))
        return FALSE;
    }

  return TRUE;
}

/*
 * rc_snapshot_bind_source:
 * @source_path: the name of the source directory
 * @parent_path: the name of the parent directory in which the new
 * snapshot will appear.
 * @name: the name of the new subdirectory relative to @parent
 * @error: used to raise a %RC_ERROR on failure
 *
 * Create a copy named @name in @parent. It is a snapshot of
 * @source. This is synchronous. Because it is a recursive copy, the operation
 * can take time.
 *
 * If @name is not syntactically valid (a single path component without `/`,
 * strictly shorter than 255 bytes, and neither `.`, `..` nor
 * empty), this function returns a recoverable runtime error.
 *
 * Returns: %TRUE if the snapshot was created successfully
 */
gboolean
rc_snapshot_bind_source (const gchar *source_path,
                         const gchar *target_path,
                         const gchar *name,
                         GError **error)
{
  g_autofree gchar *full_path = NULL;
  g_return_val_if_fail (source_path != NULL, FALSE);
  g_return_val_if_fail (target_path != NULL, FALSE);
  g_return_val_if_fail (name != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (!check_valid_subvolume_name (name, error))
    return FALSE;

  full_path = g_build_filename (target_path, name, NULL);

  if (!copy_recursively (source_path, full_path, NULL, error))
    {
      rmrf (full_path);

      /* On failure rmrf () already displayed a WARNING,
       * so don't duplicate that here */

      return FALSE;
    }

  return TRUE;
}
