/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <gio/gunixfdlist.h>
#include <glib-object.h>
#include <glib.h>

#include <ribchester/ribchester.h>

GMainLoop *loop = NULL;

static void
print_help (GOptionContext *context, const gchar *message)
{
  g_autofree gchar *help = g_option_context_get_help (context, TRUE, NULL);
  g_printerr ("%s\n\n", message);
  g_printerr ("%s\n", help);
}

static RibchesterBundleManager1 *
get_proxy (GOptionContext *context)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GDBusConnection) bus = NULL;
  g_autoptr (RibchesterBundleManager1) proxy = NULL;

  bus = g_bus_get_sync (G_BUS_TYPE_SYSTEM, NULL, &error);
  if (bus == NULL)
    {
      print_help (context, error->message);
      return NULL;
    }

  proxy =
    ribchester_bundle_manager1_proxy_new_sync (bus, G_DBUS_PROXY_FLAGS_NONE,
                                               RC_BUS_NAME,
                                               RC_OBJECT_PATH_BUNDLE_MANAGER1,
                                               NULL, &error);

  if (proxy == NULL)
    print_help (context, error->message);

  return g_steal_pointer (&proxy);
}

/** Install bundle **/
static gboolean
transaction_finished (RibchesterAppStoreTransaction *transaction)
{
  gint stage;

  g_object_get (transaction, "stage", &stage, NULL);

  return (stage == RIBCHESTER_APP_STORE_TRANSACTION_STAGE_SUCCEEDED
          || stage == RIBCHESTER_APP_STORE_TRANSACTION_STAGE_CANCELLED
          || stage == RIBCHESTER_APP_STORE_TRANSACTION_STAGE_FAILED);
}

static void
maybe_print_bundle_id (RibchesterAppStoreTransaction *transaction)
{
  g_autofree gchar *bundleid = NULL;

  g_object_get (transaction, "bundle-id", &bundleid, NULL);

  if (bundleid != NULL && *bundleid != '\0')
    g_print ("BundleID: %s\n", bundleid);
}

static void
bundle_id_cb (GObject *object, GParamSpec *pspec, gpointer user_data)
{
  RibchesterAppStoreTransaction *transaction = RIBCHESTER_APP_STORE_TRANSACTION (object);
  maybe_print_bundle_id (transaction);
}

static void
maybe_print_bundle_version (RibchesterAppStoreTransaction *transaction)
{
  g_autofree gchar *version = NULL;

  g_object_get (transaction, "bundle-version", &version, NULL);

  if (version != NULL && *version != '\0')
    g_print ("BundleVersion: %s\n", version);
}

static void
bundle_version_cb (GObject *object, GParamSpec *pspec, gpointer user_data)
{
  RibchesterAppStoreTransaction *transaction = RIBCHESTER_APP_STORE_TRANSACTION (object);
  maybe_print_bundle_version (transaction);
}

static void
print_stage (RibchesterAppStoreTransaction *transaction)
{
  gint stage;
  struct {
    gint value;
    const gchar *name;
  } stages[] = {
    { -20, "Cancelled" },
    { -15, "Cancelling" },
    { -10, "Failed" },
    { -5, "Failing" },
    { 0, "Starting" },
    { 10, "Verifying" },
    { 20, "Preparing" },
    { 30, "Unpacking" },
    { 40, "Finishing" },
    { 50, "Succeeded" },
  };
  guint i;

  g_object_get (transaction, "stage", &stage, NULL);

  for (i = 0; i < G_N_ELEMENTS (stages); i++)
    {
      if (stage == stages[i].value)
        {
          g_print ("Stage: %s\n", stages[i].name);
          return;
        }
    }

  g_print ("Stage: Unknown - %d\n", stage);
}

static void
stage_cb (GObject *object, GParamSpec *pspec, gpointer user_data)
{
  RibchesterAppStoreTransaction *transaction = RIBCHESTER_APP_STORE_TRANSACTION (object);

  print_stage (transaction);
  if (transaction_finished (transaction))
    g_main_loop_quit (loop);
}

static gint
do_install (int argc, char **argv)
{
  g_autoptr (GOptionContext) context = NULL;
  g_autoptr (GError) error = NULL;
  int bundle, handle, stage;
  g_autoptr (RibchesterBundleManager1) proxy = NULL;
  g_autoptr (RibchesterAppStoreTransaction) transaction = NULL;
  g_autoptr (GUnixFDList) fd_list = NULL;
  g_autofree gchar *objpath = NULL;
  gboolean ret;

  context = g_option_context_new ("BUNDLE");
  g_option_context_set_help_enabled (context, TRUE);
  g_option_context_set_summary (context, "Install application bundle");

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      print_help (context, error->message);
      return -1;
    }

  /* should be one remaining argument the bundle path */
  if (argc != 2)
    {
      print_help (context, "Missing bundle argument");
      return -1;
    }

  bundle = open (argv[1], O_RDONLY);
  if (bundle < 0)
    {
      print_help (context, g_strerror (errno));
      return -1;
    }

  proxy = get_proxy (context);

  if (proxy == NULL)
    return -1;

  fd_list = g_unix_fd_list_new ();
  handle = g_unix_fd_list_append (fd_list, bundle, NULL);

  ret =
    ribchester_bundle_manager1_call_install_bundle_sync (proxy,
                                                         g_variant_new_handle (handle),
                                                         fd_list,
                                                         &objpath,
                                                         NULL,
                                                         NULL,
                                                         &error);

  if (!ret)
    {
      print_help (context, error->message);
      return -1;
    }

  transaction = ribchester_app_store_transaction_proxy_new_sync (g_dbus_proxy_get_connection (G_DBUS_PROXY (proxy)),
                                                                 G_DBUS_PROXY_FLAGS_GET_INVALIDATED_PROPERTIES,
                                                                 g_dbus_proxy_get_name (G_DBUS_PROXY (proxy)),
                                                                 objpath,
                                                                 NULL,
                                                                 &error);
  if (!transaction)
    {
      print_help (context, error->message);
      return -1;
    }

  maybe_print_bundle_id (transaction);
  maybe_print_bundle_version (transaction);
  print_stage (transaction);

  if (!transaction_finished (transaction))
    {
      loop = g_main_loop_new (NULL, FALSE);

      g_signal_connect (transaction, "notify::bundle-id",
                        G_CALLBACK (bundle_id_cb), NULL);
      g_signal_connect (transaction, "notify::bundle-version",
                        G_CALLBACK (bundle_version_cb), NULL);
      g_signal_connect (transaction, "notify::stage",
                        G_CALLBACK (stage_cb), NULL);

      g_main_loop_run (loop);
      g_clear_pointer (&loop, g_main_loop_unref);
    }

  g_object_get (transaction, "stage", &stage, NULL);
  ribchester_app_store_transaction_call_release_sync (transaction, NULL, NULL);

  return stage == RIBCHESTER_APP_STORE_TRANSACTION_STAGE_SUCCEEDED ? 0 : -1;
}

/* Remove bundle */
static gint
do_remove (int argc, char **argv)
{
  g_autoptr (GOptionContext) context = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (RibchesterBundleManager1) proxy = NULL;
  gboolean ret;

  context = g_option_context_new ("BUNDLEID");
  g_option_context_set_help_enabled (context, TRUE);
  g_option_context_set_summary (context, "Remove application");

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      print_help (context, error->message);
      return -1;
    }

  /* should be one remaining argument the bundle id */
  if (argc != 2)
    {
      print_help (context, "Missing bundle id");
      return -1;
    }

  proxy = get_proxy (context);

  if (proxy == NULL)
    return -1;

  ret = ribchester_bundle_manager1_call_remove_bundle_sync (proxy, argv[1],
                                                            NULL, &error);

  if (!ret)
    {
      print_help (context, error->message);
      return -1;
    }

  return 0;
}

struct {
  const gchar *command;
  const gchar *desc;
  gint (*handle) (int argc, char **argv);
} commands[] = {
  { "install", "Install application bundle", do_install },
  { "remove", "Remove application bundle", do_remove },
};

static void
usage (void)
{
  guint i;

  g_printerr ("Usage:\n");
  g_printerr (" ribchesterctl COMMAND [ARGS...]\n");
  g_printerr ("\n");
  g_printerr ("Commands:\n");

  for (i = 0; i < G_N_ELEMENTS (commands); i++)
    {
      g_printerr ("  %s\t\t%s\n", commands[i].command, commands[i].desc);
    }
  g_printerr ("\n");
  g_printerr ("Use \'ribchesterctl COMMAND --help\' to get detailed help\n");
}

int
main (int argc, char **argv)
{
  guint i;
  const gchar *command;

  if (argc < 2)
    {
      usage ();
      return -1;
    }

  command = argv[1];
  argv += 1;
  argc--;

  for (i = 0; i < G_N_ELEMENTS (commands); i++)
    {
      if (g_strcmp0 (command, commands[i].command) == 0)
        {
          return commands[i].handle (argc, argv);
        }
    }

  g_printerr ("Invalid command: %s\n", command);
  usage ();

  return -1;
}
